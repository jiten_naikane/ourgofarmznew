package in.innasoft.gofarmz.models;


import org.json.JSONObject;

import in.innasoft.gofarmz.utils.AppUrls;

public class FarmerSupplierModel
{
    public String id,type,farm_name,message,farmer_name,farm_photo;

    public FarmerSupplierModel(JSONObject jsonObject) {
        try {
            if (jsonObject.has("id") && !jsonObject.isNull("id"))
                this.id = jsonObject.getString("id");

            if (jsonObject.has("type") && !jsonObject.isNull("type"))
                this.type = jsonObject.getString("type");

            if (jsonObject.has("farm_name") && !jsonObject.isNull("farm_name"))
                this.farm_name = jsonObject.getString("farm_name");

            if (jsonObject.has("farmer_name") && !jsonObject.isNull("farmer_name"))
                this.farmer_name = jsonObject.getString("farmer_name");

            if (jsonObject.has("farm_photo") && !jsonObject.isNull("farm_photo"))
                this.farm_photo = AppUrls.BASE_URL+jsonObject.getString("farm_photo");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
