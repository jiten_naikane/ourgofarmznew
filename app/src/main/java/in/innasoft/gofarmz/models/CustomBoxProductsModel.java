package in.innasoft.gofarmz.models;

public class CustomBoxProductsModel {

    private String id;
    private String type;
    private String boxName;
    private String unitValue;
    private String price;
    private String about;
    private String moreinfo;
    private String availability;
    private String unitName;
    private String images;

    private String pdtId;
    private String pdtName;
    private String pdtImages;
    private String pdtAvailability;

    private String optId;
    private String pId;
    private String optName;
    private String optPrice;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBoxName() {
        return boxName;
    }

    public void setBoxName(String boxName) {
        this.boxName = boxName;
    }

    public String getUnitValue() {
        return unitValue;
    }

    public void setUnitValue(String unitValue) {
        this.unitValue = unitValue;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getMoreinfo() {
        return moreinfo;
    }

    public void setMoreinfo(String moreinfo) {
        this.moreinfo = moreinfo;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getPdtId() {
        return pdtId;
    }

    public void setPdtId(String pdtId) {
        this.pdtId = pdtId;
    }

    public String getPdtName() {
        return pdtName;
    }

    public void setPdtName(String pdtName) {
        this.pdtName = pdtName;
    }

    public String getPdtImages() {
        return pdtImages;
    }

    public void setPdtImages(String pdtImages) {
        this.pdtImages = pdtImages;
    }

    public String getPdtAvailability() {
        return pdtAvailability;
    }

    public void setPdtAvailability(String pdtAvailability) {
        this.pdtAvailability = pdtAvailability;
    }

    public String getOptId() {
        return optId;
    }

    public void setOptId(String optId) {
        this.optId = optId;
    }

    public String getpId() {
        return pId;
    }

    public void setpId(String pId) {
        this.pId = pId;
    }

    public String getOptName() {
        return optName;
    }

    public void setOptName(String optName) {
        this.optName = optName;
    }

    public String getOptPrice() {
        return optPrice;
    }

    public void setOptPrice(String optPrice) {
        this.optPrice = optPrice;
    }
}
