package in.innasoft.gofarmz.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.innasoft.gofarmz.adapters.AddCitiesAdapter;
import in.innasoft.gofarmz.adapters.CitiesAdapter;
import in.innasoft.gofarmz.models.CitiesModel;

public class AddCitiesFilter extends Filter {

    AddCitiesAdapter adapter;
    ArrayList<CitiesModel> filterList;

    public AddCitiesFilter(ArrayList<CitiesModel> filterList, AddCitiesAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<CitiesModel> filteredPlayers = new ArrayList<CitiesModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getName().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results)
    {
        adapter.citiesModels = (ArrayList<CitiesModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
