package in.innasoft.gofarmz.filters;

import android.widget.Filter;
import java.util.ArrayList;
import in.innasoft.gofarmz.adapters.AreasAdapter;
import in.innasoft.gofarmz.models.AreasModel;

public class AreasFilter extends Filter {

    AreasAdapter adapter;
    ArrayList<AreasModel> filterList;

    public AreasFilter(ArrayList<AreasModel> filterList, AreasAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<AreasModel> filteredPlayers = new ArrayList<AreasModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getName().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results)
    {
        adapter.areasModels = (ArrayList<AreasModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
