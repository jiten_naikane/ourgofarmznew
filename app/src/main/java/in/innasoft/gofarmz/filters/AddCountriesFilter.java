package in.innasoft.gofarmz.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.innasoft.gofarmz.adapters.AddCountriesAdapter;
import in.innasoft.gofarmz.adapters.CountriesAdapter;
import in.innasoft.gofarmz.models.CountriesModel;

public class AddCountriesFilter extends Filter {

    AddCountriesAdapter adapter;
    ArrayList<CountriesModel> filterList;

    public AddCountriesFilter(ArrayList<CountriesModel> filterList, AddCountriesAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        if(constraint != null && constraint.length() > 0)
        {
            constraint=constraint.toString().toUpperCase();

            ArrayList<CountriesModel> filteredPlayers=new ArrayList<CountriesModel>();

            for (int i=0;i<filterList.size();i++)
            {
                if(filterList.get(i).getName().toUpperCase().contains(constraint))
                {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count=filteredPlayers.size();
            results.values=filteredPlayers;
        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results)
    {
        adapter.countriesModels = (ArrayList<CountriesModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
