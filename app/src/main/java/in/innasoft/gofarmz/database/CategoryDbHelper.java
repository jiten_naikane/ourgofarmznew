package in.innasoft.gofarmz.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class CategoryDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "gofarmz.db";
    private static final String TABLE_CATEGORY = "category";

    /*Category*/
    public static final String CATEGORY_ID = "category_id";
    public static final String CATEGORY_NAME = "category_name";
    public static final String CATEGORY_IMAGE = "category_image";
    public static final String TYPE_AVAIL = "type_avail";

    public CategoryDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public CategoryDbHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_CATEGORY_TABLE = "CREATE TABLE " + TABLE_CATEGORY + "(" + CATEGORY_ID + " TEXT ," + CATEGORY_NAME + " TEXT ," + CATEGORY_IMAGE + " TEXT ,"+TYPE_AVAIL+" TEXT )";
        db.execSQL(CREATE_CATEGORY_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
        onCreate(db);

    }

    /*category*/
    public void addCategoryList(ContentValues contentValues) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_CATEGORY, null, contentValues);
        db.close();
    }

    /*category*/
    public List<String> getCategoryId() {
        List<String> categoryId = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_CATEGORY;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(selectQuery, null);
        } finally {
            if (cursor.moveToFirst()) {
                do {
                    categoryId.add(cursor.getString(0));

                } while (cursor.moveToNext());
            }
        }

        return categoryId;
    }

    public List<String> getCategoryName() {
        List<String> categoryName = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_CATEGORY;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(selectQuery, null);
        } finally {
            if (cursor.moveToFirst()) {
                do {
                    categoryName.add(cursor.getString(1));

                } while (cursor.moveToNext());
            }
        }

        return categoryName;
    }

    public List<String> getCategoryImage() {
        List<String> categoryImage = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_CATEGORY;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(selectQuery, null);
        } finally {
            if (cursor.moveToFirst()) {
                do {
                    categoryImage.add(cursor.getString(2));

                } while (cursor.moveToNext());
            }
        }

        return categoryImage;
    }


    public List<String> getTypeAvail() {
        List<String> type_avail = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_CATEGORY;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(selectQuery, null);
        } finally {
            if (cursor.moveToFirst()) {
                do {
                    type_avail.add(cursor.getString(3));

                } while (cursor.moveToNext());
            }
        }

        return type_avail;
    }


    public void deleteCategory() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_CATEGORY);
        db.close();
    }
}
