package in.innasoft.gofarmz.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class GoFrmzDB extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "storecontents.db";
    private static final String TABLE_DETAILS = "stract";

    public static final String ID = "id";
    public static final String PDT_NAME = "pdtName";
    public static final String IMAGES = "images";
    public static final String AVAILABILITY = "availability";
    public static final String OPTIONS = "options";



    public GoFrmzDB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public GoFrmzDB(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ACTIVITY_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_DETAILS + "("
                + ID + " TEXT ,"
                + PDT_NAME + " TEXT ,"
                + IMAGES + " TEXT ,"
                + AVAILABILITY + " TEXT ,"
                + OPTIONS + " TEXT " +
                 ")";
        db.execSQL(CREATE_ACTIVITY_TABLE);

     //db.execSQL("CREATE INDEX idxModel ON serfrd (NAME);");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DETAILS);
        onCreate(db);
    }

    public void addFriendsList(ContentValues contentValues){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_DETAILS, null, contentValues);
        db.close();
    }

    public List<String> getId() {
        List<String> id = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                id.add(cursor.getString(0));

            } while (cursor.moveToNext());
        }
        return id;
    }

    public List<String> getPdtName() {
        List<String> pdt_name = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                pdt_name.add(cursor.getString(1));

            } while (cursor.moveToNext());
        }
        return pdt_name;
    }

    public List<String> getImages() {
        List<String> images = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                images.add(cursor.getString(2));

            } while (cursor.moveToNext());
        }
        return images;
    }

    public List<String> getAvailability() {
        List<String> availability = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                availability.add(cursor.getString(3));

            } while (cursor.moveToNext());
        }
        return availability;
    }

    public List<String> getOptions() {
        List<String> options = new ArrayList<String>();

        String selectQuery = "SELECT * FROM " + TABLE_DETAILS;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()){
            do {
                options.add(cursor.getString(4));

            } while (cursor.moveToNext());
        }
        return options;
    }


    public void emptyDBBucket()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_DETAILS); //delete all rows in a table
        db.close();
    }

    public Cursor retrieve(String searchTerm)
    {
        String[] columns={PDT_NAME};
        Cursor c=null;
        SQLiteDatabase db = this.getWritableDatabase();
        if(searchTerm != null && searchTerm.length()>0)
        {
            String sql="SELECT * FROM "+ TABLE_DETAILS+" WHERE "+ PDT_NAME+" LIKE '%"+searchTerm+"%'";
            c=db.rawQuery(sql,null);
            return c;
        }
        c=db.query(TABLE_DETAILS,columns,null,null,null,null,null);
        return c;
    }

    public void openDB()
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
    //CLOSE
    public void closeDB()
    {
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
}
