package in.innasoft.gofarmz.itemClickListerners;

import android.view.View;

public interface CountriesItemClickListener
{
    void onItemClick(View v, int pos);
}

