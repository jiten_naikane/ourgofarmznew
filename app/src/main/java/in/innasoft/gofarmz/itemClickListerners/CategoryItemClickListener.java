package in.innasoft.gofarmz.itemClickListerners;

import android.view.View;

public interface CategoryItemClickListener
{
    void onItemClick(View v, int pos);
}

