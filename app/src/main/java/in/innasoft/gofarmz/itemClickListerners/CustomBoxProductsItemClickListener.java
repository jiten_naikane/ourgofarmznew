package in.innasoft.gofarmz.itemClickListerners;

import android.view.View;

public interface CustomBoxProductsItemClickListener
{
    void onItemClick(View v, int pos);
}

