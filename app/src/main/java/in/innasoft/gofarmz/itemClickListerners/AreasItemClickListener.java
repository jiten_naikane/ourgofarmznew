package in.innasoft.gofarmz.itemClickListerners;

import android.view.View;

public interface AreasItemClickListener
{
    void onItemClick(View v, int pos);
}

