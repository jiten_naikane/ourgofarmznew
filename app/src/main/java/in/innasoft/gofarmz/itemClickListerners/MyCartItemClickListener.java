package in.innasoft.gofarmz.itemClickListerners;

import android.view.View;

public interface MyCartItemClickListener
{
    void onItemClick(View v, int pos);
}

