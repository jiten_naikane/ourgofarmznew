package in.innasoft.gofarmz.itemClickListerners;

import android.view.View;

public interface StatesItemClickListener
{
    void onItemClick(View v, int pos);
}

