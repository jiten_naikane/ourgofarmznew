package in.innasoft.gofarmz.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapters.PromoCodeAdapter;
import in.innasoft.gofarmz.models.PromoCodeModel;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.DividerItemDecorator;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class PromoCouponListActivity extends AppCompatActivity implements View.OnClickListener {

    TextView done_text, offer_text, toolbar_title;
    ImageView close_img;
    Button applyCoupanyButton;
    EditText edt_coupan_code;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    String deviceId, token, user_id;
    Typeface light, regular, bold;
    RecyclerView promocode_recyclerview;
    PromoCodeAdapter promoCodeAdapter;
    ArrayList<PromoCodeModel> promoCodeList;
    LinearLayoutManager layoutManager;
    Double final_amount;

    ProgressDialog pprogressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dummy_activity_promo_coupon_list);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);

        checkInternet = NetworkChecking.isConnected(this);
        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));

        Bundle bundle = getIntent().getExtras();
        final_amount = bundle.getDouble("FINAL_AMOUNT");

        Log.d("FFFF", String.valueOf(final_amount));

        promocode_recyclerview = findViewById(R.id.promocode_recyclerview);
        pprogressDialog = new ProgressDialog(PromoCouponListActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);


        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        done_text = findViewById(R.id.done_text);
        done_text.setTypeface(bold);
        done_text.setOnClickListener(this);
        offer_text = findViewById(R.id.offer_text);
        offer_text.setTypeface(bold);

        edt_coupan_code = findViewById(R.id.edt_coupan_code);
        edt_coupan_code.setTypeface(light);


        applyCoupanyButton = findViewById(R.id.applyCoupanyButton);
        applyCoupanyButton.setTypeface(light);
        applyCoupanyButton.setOnClickListener(this);
        getPromoCodes();
    }

    public void getPromoCodes() {
        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.PROMOCODES;
            Log.d("PROMORURL", url);
            pprogressDialog.show();
            StringRequest strAllMember = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("PROMORRESP", response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("status");
                        if (successResponceCode.equals("10100"))
                        {
                            pprogressDialog.dismiss();
                            promoCodeList = new ArrayList<>();

                            JSONArray getPromoData = jsonObject.getJSONArray("data");
                            for (int i = 0; i < getPromoData.length(); i++) {
                                JSONObject jdataobj = getPromoData.getJSONObject(i);
                                PromoCodeModel allmember = new PromoCodeModel(jdataobj);
                                promoCodeList.add(allmember);
                            }
                            promoCodeAdapter = new PromoCodeAdapter(promoCodeList, PromoCouponListActivity.this, R.layout.dummy_row_promo_codes);
                            layoutManager = new LinearLayoutManager(getApplicationContext());
                            promocode_recyclerview.setNestedScrollingEnabled(false);
                            promocode_recyclerview.setLayoutManager(layoutManager);
                            RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(PromoCouponListActivity.this, R.drawable.recycler_view_divider));
                            promocode_recyclerview.addItemDecoration(dividerItemDecoration);

                            promocode_recyclerview.setAdapter(promoCodeAdapter);
                        }
                        if (successResponceCode.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(PromoCouponListActivity.this, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                        }
                        if (successResponceCode.equals("10300")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(PromoCouponListActivity.this, "No Coupon Found..!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    Log.e("Error", error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
                    headers.put("X-Deviceid", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("TOP_FIVE_HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strAllMember.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(strAllMember);
        } else {
            Toast.makeText(this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onClick(View v) {
        if (v == applyCoupanyButton) {
            String selected_code = promoCodeAdapter.send_code_Value;
            Log.d("CDDCDCD", selected_code);
            validateCouponCode(selected_code);
        }
        if (v == close_img) {
            Intent intent = new Intent(PromoCouponListActivity.this,PaymentDetails.class);
            intent.putExtra("activity","PromoCouponListActivity");
            intent.putExtra("promoStatus","0");
            startActivity(intent);
        }

        if(v==done_text)
        {
            String selected_code = edt_coupan_code.getText().toString();
            Log.d("DOOODONE", selected_code);
            validateCouponCode(selected_code);
        }
    }

    public void validateCouponCode(final String coupon_code)
    {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {

            String url_follow_members = AppUrls.BASE_URL + AppUrls.VALIDATE_COUPON_CODE + user_id + "/coupon/validate";
            Log.d("validcoupnURL", url_follow_members);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_follow_members, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("validcoupnRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("status");
                        String message = jobcode.getString("message");

                        if (response_code.equals("10100")) {

                            String appliedCoupon_amount = jobcode.getString("data");
                            Log.d("APPPP", appliedCoupon_amount);

                            Intent returnIntent = new Intent();
                            returnIntent.putExtra("result",coupon_code);
                            returnIntent.putExtra("applied_amt", appliedCoupon_amount);
                            PromoCouponListActivity.this.setResult(Activity.RESULT_OK, returnIntent);
                            PromoCouponListActivity.this.finish();


                        }
                        if (response_code.equals("10200")) {

                            Toast.makeText(PromoCouponListActivity.this, "Invalid Input", Toast.LENGTH_LONG).show();
                        }
                        if (response_code.equals("10300")) {

                            Intent returnIntent = new Intent();
                            returnIntent.putExtra("result","2");

                            PromoCouponListActivity.this.setResult(Activity.RESULT_OK, returnIntent);
                            PromoCouponListActivity.this.finish();
                            Toast.makeText(PromoCouponListActivity.this, ""+message, Toast.LENGTH_LONG).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    error.getMessage();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("code", coupon_code);
                    params.put("order_amount", String.valueOf(final_amount));
                    Log.d("COUPANPARAM", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
                    headers.put("X-Deviceid", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(req_members);
        } else {
            Toast.makeText(PromoCouponListActivity.this, "Check Internet Connection..!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(PromoCouponListActivity.this,PaymentDetails.class);
        intent.putExtra("activity","PromoCouponListActivity");
        intent.putExtra("promoStatus","0");
        startActivity(intent);
        finish();
    }

    public void setCode(String code) {
        edt_coupan_code.setText(code);


    }

}
