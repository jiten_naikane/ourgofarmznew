package in.innasoft.gofarmz.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapters.CartRecordChild;
import in.innasoft.gofarmz.adapters.MyCartAdapter;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class ViewDetails extends AppCompatActivity implements View.OnClickListener {

    ImageView close;
    TextView allsponsors_toolbar_title;
    ListView productlist;
    Typeface light, regular, bold;
    CartRecordChild adapter;

    ArrayList<Object> detailsarr=new ArrayList<Object>();
    String type;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token,idd, price;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_details);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));

        userSessionManager = new UserSessionManager(ViewDetails.this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);

        close = findViewById(R.id.close_img);
        close.setOnClickListener(this);
        allsponsors_toolbar_title = findViewById(R.id.allsponsors_toolbar_title);
        allsponsors_toolbar_title.setTypeface(bold);

        productlist = findViewById(R.id.listRecordChild);

      //   detailsarr = (ArrayList<Object>) getIntent().getSerializableExtra("productlist");
        type = (String) getIntent().getSerializableExtra("TYPE");
        idd = (String) getIntent().getSerializableExtra("IDD");
        Log.d("LLL",type+"---"+idd);

       // adapter = new CartRecordChild(ViewDetails.this, detailsarr,type);
      //  productlist.setAdapter(adapter);

        getViewDetailChild();


    }

    public void getViewDetailChild()
    {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            String URL = AppUrls.BASE_URL + AppUrls.VIEW_CHILD_CART_DATA+"?user_id="+user_id+"&browser_id="+deviceId+"&cart_id="+idd;
            Log.d("CHILDCARTURL", URL);

            StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("CHILDCRES", response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                if (status.equals("10100"))
                                {


                                    String message = jsonObject.getString("message");
                                    Type stringMap = new TypeToken<Map<String, Object>>() {
                                    }.getType();
                                    Map<String, Object> data = new Gson().fromJson(jsonObject.optString("data"), stringMap);

                                    String currency = (String) data.get("currency");
                                    detailsarr = (ArrayList<Object>) data.get("records");
                                    Map<String, Object> recordPos = (Map<String, Object>) detailsarr.get(0);
                                    String cart_product_addon_id = (String) recordPos.get("cart_product_addon_id");
                                    String pdtId = (String) recordPos.get("pdtId");
                                    String optId = (String) recordPos.get("optId");
                                    String pdtName = (String) recordPos.get("pdtName");
                                    String images = (String) recordPos.get("images");
                                    String price = (String) recordPos.get("price");
                                    String quantity = (String) recordPos.get("quantity");
                                    String unitValue = (String) recordPos.get("unitValue");
                                    String unit_name = (String) recordPos.get("unit_name");
                                    String unitName = (String) recordPos.get("unitName");

                                    adapter = new CartRecordChild(ViewDetails.this, detailsarr,type);
                                    productlist.setAdapter(adapter);


                                }
                                if (status.equals("10200")) {

                                    Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10300")) {

                                }
                                if (status.equals("10400")) {

                                    Toast.makeText(getApplicationContext(), "Unable to Send Otp..", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.getMessage();
                        }
                    }) {

            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ViewDetails.this);
            requestQueue.add(stringRequest);
        } else {

            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onClick(View view)
    {
        if(view==close)
        {
            finish();
        }
      }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }
}
