package in.innasoft.gofarmz.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapters.CartRecordChild;
import in.innasoft.gofarmz.adapters.OrderHisDetailChildADapter;

public class OrderChildeViewDetails extends AppCompatActivity implements View.OnClickListener {

    ImageView close;
    TextView headet_text_category;
    ListView productlist;
    Typeface light, regular, bold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart_record_child_dialog);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));

          Bundle b=getIntent().getExtras();
           String title_text=b.getString("TITLE");

        close = findViewById(R.id.close);
        close.setOnClickListener(this);
        headet_text_category = findViewById(R.id.headet_text_category);
        headet_text_category.setText(title_text);
        headet_text_category.setTypeface(bold);

        productlist = findViewById(R.id.listRecordChild);

        ArrayList<Object> detailsarr = (ArrayList<Object>) getIntent().getSerializableExtra("productlist");
        OrderHisDetailChildADapter adapter = new OrderHisDetailChildADapter(OrderChildeViewDetails.this, detailsarr);
        productlist.setAdapter(adapter);
    }

    @Override
    public void onClick(View view)
    {
        if(view ==close)
        {
            finish();
        }

    }
}
