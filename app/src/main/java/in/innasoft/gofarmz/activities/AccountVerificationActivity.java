package in.innasoft.gofarmz.activities;

import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chaos.view.PinView;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class AccountVerificationActivity extends AppCompatActivity implements View.OnClickListener {

    TextView otp_txt, txtresend;
    PinView pinView;
    Button verify_btn;
    Typeface light, regular, bold;
    private boolean checkInternet;
    String send_mobile, deviceId,fromWhere;
    UserSessionManager userSessionManager;
    RotateLoading progress_indicator;
    SmsVerifyCatcher smsVerifyCatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_verification);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        send_mobile = bundle.getString("mobile");
        fromWhere = bundle.getString("activity");
        Log.d("DDDDD","///"+send_mobile+"///"+fromWhere);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);

        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));

        progress_indicator = findViewById(R.id.progress_indicator);
      //  progress_indicator.start();

        otp_txt = findViewById(R.id.otp_txt);
        otp_txt.setTypeface(bold);
        txtresend = findViewById(R.id.txtresend);
        txtresend.setPaintFlags(txtresend.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtresend.setTypeface(light);
        txtresend.setOnClickListener(this);

        pinView = findViewById(R.id.pinView);
        pinView.setTypeface(light);

        verify_btn = findViewById(R.id.verify_btn);
        verify_btn.setTypeface(light);
        verify_btn.setOnClickListener(this);
        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = message.replaceAll("[^0-9]", "");
                pinView.setText(code);
                Log.v("message >>> ", message.replaceAll("[^0-9]", ""));
            }
        });
        //getOTP();
    }

    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }


   /* @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }*/

    private void getOTP() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.RESENDOTP,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                if (status.equals("10100")) {
                                    progress_indicator.stop();
                                    String message = jsonObject.getString("message");
                                    Toast.makeText(AccountVerificationActivity.this, message, Toast.LENGTH_SHORT).show();

                                }
                                if (status.equals("10200")) {
                                    progress_indicator.stop();
                                    Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10300")) {
                                    progress_indicator.stop();
                                    Toast.makeText(getApplicationContext(), "User Not Exist..", Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10400")) {
                                    progress_indicator.stop();
                                    Toast.makeText(getApplicationContext(), "Unable to Send Otp..", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            error.getMessage();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("mobile", send_mobile);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("X-Platform", "ANDROID");
                    headers.put("X-Deviceid", deviceId);
                    return headers;
                }

            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(AccountVerificationActivity.this);
            requestQueue.add(stringRequest);
        } else {

            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onClick(View v) {

        if (v == verify_btn) {
            verifyOtp();
        }

        if (v == txtresend) {
            getOTP();
        }
    }

    private boolean validate() {
        boolean result = true;
        String otp = pinView.getText().toString().trim();
        if ((otp == null || otp.equals("")) || otp.length() != 6) {
            pinView.setError("Invalid OTP");
            result = false;
        }
        return result;
    }

    public void verifyOtp() {
        checkInternet = NetworkChecking.isConnected(this);
        if (validate()) {

            if (checkInternet) {
                Log.d("hsdfjkahs",AppUrls.BASE_URL + AppUrls.VERIFYOTP);

                StringRequest strveriFyOtp = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.VERIFYOTP, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.v("KKKKK","//"+response);
                            String successResponceCode = jsonObject.getString("status");
                            if (successResponceCode.equals("10100"))
                            {
                                progress_indicator.stop();
                                String message = jsonObject.getString("message");

                                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                String jwt = jsonObject1.getString("jwt");
                                String user_id = jsonObject1.getString("user_id");
                                String user_name = jsonObject1.getString("user_name");
                                String email = jsonObject1.getString("email");
                                String mobile = jsonObject1.getString("mobile");

                                Toast.makeText(AccountVerificationActivity.this, message, Toast.LENGTH_SHORT).show();
                                userSessionManager.createUserLoginSession(jwt, user_id, user_name, email, mobile);


                                if(fromWhere.equalsIgnoreCase("PAYMENTDETAIL"))
                                {
                                    Intent intent = new Intent(AccountVerificationActivity.this, PaymentDetails.class);
                                    intent.putExtra("activity",fromWhere);
                                    startActivity(intent);
                                }
                               else
                                {
                                    Intent intent = new Intent(AccountVerificationActivity.this, MainActivity.class);
                                    startActivity(intent);
                                }

                            }
                            if (successResponceCode.equals("10200")) {
                                progress_indicator.stop();
                                Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                            }

                            if (successResponceCode.equals("10300")) {
                                progress_indicator.stop();
                                Toast.makeText(getApplicationContext(), "Invalid OTP..!", Toast.LENGTH_SHORT).show();
                            }
                            if (successResponceCode.equals("10400")) {
                                progress_indicator.stop();
                                Toast.makeText(getApplicationContext(), "Unable to handle your request, Try again..!", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        error.getMessage();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("otp", pinView.getText().toString());
                        params.put("mobile", send_mobile);
                        params.put("browser_id", deviceId);
                        Log.v("VERYOTPPARAMS","//"+params.toString());
                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("X-Platform", "ANDROID");
                        headers.put("X-Deviceid", deviceId);
                        return headers;
                    }
                };
                strveriFyOtp.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(AccountVerificationActivity.this);
                requestQueue.add(strveriFyOtp);
            } else {
                Toast.makeText(AccountVerificationActivity.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
    }
}
