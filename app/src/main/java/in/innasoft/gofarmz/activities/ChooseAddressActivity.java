package in.innasoft.gofarmz.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapters.AddAreasAdapter;
import in.innasoft.gofarmz.adapters.AddCitiesAdapter;
import in.innasoft.gofarmz.adapters.AddCountriesAdapter;
import in.innasoft.gofarmz.adapters.AddStatesAdapter;
import in.innasoft.gofarmz.adapters.AddressesAdapter;
import in.innasoft.gofarmz.models.AddressesModel;
import in.innasoft.gofarmz.models.AreasModel;
import in.innasoft.gofarmz.models.CitiesModel;
import in.innasoft.gofarmz.models.CountriesModel;
import in.innasoft.gofarmz.models.StatesModel;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.DividerItemDecorator;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class ChooseAddressActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private static final int PLACE_PICKER_REQUEST = 1;
    String latitude, longitude, finaladdress;
    ImageView close_img, search_location, plus_img, no_addresses_img;
    TextView address_txt, add_tittle_txt, prev_txt, default_title;
    ScrollView scrollView;
    RelativeLayout add_form_rl;
    EditText name_edt, address_one_edt, address_two_edt, pincode_edt, mobile_edt, alt_mobile_edt, area_edt, city_edt, state_edt, country_edt;
    TextInputLayout  name_til,address_one_til,address_two_til,pincode_til,mobile_til,alt_mobile_til,area_til,city_til,state_til,country_til;
    Button add_new_address_btn;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token, user_name, mobile, send_default_value;
    private boolean checkInternet;
    RecyclerView prev_recyclerview;
    AddressesAdapter addressesAdapter;
    ArrayList<AddressesModel> addressesModelArrayList = new ArrayList<AddressesModel>();

    RadioGroup rg_default;
    RadioButton yes_rb, no_rb;

    AlertDialog dialog, dialog2, dialog3, dialog4;
    /*countries*/
    AddCountriesAdapter countriesAdapter;
    ArrayList<CountriesModel> countriesModels = new ArrayList<CountriesModel>();
    ArrayList<String> countriesList = new ArrayList<String>();

    /*states*/
    AddStatesAdapter statesAdapter;
    ArrayList<StatesModel> statesModels = new ArrayList<StatesModel>();
    ArrayList<String> statesList = new ArrayList<String>();

    /*cities*/
    AddCitiesAdapter citiesAdapter;
    ArrayList<CitiesModel> citiesModels = new ArrayList<CitiesModel>();
    ArrayList<String> citiesList = new ArrayList<String>();

    /*areas*/
    AddAreasAdapter areasAdapter;
    ArrayList<AreasModel> areasModels = new ArrayList<AreasModel>();
    ArrayList<String> areasList = new ArrayList<String>();

    String sendCountryName, countryId, sendStateName, stateId, sendCityName, cityId, sendAreaName, areaId;
    Geocoder geocoder;
    List<Address> addresses;

    Typeface light, regular, bold;
    RotateLoading progress_indicator;
    ProgressDialog pprogressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_address);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_name = userDetails.get(UserSessionManager.USER_NAME);
        mobile = userDetails.get(UserSessionManager.USER_MOBILE);
        Log.d("SESSIONDATA", deviceId + "/n" + user_id + "/n" + token);

        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));

        pprogressDialog = new ProgressDialog(ChooseAddressActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
       // progress_indicator = findViewById(R.id.progress_indicator);
     //   progress_indicator.start();

        geocoder = new Geocoder(this, Locale.getDefault());

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        search_location = findViewById(R.id.search_location);
        search_location.setOnClickListener(this);
        plus_img = findViewById(R.id.plus_img);
        plus_img.setOnClickListener(this);
        no_addresses_img = findViewById(R.id.no_addresses_img);

        scrollView = findViewById(R.id.scrollView);
        add_form_rl = findViewById(R.id.add_form_rl);
        address_txt = findViewById(R.id.address_txt);
        address_txt.setTypeface(light);
        address_txt.setOnClickListener(this);
        add_tittle_txt = findViewById(R.id.add_tittle_txt);
        add_tittle_txt.setTypeface(bold);
        add_tittle_txt.setOnClickListener(this);
        prev_txt = findViewById(R.id.prev_txt);
        prev_txt.setTypeface(light);
        prev_txt.setOnClickListener(this);
        name_edt = findViewById(R.id.name_edt);
        name_edt.setTypeface(light);
        address_one_edt = findViewById(R.id.address_one_edt);
        address_one_edt.setTypeface(light);
        address_two_edt = findViewById(R.id.address_two_edt);
        address_two_edt.setTypeface(light);
        area_edt = findViewById(R.id.area_edt);
        area_edt.setTypeface(light);
        city_edt = findViewById(R.id.city_edt);
        city_edt.setTypeface(light);
        state_edt = findViewById(R.id.state_edt);
        state_edt.setTypeface(light);
        country_edt = findViewById(R.id.country_edt);
        country_edt.setTypeface(light);
        pincode_edt = findViewById(R.id.pincode_edt);
        pincode_edt.setTypeface(light);
        mobile_edt = findViewById(R.id.mobile_edt);
        mobile_edt.setTypeface(light);
        alt_mobile_edt = findViewById(R.id.alt_mobile_edt);
        alt_mobile_edt.setTypeface(light);

        name_til = findViewById(R.id.name_til);
        name_til.setTypeface(light);
        address_one_til = findViewById(R.id.address_one_til);
        address_one_til.setTypeface(light);
        address_two_til = findViewById(R.id.address_two_til);
        address_two_til.setTypeface(light);
        pincode_til = findViewById(R.id.pincode_til);
        pincode_til.setTypeface(light);
        mobile_til = findViewById(R.id.mobile_til);
        mobile_til.setTypeface(light);
        alt_mobile_til = findViewById(R.id.alt_mobile_til);
        alt_mobile_til.setTypeface(light);
        area_til = findViewById(R.id.area_til);
        area_til.setTypeface(light);
        city_til = findViewById(R.id.city_til);
        city_til.setTypeface(light);
        state_til = findViewById(R.id.state_til);
        state_til.setTypeface(light);
        country_til = findViewById(R.id.country_til);
        country_til.setTypeface(light);


        default_title = findViewById(R.id.default_title);
        default_title.setTypeface(light);


        prev_recyclerview = findViewById(R.id.prev_recyclerview);
        prev_recyclerview.setHasFixedSize(true);

        add_new_address_btn = findViewById(R.id.add_new_address_btn);
        add_new_address_btn.setTypeface(bold);
        add_new_address_btn.setOnClickListener(this);
      //  add_new_address_btn.setEnabled(false);
        rg_default = findViewById(R.id.rg_default);
        rg_default.setOnCheckedChangeListener(this);
        yes_rb = findViewById(R.id.yes_rb);
        yes_rb.setTypeface(light);
        no_rb = findViewById(R.id.no_rb);
        no_rb.setTypeface(light);
        send_default_value = "Yes";
        getCountries();
        getAddresses();
    }

    private void getCountries() {
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {
            countriesList.clear();
            countriesModels.clear();
            statesModels.clear();
            //progressDialog.show();
            String url = AppUrls.BASE_URL + AppUrls.GETCOUNTRIES;
            Log.d("LISTOFINURL", url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("FAVOURTRESPONSE:", response);

                                String status = jsonObject.getString("status");
                                if (status.equals("10100")) {
                                   // progress_indicator.stop();

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        CountriesModel cm = new CountriesModel();
                                        cm.setId(jsonObject1.getString("id"));
                                        String name = jsonObject1.getString("name");
                                        cm.setName(name);
                                        countriesList.add(name);
                                        countriesModels.add(cm);
                                    }
                                } else {
                                    //progress_indicator.stop();
                                }

                            } catch (JSONException e) {
                               // progress_indicator.stop();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                          //  progress_indicator.stop();
                           error.getMessage();
                        }
                    }) {
            };
            RequestQueue requestQueue = Volley.newRequestQueue(ChooseAddressActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    public void getAddresses() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();
            Log.d("teer",AppUrls.BASE_URL + AppUrls.ADDADDRESS + user_id + "/addresses");
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.ADDADDRESS + user_id + "/addresses",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                        Log.d("MMMM",response);
                            try {
                                addressesModelArrayList.clear();
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                if (status.equals("10100")) {
                                    no_addresses_img.setVisibility(View.GONE);

                                    pprogressDialog.dismiss();
                                   // progress_indicator.stop();
                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                        AddressesModel am = new AddressesModel();
                                        am.setId(jsonObject1.getString("id"));
                                        am.setName(jsonObject1.getString("name"));
                                        am.setAddress_line1(jsonObject1.getString("address_line1"));
                                        am.setAddress_line2(jsonObject1.getString("address_line2"));
                                        am.setArea(jsonObject1.getString("area"));
                                        am.setCity(jsonObject1.getString("city"));
                                        am.setState(jsonObject1.getString("state"));
                                        am.setPincode(jsonObject1.getString("pincode"));
                                        am.setContact_no(jsonObject1.getString("contact_no"));
                                        am.setAlternate_contact_no(jsonObject1.getString("alternate_contact_no"));
                                        addressesModelArrayList.add(am);
                                    }
                                    addressesAdapter = new AddressesAdapter(addressesModelArrayList, ChooseAddressActivity.this, R.layout.row_addresses);
                                    prev_recyclerview.setLayoutManager(new LinearLayoutManager(ChooseAddressActivity.this, LinearLayoutManager.VERTICAL, false));
                                    prev_recyclerview.setItemAnimator(new DefaultItemAnimator());
                                    RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(ChooseAddressActivity.this, R.drawable.recycler_view_divider));
                                    prev_recyclerview.addItemDecoration(dividerItemDecoration);
                                    prev_recyclerview.setAdapter(addressesAdapter);
                                   // prev_recyclerview.setAdapter(addressesAdapter);
                                } else {
                                    pprogressDialog.dismiss();
                                    //progress_indicator.stop();
                                    no_addresses_img.setVisibility(View.VISIBLE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.getMessage();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("X-Platform", "ANDROID");
                    headers.put("X-Deviceid", deviceId);
                    headers.put("Authorization-Basic", token);
                    return headers;
                }

            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ChooseAddressActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "Please Check Your Intenet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PLACE_PICKER_REQUEST && resultCode == Activity.RESULT_OK) {

            pprogressDialog.dismiss();
           // progress_indicator.stop();

            address_txt.setVisibility(View.GONE);
            search_location.setVisibility(View.GONE);
            add_tittle_txt.setVisibility(View.GONE);
            plus_img.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);
            prev_txt.setVisibility(View.GONE);
            prev_recyclerview.setVisibility(View.GONE);


            final Place place = PlacePicker.getPlace(this, data);
            String stringlat = place.getLatLng().toString();
            CharSequence name = place.getName();
            final CharSequence address = place.getAddress();
            String attributions = (String) place.getAttributions();
            if (attributions == null) {
                attributions = "";
            }
            if (name.toString().contains("°")) {
                name = "";
            }
            stringlat = stringlat.substring(stringlat.indexOf("(") + 1);
            stringlat = stringlat.substring(0, stringlat.indexOf(")"));
            String latValue = stringlat.split(",")[0];
            latitude = latValue;
            String lngValue = stringlat.split(",")[1];
            longitude = lngValue;

            Double lat = Double.valueOf(latitude);
            Double lng = Double.valueOf(longitude);
            String fulladd = address.toString();
            String addr[] = fulladd.split(",", 2);
            if (name != null && name.length() > 0)
                address_one_edt.setText(name);
            else
                address_one_edt.setText(addr[0]);


            //area_edt.setText(addr[5]+","+addr[6]+","+addr[7]);
            ////#2-56/2/19, 3rd Floor, Vijaya Towers, near Meridian School,, Ayyappa Society 100ft Road, Madhapur, Ayyappa Society, Chanda Naik Nagar, Madhapur, Hyderabad, Telangana 500081, India
            try {
                addresses = geocoder.getFromLocation(lat, lng, 1);
            //    add_new_address_btn.setEnabled(true);
                String addLine = addresses.get(0).getAddressLine(0);
                String city = addresses.get(0).getLocality();
                city_edt.setText(city);
                String area = addresses.get(0).getSubLocality();
                area_edt.setText(area);
                String state = addresses.get(0).getAdminArea();
                state_edt.setText(state);
                String country = addresses.get(0).getCountryName();
                country_edt.setText(country);
                String postalCode = addresses.get(0).getPostalCode();
                pincode_edt.setText(postalCode);
                String addLineTwo = addresses.get(0).getThoroughfare();
                // address_two_edt.setText(addLineTwo);
                String addLineOne = addresses.get(0).getFeatureName();
                // address_one_edt.setText(addLineOne);
                if (addr[1].contains(postalCode)) {
                    addr[1] = addr[1].replace(postalCode, " ");
                }
                if (addr[1].contains(state)) {
                    addr[1] = addr[1].replace(state, " ");
                }
                if (addr[1].contains(country)) {
                    addr[1] = addr[1].replace(country, " ");
                }
                if (addr[1].contains(city)) {
                    addr[1] = addr[1].replace(city, " ");
                }
                if (addr[1] != null && area != null && addr[1].contains(area)) {
                    addr[1] = addr[1].replace(area, " ");
                }
                addr[1] = new LinkedHashSet<String>(Arrays.asList(addr[1].split("\\s"))).toString().replaceAll("[\\[\\],]", "");
                address_two_edt.setText(addr[1]);
                name_edt.setText(user_name);
              if(!mobile.equalsIgnoreCase("null"))
                mobile_edt.setText(mobile);

                Log.d("ADDRESSES", addLine + "\n" + city + "\n" + state + "\n" + country + "\n" + postalCode + "\n" + address_one_edt);
                Log.d("REMAININGADDRESS", addresses.get(0).getSubAdminArea() + "\n" + addresses.get(0).getSubLocality() + "\n" + addresses.get(0).getPremises() + "\n" + addresses.get(0).getSubThoroughfare()
                        + "\n" + addresses.get(0).getThoroughfare());
                Log.v("FFFFFFF", "//" + address.toString());
                //address_txt.setText(address);
                finaladdress = address.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }


        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }

    }


    @Override
    public void onClick(View v) {

        if (v == close_img) {

            Intent intent = new Intent(ChooseAddressActivity.this, PaymentDetails.class);
            intent.putExtra("activity", "ChooseAddressActivity");
            startActivity(intent);
        }

        if (v == address_txt) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                no_addresses_img.setVisibility(View.GONE);
                add_tittle_txt.setVisibility(View.GONE);
                plus_img.setVisibility(View.GONE);
                prev_txt.setVisibility(View.GONE);
                prev_recyclerview.setVisibility(View.GONE);
             //   add_new_address_btn.setEnabled(true);

                try {
                    PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
                    Intent intent = intentBuilder.build(ChooseAddressActivity.this);
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);

                } catch (GooglePlayServicesRepairableException
                        | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }

        if (v == search_location) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {
                no_addresses_img.setVisibility(View.GONE);
                add_tittle_txt.setVisibility(View.GONE);
                plus_img.setVisibility(View.GONE);
                prev_txt.setVisibility(View.GONE);
                prev_recyclerview.setVisibility(View.GONE);
              //  add_new_address_btn.setEnabled(true);
                try {
                    PlacePicker.IntentBuilder intentBuilder = new PlacePicker.IntentBuilder();
                    Intent intent = intentBuilder.build(ChooseAddressActivity.this);
                    startActivityForResult(intent, PLACE_PICKER_REQUEST);

                } catch (GooglePlayServicesRepairableException
                        | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }

        if (v == add_tittle_txt) {
            no_addresses_img.setVisibility(View.GONE);
            address_txt.setVisibility(View.GONE);
            search_location.setVisibility(View.GONE);
            prev_txt.setVisibility(View.GONE);
            prev_recyclerview.setVisibility(View.GONE);
         //   add_new_address_btn.setEnabled(true);
            scrollView.setVisibility(View.VISIBLE);
        }

        if (v == plus_img) {
            no_addresses_img.setVisibility(View.GONE);
            address_txt.setVisibility(View.GONE);
            search_location.setVisibility(View.GONE);
            prev_txt.setVisibility(View.GONE);
            prev_recyclerview.setVisibility(View.GONE);
         //   add_new_address_btn.setEnabled(true);
            scrollView.setVisibility(View.VISIBLE);
        }

        if (v == add_new_address_btn)
        {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet)
            {
                Intent intent = new Intent(ChooseAddressActivity.this, TestActivity.class);
                startActivity(intent);

            }
            else
            {
                Toast.makeText(this, "Please Check Your Intenet Connection", Toast.LENGTH_SHORT).show();
            }
             /* if(validate())
              {
                  Log.d("SAVEURL", AppUrls.BASE_URL + AppUrls.ADDADDRESS + user_id + "/addresses");
                  StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.ADDADDRESS + user_id + "/addresses",
                          new Response.Listener<String>() {
                              @Override
                              public void onResponse(String response) {

                                  Log.d("SAVERESP", response);
                                  try {
                                      JSONObject jsonObject = new JSONObject(response);
                                      String status = jsonObject.getString("status");
                                      String message = jsonObject.getString("message");
                                      if (status.equals("10100")) {
                                          progress_indicator.stop();

                                          Toast.makeText(ChooseAddressActivity.this, message, Toast.LENGTH_SHORT).show();
                                          no_addresses_img.setVisibility(View.GONE);
                                          address_txt.setVisibility(View.VISIBLE);
                                          search_location.setVisibility(View.VISIBLE);
                                          prev_txt.setVisibility(View.VISIBLE);
                                          prev_recyclerview.setVisibility(View.VISIBLE);
                                          save_btn.setEnabled(false);
                                          add_tittle_txt.setVisibility(View.VISIBLE);
                                          plus_img.setVisibility(View.VISIBLE);
                                          scrollView.setVisibility(View.GONE);
                                          getAddresses();

                                     *//*   Intent intent = new Intent(ChooseAddressActivity.this, ChooseAddressActivity.class);
                                        startActivity(intent);*//*

                                      }
                                      if (status.equals("10200")) {
                                          progress_indicator.stop();
                                          Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                      }
                                      if (status.equals("10300")) {
                                          progress_indicator.stop();
                                          Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                      }
                                      if (status.equals("10400")) {
                                          progress_indicator.stop();
                                          Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                      }
                                      if (status.equals("10500")) {
                                          progress_indicator.stop();
                                          Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                      }


                                  } catch (JSONException e) {
                                      e.printStackTrace();

                                  }
                              }
                          },
                          new Response.ErrorListener() {
                              @Override
                              public void onErrorResponse(VolleyError error) {

                                  error.getMessage();
                              }
                          }) {
                      @Override
                      protected Map<String, String> getParams() {
                          Map<String, String> params = new HashMap<String, String>();
                          params.put("name", name_edt.getText().toString());
                          params.put("address_line1", address_one_edt.getText().toString());
                          params.put("address_line2", address_two_edt.getText().toString());
                          params.put("area", area_edt.getText().toString());
                          params.put("city", city_edt.getText().toString());
                          params.put("state", state_edt.getText().toString());
                          params.put("pincode", pincode_edt.getText().toString());
                          params.put("contact_no", mobile_edt.getText().toString());
                          params.put("alternate_contact_no", alt_mobile_edt.getText().toString());
                          params.put("is_default", send_default_value);
                          if (latitude != null && latitude.length() > 0) {
                              params.put("latitude", latitude);
                              params.put("longitude", longitude);
                          }
                          Log.d("SAVEPARAM", params.toString());
                          return params;
                      }

                      @Override
                      public Map<String, String> getHeaders() {
                          Map<String, String> headers = new HashMap<>();
                          headers.put("Authorization-Basic", token);
                          headers.put("X-Deviceid", deviceId);
                          headers.put("x-device-platform", "ANDROID");
                          return headers;
                      }
                  };
                  stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                  RequestQueue requestQueue = Volley.newRequestQueue(ChooseAddressActivity.this);
                  requestQueue.add(stringRequest);
              }


            }
            else {
                Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }*/
        }

        /*if (v == country_txt){
            statesModels.clear();
            //state_txt.setText("Select State");
            countryDialog();
        }

        if (v == state_txt){
            String country = country_txt.getText().toString().trim();
            if (!country.equals("Select Country")) {
                if (statesModels.size() != 0) {
                    stateDialog();

                } else {
                    Toast.makeText(this, "No States Found..!", Toast.LENGTH_SHORT).show();
                }
            }else {
                Toast.makeText(this, "Select Country", Toast.LENGTH_SHORT).show();
            }
        }

        if (v == city_txt){
            String state = state_txt.getText().toString().trim();
            if (!state.equals("Select State")) {
                if (citiesModels.size() != 0) {
                    cityDialog();

                } else {
                    Toast.makeText(this, "No Cities Found..!", Toast.LENGTH_SHORT).show();
                }
            }else {
                Toast.makeText(this, "Select State", Toast.LENGTH_SHORT).show();
            }
        }

        if (v == area_txt){
            String city = city_txt.getText().toString().trim();
            if (!city.equals("Select City")) {
                if (areasModels.size() != 0) {
                    areaDialog();

                } else {
                    Toast.makeText(this, "No Areas Found..!", Toast.LENGTH_SHORT).show();
                }
            }else {
                Toast.makeText(this, "Select City", Toast.LENGTH_SHORT).show();
            }
        }*/
    }

    private void countryDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ChooseAddressActivity.this);
        LayoutInflater inflater = ChooseAddressActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.countries_dialog, null);

        final TextView country_title_txt = dialog_layout.findViewById(R.id.country_title_txt);

        final SearchView country_search = dialog_layout.findViewById(R.id.country_search);
        EditText searchEditText = country_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        RecyclerView country_recyclerview = dialog_layout.findViewById(R.id.country_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        country_recyclerview.setLayoutManager(layoutManager);
        countriesAdapter = new AddCountriesAdapter(countriesModels, ChooseAddressActivity.this, R.layout.row_countries_list);
        country_recyclerview.setAdapter(countriesAdapter);

        country_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                country_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                country_title_txt.setText("Select Country");
                //progressDialog.dismiss();
                dialogInterface.dismiss();
            }
        });

        dialog = builder.create();

        country_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                countriesAdapter.getFilter().filter(query);
                return false;
            }
        });
        dialog.show();
    }

    private void stateDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ChooseAddressActivity.this);
        LayoutInflater inflater = ChooseAddressActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.states_dialog, null);

        TextView state_title = dialog_layout.findViewById(R.id.state_title);
        final SearchView state_search = dialog_layout.findViewById(R.id.state_search);
        EditText searchEditText = state_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        RecyclerView state_recyclerview = (RecyclerView) dialog_layout.findViewById(R.id.state_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        state_recyclerview.setLayoutManager(layoutManager);

        statesAdapter = new AddStatesAdapter(statesModels, ChooseAddressActivity.this, R.layout.row_states_list);

        state_recyclerview.setAdapter(statesAdapter);
        state_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                state_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("Done", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //progressDialog.dismiss();
                dialogInterface.dismiss();
            }
        });

        dialog2 = builder.create();

        state_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                statesAdapter.getFilter().filter(query);
                return false;
            }
        });
        dialog2.show();

    }

    private void cityDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ChooseAddressActivity.this);
        LayoutInflater inflater = ChooseAddressActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.cities_dialog, null);

        TextView city_title = dialog_layout.findViewById(R.id.city_title);
        final SearchView city_search = dialog_layout.findViewById(R.id.city_search);
        EditText searchEditText = city_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        RecyclerView city_recyclerview = dialog_layout.findViewById(R.id.city_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        city_recyclerview.setLayoutManager(layoutManager);

        citiesAdapter = new AddCitiesAdapter(citiesModels, ChooseAddressActivity.this, R.layout.row_cities_list);

        city_recyclerview.setAdapter(citiesAdapter);
        city_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                city_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("Done", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //progressDialog.dismiss();
                dialogInterface.dismiss();
            }
        });

        dialog3 = builder.create();

        city_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                statesAdapter.getFilter().filter(query);
                return false;
            }
        });
        dialog3.show();

    }

    private void areaDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ChooseAddressActivity.this);
        LayoutInflater inflater = ChooseAddressActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.areas_dialog, null);

        TextView area_title = dialog_layout.findViewById(R.id.area_title);
        final SearchView area_search = dialog_layout.findViewById(R.id.area_search);
        EditText searchEditText = area_search.findViewById(android.support.v7.appcompat.R.id.search_src_text);

        RecyclerView area_recyclerview = dialog_layout.findViewById(R.id.area_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        area_recyclerview.setLayoutManager(layoutManager);

        areasAdapter = new AddAreasAdapter(areasModels, ChooseAddressActivity.this, R.layout.row_areas_list);

        area_recyclerview.setAdapter(areasAdapter);
        area_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                area_search.setIconified(false);
            }
        });

        builder.setView(dialog_layout).setNegativeButton("Done", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //progressDialog.dismiss();
                dialogInterface.dismiss();
            }
        });

        dialog4 = builder.create();

        area_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                statesAdapter.getFilter().filter(query);
                return false;
            }
        });
        dialog4.show();

    }

    public void setCountryName(String categoryName, String category_id) {

        dialog.dismiss();
        countriesAdapter.notifyDataSetChanged();
        sendCountryName = categoryName;
        countryId = category_id;
        country_edt.setText(sendCountryName);
        getStatesData(category_id);

        Log.d("COUNTRYDETAIL:", sendCountryName + "::::" + countryId);
    }

    private void getStatesData(String cID) {
        statesModels.clear();
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {

            StringRequest string_request = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GETSTATES + cID,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            String repo = response.toString();
                            Log.e("SUBCATEGORY", "RESPCITY:" + repo);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("Subcategory:", response);

                                String responceCode = jsonObject.getString("status");

                                if (responceCode.equals("10100")) {
                                 //   progress_indicator.stop();

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        StatesModel sm = new StatesModel();
                                        String id = jsonObject1.getString("id");
                                        String name = jsonObject1.getString("name");
                                        sm.setId(id);
                                        sm.setName(name);

                                        statesList.add(name);
                                        statesModels.add(sm);
                                    }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                              //  progress_indicator.stop();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                  //  progress_indicator.stop();
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(string_request);
        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    public void setStateName(String stateName, String state_id) {

        dialog2.dismiss();
        statesAdapter.notifyDataSetChanged();
        sendStateName = stateName;
        stateId = state_id;
        state_edt.setText(sendStateName);
        getCitiesData(state_id);

        Log.d("COUNTRYDETAIL:", sendCountryName + "::::" + countryId);
    }

    private void getCitiesData(String cID) {
        citiesModels.clear();
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {

            StringRequest string_request = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GETCITIES + cID,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            String repo = response.toString();
                            Log.e("SUBCATEGORY", "RESPCITY:" + repo);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("Subcategory:", response);

                                String responceCode = jsonObject.getString("status");

                                if (responceCode.equals("10100")) {
                                 //   progress_indicator.stop();

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        CitiesModel cm = new CitiesModel();
                                        String id = jsonObject1.getString("id");
                                        String name = jsonObject1.getString("name");
                                        cm.setId(id);
                                        cm.setName(name);

                                        citiesList.add(name);
                                        citiesModels.add(cm);
                                    }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                               // progress_indicator.stop();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                   // progress_indicator.stop();
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(string_request);
        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    public void setCityName(String cityName, String city_id) {

        dialog3.dismiss();
        citiesAdapter.notifyDataSetChanged();
        sendCityName = cityName;
        cityId = city_id;
        city_edt.setText(sendCityName);
        getAreasData(city_id);

        Log.d("COUNTRYDETAIL:", sendCountryName + "::::" + countryId);
    }

    private void getAreasData(String cID) {
        citiesModels.clear();
        checkInternet = NetworkChecking.isConnected(getApplicationContext());
        if (checkInternet) {

            StringRequest string_request = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GETAREAS + cID,
                    new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {

                            String repo = response.toString();
                            Log.e("SUBCATEGORY", "RESPCITY:" + repo);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("Subcategory:", response);

                                String responceCode = jsonObject.getString("status");

                                if (responceCode.equals("10100")) {
                                   // progress_indicator.stop();

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        AreasModel am = new AreasModel();
                                        String id = jsonObject1.getString("id");
                                        String name = jsonObject1.getString("name");
                                        am.setId(id);
                                        am.setName(name);

                                        areasList.add(name);
                                        areasModels.add(am);
                                    }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                               // progress_indicator.stop();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                  //  progress_indicator.stop();
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(string_request);
        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection...!", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ChooseAddressActivity.this, PaymentDetails.class);
        intent.putExtra("activity", "ChooseAddressActivity");

        startActivity(intent);
    }


    public void setAreaName(String areaName, String area_id) {

        dialog4.dismiss();
        areasAdapter.notifyDataSetChanged();
        sendAreaName = areaName;
        areaId = area_id;
        area_edt.setText(sendAreaName);
        Log.d("COUNTRYDETAIL:", sendCountryName + "::::" + countryId);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {


        if (checkedId == R.id.yes_rb) {
            yes_rb.setChecked(true);
            send_default_value = "Yes";
        } else {
            no_rb.setChecked(true);
            send_default_value = "No";
        }
    }

    public boolean validate()
    {
        boolean result = true;

        String name = name_edt.getText().toString();
        if (name.isEmpty() || name.equals("") || name.equals(null)) {
            name_til.setError("Please Enter Name");
            result = false;
        } else {
          //  name_edt.setError("Please Enter Name");
        }

        String addline_one = address_one_edt.getText().toString();
        if (addline_one.isEmpty() || addline_one.equals("") || addline_one.equals(null)) {
            address_one_til.setError("Please Enter Address 1");
            result = false;
        } else {
         //   address_one_edt.setError("Please Enter Address 1");
        }

        String addline_two = address_two_edt.getText().toString();
        if (addline_two.isEmpty() || addline_two.equals("") || addline_two.equals(null)) {
            address_two_til.setError("Please Enter Address 2");
            result = false;
        } else {
           // address_two_edt.setError("Please Enter Address 2");
        }

        String area = area_edt.getText().toString();
        if (area.isEmpty() || area.equals("") || area.equals(null)) {
            area_til.setError("Please Enter Area");
            result = false;
        } else {
          //  area_edt.setError("Please Enter Area");
        }

        String city = city_edt.getText().toString();
        if (city.isEmpty() || city.equals("") || city.equals(null)) {
            city_til.setError("Please Enter City");
            result = false;
        } else {
           // city_edt.setError("Please Enter City");
        }

        String state = state_edt.getText().toString();
        if (state.isEmpty() || state.equals("") || state.equals(null)) {
            state_til.setError("Please Enter State");
            result = false;
        } else {
           // state_edt.setError("Please Enter State");
        }

        String country = country_edt.getText().toString();
        if (country.isEmpty() || country.equals("") || country.equals(null)) {
            country_til.setError("Please Enter Country");
            result = false;
        } else {
          //  country_edt.setError("Please Enter Country");
        }

        String pincode = pincode_edt.getText().toString();
        if (pincode.isEmpty() || pincode.equals("") || pincode.equals(null)|| pincode.length() != 6) {
            pincode_til.setError("Please Enter Pincode");
            result = false;
        } else {
          //  pincode_edt.setError("Please Enter Pincode");
        }

        String mobile = mobile_edt.getText().toString();
        if (mobile.isEmpty() || mobile.equals("") || mobile.equals(null)) {
            mobile_til.setError("Please Enter Mobile");
            result = false;
        } else {
           // mobile_edt.setError("Please Enter Mobile");
        }
        return result;
    }
}
