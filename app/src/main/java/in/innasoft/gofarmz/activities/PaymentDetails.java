package in.innasoft.gofarmz.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapters.MyProductDetailsAdapter;
import in.innasoft.gofarmz.adapters.PaymentTypeAdapter;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.DividerItemDecorator;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class PaymentDetails extends AppCompatActivity implements View.OnClickListener {

    TextView manage_address_txt,addAddress_txt,delivery_title_txt, delivery_detail_txt, product_title_txt, payment_title_txt, add_coupan_code, price_title, pricetxt, coupan_code_txt, discount_title,
            discounttxt, delivery_title, dileverytxt,deliverytext_nocharge, totlaPrice_title, totlaPricetxt, payment_type_txt, promo_code_txt,allsponsors_toolbar_title,expected_delevry_title,expected_delevry_date;
    Button  next_btn;
    RecyclerView product_details_recyclerview, payment_details_recyclerview;
    MyProductDetailsAdapter myProductDetailsAdapter;
    ArrayList<Object> recordList = new ArrayList<Object>();
    ArrayList<Object> defaultAddressList = new ArrayList<Object>();
    ArrayList<Object> recordChildList = new ArrayList<Object>();
    UserSessionManager userSessionManager;
    String activity, id, promoStatus, deviceId, user_id, token, coupon_code;
    private boolean checkInternet;
    Dialog dialog;
    RelativeLayout delivery_ll;
    ArrayList<Object> orderDetailProduct = new ArrayList<>();
    RecyclerView payment_type_recyclerview;
    PaymentTypeAdapter paymentTypeAdapter;
    Double final_amount, final_discount, Totalprice;
    String delivery_charges, adress_id, appliedCoupon_amount;
    Typeface light, regular, bold;
    ImageView close_img,remove_coupon_code;

    TableRow row_coupons;
    Double total_price_after_coupon,total_after_remove_coupan;
    SweetAlertDialog sd,sd_not_verify,sd_orderClose;
    String  order_id,mobile_verifyed,contact_no,expected_delevry,users_mobile,orderCloseStatText;
    Double orderCloseStatus;
    ProgressDialog pprogressDialog;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;



    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      //  setContentView(R.layout.activity_payment_details);
        setContentView(R.layout.dummy_payment_detail_detail);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));

        pprogressDialog = new ProgressDialog(PaymentDetails.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        checkInternet = NetworkChecking.isConnected(this);
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        activity = bundle.getString("activity");
        assert activity != null;
        if (activity.equalsIgnoreCase("ChooseAddressAdapter")) {
            id = bundle.getString("id");
            contact_no = bundle.getString("contact_no");
        //    addID = bundle.getString("id");
            //delAddress();
        }


        delivery_ll = findViewById(R.id.delivery_ll);
        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        allsponsors_toolbar_title = findViewById(R.id.allsponsors_toolbar_title);
        allsponsors_toolbar_title.setTypeface(bold);
        delivery_title_txt = findViewById(R.id.delivery_title_txt);
        delivery_title_txt.setTypeface(bold);
        delivery_detail_txt = findViewById(R.id.delivery_detail_txt);
        delivery_detail_txt.setTypeface(light);
        product_title_txt = findViewById(R.id.product_title_txt);
        product_title_txt.setTypeface(bold);
        payment_title_txt = findViewById(R.id.payment_title_txt);
        payment_title_txt.setTypeface(bold);
        payment_type_txt = findViewById(R.id.payment_type_txt);
        payment_type_txt.setTypeface(bold);


        expected_delevry_title = findViewById(R.id.expected_delevry_title);
        expected_delevry_title.setTypeface(light);
        expected_delevry_date = findViewById(R.id.expected_delevry_date);
        expected_delevry_date.setTypeface(light);

        promo_code_txt = findViewById(R.id.promo_code_txt);
        promo_code_txt.setPaintFlags(promo_code_txt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        promo_code_txt.setTypeface(bold);
        promo_code_txt.setOnClickListener(this);

        row_coupons = findViewById(R.id.row_coupons);

        manage_address_txt = findViewById(R.id.manage_address_txt);
        manage_address_txt.setTypeface(bold);
        manage_address_txt.setOnClickListener(this);

        addAddress_txt = findViewById(R.id.addAddress_txt);
        addAddress_txt.setTypeface(bold);
        addAddress_txt.setOnClickListener(this);

        next_btn = findViewById(R.id.next_btn);
        next_btn.setTypeface(bold);
        next_btn.setOnClickListener(this);

        price_title = findViewById(R.id.price_title);
        pricetxt = findViewById(R.id.pricetxt);
        add_coupan_code = findViewById(R.id.add_coupan_code);
        //add_coupan_code.setOnClickListener(this);
        coupan_code_txt = findViewById(R.id.coupan_code_txt);
        discount_title = findViewById(R.id.discount_title);
        discounttxt = findViewById(R.id.discounttxt);
        delivery_title = findViewById(R.id.delivery_title);
        dileverytxt = findViewById(R.id.dileverytxt);
        deliverytext_nocharge = findViewById(R.id.deliverytext_nocharge);
        totlaPrice_title = findViewById(R.id.totlaPrice_title);
        totlaPricetxt = findViewById(R.id.totlaPricetxt);
        price_title.setTypeface(light);

        add_coupan_code.setTypeface(light);
        discount_title.setTypeface(light);

        delivery_title.setTypeface(light);

        deliverytext_nocharge.setTypeface(light);
        totlaPrice_title.setTypeface(light);

        delivery_detail_txt.setTypeface(light);

        product_details_recyclerview = findViewById(R.id.product_details_recyclerview);
        payment_type_recyclerview = findViewById(R.id.payment_type_recyclerview);
        product_details_recyclerview.setHasFixedSize(true);
        product_details_recyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        product_details_recyclerview.setItemAnimator(new DefaultItemAnimator());
        myProductDetailsAdapter = new MyProductDetailsAdapter(PaymentDetails.this, recordList, R.layout.row_product_payment_detail);


        if (activity.equalsIgnoreCase("PromoCouponListActivity")) {
            promoStatus = bundle.getString("promoStatus");

            if (promoStatus.equalsIgnoreCase("0")) {
                promo_code_txt.setVisibility(View.VISIBLE);
                row_coupons.setVisibility(View.GONE);
            } else {
                promo_code_txt.setVisibility(View.GONE);
                row_coupons.setVisibility(View.VISIBLE);
            }
        }

        getCartData();
        getCheckOutData();
        coupan_code_txt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (coupan_code_txt.getRight() - coupan_code_txt.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        promo_code_txt.setVisibility(View.VISIBLE);
                       // remove_coupon_code.setVisibility(View.GONE);
                        row_coupons.setVisibility(View.GONE);
                        Log.d("ASSSSS",""+appliedCoupon_amount+"///////"+total_price_after_coupon);
                        total_after_remove_coupan=total_price_after_coupon+Double.parseDouble(appliedCoupon_amount);
                        totlaPricetxt.setText(""  +new DecimalFormat("##.##").format(total_after_remove_coupan)+ "/-");
                        return true;
                    }
                }
                return true;
            }
        });
    }

    private void delAddress()
    {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();
            String URL = AppUrls.BASE_URL + AppUrls.ADDADDRESS + user_id + "/addresses/" + id;
            Log.d("URL", URL);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.ADDADDRESS + user_id + "/addresses/" + id,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                Log.d("Res", response);
                                if (status.equals("10100")) {

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    id = jsonObject1.getString("id");
                                    String user_id = jsonObject1.getString("user_id");
                                    String user_name = jsonObject1.getString("name");
                                    String address_line1 = jsonObject1.getString("address_line1");
                                    String address_line2 = jsonObject1.getString("address_line2");
                                    String area = jsonObject1.getString("area");
                                    String city = jsonObject1.getString("city");
                                    String state = jsonObject1.getString("state");
                                    String country = jsonObject1.getString("country");
                                    String pincode = jsonObject1.getString("pincode");
                                    String contact_no = jsonObject1.getString("contact_no");
                                    String alternate_contact_no = jsonObject1.getString("alternate_contact_no");
                                    String created_on = jsonObject1.getString("created_on");
                                    String updated_on = jsonObject1.getString("updated_on");
                                      String deliveryaddress=user_name + "\n" + address_line1 + "," + address_line2 + "," + area + "," + city + "," + state + "," + country + "," + pincode + "\n" + contact_no + "\n" + alternate_contact_no;
                                    delivery_detail_txt.setText(deliveryaddress);
                                    dileverytxt.setText(jsonObject1.getString("delivery_charges"));
                                    SharedPreferences preferences=getSharedPreferences("amount",0);
                                    int amount=preferences.getInt("finalamount",0);
                                    final_amount=Double.valueOf(amount);
                                    delivery_charges=jsonObject1.getString("delivery_charges");
                                     Double total = final_amount + Double.parseDouble(delivery_charges);   //after applied coupon add delivery chages
                                    totlaPricetxt.setText("" + new DecimalFormat("##.##").format(total) + "/-");



                                }

                                if (status.equals("10200")) {

                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                                }

                                if (status.equals("10300")) {

                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "No Data Found..", Toast.LENGTH_SHORT).show();
                                }

                                if (status.equals("10400")) {

                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Unable to Send Otp..", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.getMessage();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
                    headers.put("X-Deviceid", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    return headers;
                }

            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(PaymentDetails.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(this, "Please Check Your Intenet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {

        if (v == close_img) {
            Intent intent = new Intent(PaymentDetails.this, DeliverySlotsActivity.class);
            intent.putExtra("activity","Home");
            startActivity(intent);

        }

        if (v == manage_address_txt) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet)
            {
                if(checkLocationPermission()) {
                    Intent intent = new Intent(PaymentDetails.this, ChooseAddressActivity.class);
                    startActivity(intent);
                }
            }
            else {
                Toast.makeText(this, "Please Check Your Intenet Connection", Toast.LENGTH_SHORT).show();
            }
        }
        if (v == addAddress_txt)
        {
            if(checkLocationPermission())
            {
                Intent intent = new Intent(PaymentDetails.this, TestActivity.class);
                startActivity(intent);
            }
        }

        if (v == promo_code_txt) {


            String deliveryaddress = delivery_detail_txt.getText().toString();
            if (deliveryaddress!=null && deliveryaddress.length()>0) {
                promo_code_txt.setVisibility(View.GONE);
                row_coupons.setVisibility(View.VISIBLE);
                coupan_code_txt.setVisibility(View.GONE);
                Intent promolist = new Intent(PaymentDetails.this, PromoCouponListActivity.class);
                promolist.putExtra("FINAL_AMOUNT", final_amount);
                startActivityForResult(promolist, 1);
            } else {

                Toast.makeText(PaymentDetails.this, "Select Your delivery details", Toast.LENGTH_SHORT).show();
            }

        }

        if (v == next_btn)
        {
            if (checkInternet)
            {
                if (addAddress_txt.getVisibility()==View.VISIBLE)
                {
                      Toast.makeText(PaymentDetails.this, "Add Your delivery Address..!", Toast.LENGTH_SHORT).show();
                }

                else if(mobile_verifyed.equalsIgnoreCase("0"))
                {
                    if(!(users_mobile.isEmpty() || users_mobile.equalsIgnoreCase("null") || users_mobile.equalsIgnoreCase("") ))
                    {
                        contact_no=users_mobile;
                    }
                   // Toast.makeText(PaymentDetails.this, "not verified", Toast.LENGTH_SHORT).show();
                    sd_not_verify = new SweetAlertDialog(PaymentDetails.this, SweetAlertDialog.NORMAL_TYPE);
                    sd_not_verify.setTitleText("Mobile Verification");
                    sd_not_verify.setContentText("Please verify your mobile number -- "+contact_no);
                    sd_not_verify.setConfirmText("OK");
                    sd_not_verify.showCancelButton(true);
                    sd_not_verify.setCanceledOnTouchOutside(false);
                    sd_not_verify.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(final SweetAlertDialog sweetAlertDialog)
                        {
                            getOTPMobileVerified();
                            Intent it=new Intent(PaymentDetails.this,AccountVerificationActivity.class);
                            it.putExtra("activity","PAYMENTDETAIL");
                            it.putExtra("mobile",contact_no);
                            startActivity(it);
                            finish();
                        }
                    });
                    sd_not_verify.show();
                }
                else {
                    next_btn.setClickable(false);
                    next_btn.setFocusable(false);
                    next_btn.setFocusableInTouchMode(false);

                   doCheckoutPayment();
                    Log.d("ASF","DASFDS");
                }
            } else {
                Toast.makeText(PaymentDetails.this, "Check Internet Connection..!", Toast.LENGTH_SHORT).show();
            }
        }

      /*  if(v==remove_coupon_code)
        {
            promo_code_txt.setVisibility(View.VISIBLE);
            remove_coupon_code.setVisibility(View.GONE);
            row_coupons.setVisibility(View.GONE);
            Log.d("ASSSSS",""+appliedCoupon_amount+"///////"+total_price_after_coupon);
            total_after_remove_coupan=total_price_after_coupon+Double.parseDouble(appliedCoupon_amount);
            totlaPricetxt.setText(""  +total_after_remove_coupan+ "/-");


        }*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK)
            {


                coupon_code = data.getStringExtra("result");
                Log.d("dsafds",coupon_code);
                if(coupon_code.equalsIgnoreCase("2"))
                {
                    promo_code_txt.setVisibility(View.VISIBLE);
                    add_coupan_code.setVisibility(View.GONE);
                }
                else
                {
                    add_coupan_code.setVisibility(View.VISIBLE);
                    appliedCoupon_amount = data.getStringExtra("applied_amt");
                    Log.d("CODESSS", coupon_code + "///" + appliedCoupon_amount);
                    add_coupan_code.setText("Applied Code " + coupon_code);
                    coupan_code_txt.setVisibility(View.VISIBLE);
                    // remove_coupon_code.setVisibility(View.VISIBLE);
                    coupan_code_txt.setText(appliedCoupon_amount + "/-");
                    Double appliedCoupAmt = final_amount - Double.parseDouble(appliedCoupon_amount);
                    total_price_after_coupon = appliedCoupAmt + Double.parseDouble(delivery_charges);   //after applied coupon add delivery chages
                    totlaPricetxt.setText("" + new DecimalFormat("##.##").format(total_price_after_coupon) + "/-");                   //
                }


            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }


    public void getCartData() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();
            String URL = AppUrls.BASE_URL + AppUrls.GETCARTDATA + user_id + "&browser_id=" + deviceId;
            Log.d("CARTURL", URL);

            StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                if (status.equals("10100")) {

                                    Log.d("CARTRES", response);
                                    product_details_recyclerview.setVisibility(View.VISIBLE);
                                    String message = jsonObject.getString("message");

                                    Type stringMap = new TypeToken<Map<String, Object>>() {
                                    }.getType();
                                    Map<String, Object> data = new Gson().fromJson(jsonObject.optString("data"), stringMap);

                                    String currency = (String) data.get("currency");
                                    Double finalTotal = (Double) data.get("finalTotal");
                                    Double finalDiscount = (Double) data.get("finalDiscount");
                                    recordList = (ArrayList<Object>) data.get("records");
                                    Map<String, Object> recordPos = (Map<String, Object>) recordList.get(0);
                                    String id = (String) recordPos.get("id");
                                    Double total_price = (Double) recordPos.get("total_price");
                                    Double grand_total = (Double) recordPos.get("grand_total");
                                    String product_id = (String) recordPos.get("product_id");
                                    String images = (String) recordPos.get("images");
                                    String mrp_price = (String) recordPos.get("mrp_price");
                                    String type = (String) recordPos.get("type");
                                    String purchase_quantity = (String) recordPos.get("purchase_quantity");
                                    String unit_name = (String) recordPos.get("unit_name");
                                    Double discount = (Double) recordPos.get("discount");
                                    myProductDetailsAdapter = new MyProductDetailsAdapter(PaymentDetails.this, recordList, R.layout.row_product_payment_detail);
                                    product_details_recyclerview.setAdapter(myProductDetailsAdapter);
                                    RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(PaymentDetails.this, R.drawable.recycler_view_divider));
                                    product_details_recyclerview.addItemDecoration(dividerItemDecoration);
                                    recordChildList = (ArrayList<Object>) recordPos.get("ESSENTIAL");
                                    myProductDetailsAdapter.notifyDataSetChanged();
                                    // cartRecordChild = new CartRecordChild(MyCartActivity.this, recordChildList);

                                }
                                if (status.equals("10200")) {

                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10300")) {

                                    pprogressDialog.dismiss();
                                    product_details_recyclerview.setVisibility(View.GONE);
                                    Toast.makeText(getApplicationContext(), "No Data Found..", Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10400")) {

                                    pprogressDialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Unable to Send Otp..", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.getMessage();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
                    headers.put("X-Deviceid", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    return headers;
                }

            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(PaymentDetails.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(PaymentDetails.this, "Check Internet Connection..!", Toast.LENGTH_SHORT).show();
        }
    }

    public void refreshCartPage() {

        pprogressDialog.dismiss();
        getCartData();
    }

    public void getCheckOutData() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();
            String checkoutUrl = AppUrls.BASE_URL + AppUrls.GET_CHECKOUT_DATA + "?user_id=" + user_id;
            Log.d("detailorderURL", checkoutUrl);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, checkoutUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            pprogressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                Log.d("detailorderPONSE", response);

                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");
                                if (status.equalsIgnoreCase("10100"))
                                {

                                    pprogressDialog.dismiss();
                                    Type stringMap = new TypeToken<Map<String, Object>>() {
                                    }.getType();
                                    Map<String, Object> data = new Gson().fromJson(jsonObject.optString("data"), stringMap);
                                    if (data.get("address") instanceof Boolean)
                                    {
                                        addAddress_txt.setVisibility(View.VISIBLE);
                                    }
                                    else
                                    {
                                        delivery_ll.setVisibility(View.VISIBLE);
                                        defaultAddressList = (ArrayList<Object>) data.get("address");
                                        Map<String, Object> recordPos = (Map<String, Object>) defaultAddressList.get(0);
                                        adress_id = (String) recordPos.get("id");
                                        String name = (String) recordPos.get("name");
                                        String address_line1 = (String) recordPos.get("address_line1");
                                        String address_line2 = (String) recordPos.get("address_line2");
                                        String area = (String) recordPos.get("area");
                                        String city = (String) recordPos.get("city");
                                        String state = (String) recordPos.get("state");
                                        String pincode = (String) recordPos.get("pincode");
                                        String contact_no = (String) recordPos.get("contact_no");
                                        String alternate_contact_no = (String) recordPos.get("alternate_contact_no");
                                        boolean delivery_status = (Boolean) recordPos.get("delivery_status");


                                       // Log.d("FFFFFFFFFFFF", "//" + delivery_charges + "///" + delivery_status);
                                        if (activity.equalsIgnoreCase("ChooseAddressAdapter"))
                                        {
                                             delAddress();
                                         }
                                        else
                                            {
                                            if(recordPos.get("delivery_charges") instanceof String)
                                                delivery_charges = (
                                                        String) recordPos.get("delivery_charges");
                                            else {
                                                Double charge=(Double) recordPos.get("delivery_charges");
                                                delivery_charges = String.valueOf(charge);
                                            }
                                            dileverytxt.setText(delivery_charges);
                                            delivery_detail_txt.setText(name + "\n" + address_line1 + "," + address_line2 + "," + area + "," + city + "," + state + "," + pincode + "\n" + contact_no + "\n" + alternate_contact_no);

                                        }

                                    }

                                    orderCloseStatus= (Double) data.get("enable_order_closing");
                                    orderCloseStatText= (String) data.get("order_closing_txt");
                                    if(orderCloseStatus==1)
                                    {
                                        sd_orderClose = new SweetAlertDialog(PaymentDetails.this, SweetAlertDialog.ERROR_TYPE);
                                        sd_orderClose.setTitleText("Oop's Sorry....!");
                                        sd_orderClose.setContentText(orderCloseStatText);
                                        sd_orderClose.setConfirmText("OK");
                                        sd_orderClose.showCancelButton(true);
                                        sd_orderClose.setCanceledOnTouchOutside(false);
                                        sd_orderClose.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(final SweetAlertDialog sweetAlertDialog)
                                             {
                                                Intent order_close= new Intent(PaymentDetails.this,MainActivity.class);
                                                startActivity(order_close);
                                                finish();
                                            }
                                        });
                                        sd_orderClose.show();

                                    }
                                    users_mobile= (String) data.get("mobile");
                                    mobile_verifyed= (String) data.get("mobile_verify_status");
                                    expected_delevry= (String) data.get("expected_delivery");
                                    expected_delevry_date.setText(expected_delevry);
                                    final_amount = (Double) data.get("finalTotal");   //200.00
                                    SharedPreferences preferences=getSharedPreferences("amount",0);
                                    SharedPreferences.Editor editor=preferences.edit();
                                    editor.putInt("finalamount",final_amount.intValue());
                                    editor.apply();
                                    final_discount = (Double) data.get("finalDiscount");
                                    pricetxt.setText(""+Math.round(final_amount) + "/-");
                                    discounttxt.setText(String.valueOf(final_discount));
                                    if (delivery_charges != null)
                                        Totalprice = final_amount + Double.valueOf(delivery_charges);
                                    else
                                        Totalprice = final_amount + Double.valueOf(0);
                                    totlaPricetxt.setText("" + new DecimalFormat("##.##").format(Totalprice) + "/-");

                                    orderDetailProduct = (ArrayList<Object>) data.get("payment_gateway");
                                    paymentTypeAdapter = new PaymentTypeAdapter(PaymentDetails.this, orderDetailProduct, R.layout.row_pay_type);
                                    payment_type_recyclerview.setHasFixedSize(true);

                                    payment_type_recyclerview.setLayoutManager(new LinearLayoutManager(PaymentDetails.this, LinearLayoutManager.VERTICAL, false));
                                    payment_type_recyclerview.setItemAnimator(new DefaultItemAnimator());
                                    payment_type_recyclerview.setAdapter(paymentTypeAdapter);

                                }

                                if (status.equalsIgnoreCase("10200")) {

                                    pprogressDialog.dismiss();
                                    Toast.makeText(PaymentDetails.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                                if (status.equalsIgnoreCase("10400")) {
                                    pprogressDialog.dismiss();
                                   /*
                                 Intent it=new Intent(PaymentDetails.this,ProfileActivity.class);
                                 startActivity(it);
                                 finish();*/
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.getMessage();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
                    headers.put("X-Deviceid", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    return headers;
                }

            };
            RequestQueue requestQueue = Volley.newRequestQueue(PaymentDetails.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(PaymentDetails.this, "Check Internet Connection..!", Toast.LENGTH_SHORT).show();
        }
    }

    private void doCheckoutPayment()
    {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet)
        {
            pprogressDialog.show();
            String url_checkout = AppUrls.BASE_URL + AppUrls.GET_CHECKOUT;
            Log.d("checkoutURL", url_checkout);
            StringRequest req_members = new StringRequest(Request.Method.POST, url_checkout, new Response.Listener<String>()
            {
                @Override
                public void onResponse(String response)
                {
                    pprogressDialog.dismiss();

                    Log.d("checkoutRESP", response);
                    try {
                        JSONObject jobcode = new JSONObject(response);
                        String response_code = jobcode.getString("status");
                        String message = jobcode.getString("message");
                        if (response_code.equals("10100"))
                        {
                           // Toast.makeText(PaymentDetails.this, ""+message, Toast.LENGTH_LONG).show();
                            order_id = jobcode.getString("order_id");
                            Log.d("ORDERIDDDD",order_id);
                             sd = new SweetAlertDialog(PaymentDetails.this, SweetAlertDialog.SUCCESS_TYPE);
                            sd.setTitleText("Successfully Done!!");
                            sd.setContentText("Order has been confirmed.!");
                            sd.setConfirmText("OK");
                            sd.showCancelButton(true);
                            sd.setCanceledOnTouchOutside(false);
                            sd.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(final SweetAlertDialog sweetAlertDialog)
                                {
                                    Intent order_detail= new Intent(PaymentDetails.this,OrderHistoryDetailActivity.class);
                                    order_detail.putExtra("ORDERID", order_id);
                                    startActivity(order_detail);
                                    finish();
                                }
                            });
                            sd.show();

                        }else
                        if (response_code.equals("10400"))
                        {

                            pprogressDialog.dismiss();
                            JSONObject jsobjData = jobcode.getJSONObject("data");
                            String pay_url = jsobjData.getString("payment_url");
                            String pay_amount = jsobjData.getString("amount");
                            String order_id = jsobjData.getString("order_id");
                            Log.d("MESSAGE", message + "////" + pay_url+"////"+order_id);
                            Intent order_detail= new Intent(PaymentDetails.this,OrdersHistoryActivity.class);
                            order_detail.putExtra("payurl",pay_url);
                            order_detail.putExtra("ORDERID",order_id);
                            startActivity(order_detail);
                            finish();
                            ///////ONLINE RESPONSE
                        }else
                        if (response_code.equals("10300")) {

                            pprogressDialog.dismiss();
                          Toast.makeText(PaymentDetails.this, "Unable to process your request..! ", Toast.LENGTH_LONG).show();
                        }else
                        if (response_code.equals("10500"))
                        {
                             Toast.makeText(PaymentDetails.this, ""+message, Toast.LENGTH_LONG).show();

                        }else
                        if (response_code.equals("10600")) {

                            pprogressDialog.dismiss();
                            Toast.makeText(PaymentDetails.this, "Coupon is invalid..!", Toast.LENGTH_LONG).show();
                        }else
                        if (response_code.equals("10700")) {

                            pprogressDialog.dismiss();
                            Toast.makeText(PaymentDetails.this, message, Toast.LENGTH_LONG).show();
                        }
                        else
                        if (response_code.equals("10110")) {

                            pprogressDialog.dismiss();
                            Toast.makeText(PaymentDetails.this, message, Toast.LENGTH_LONG).show();
                        }
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                }
            },
                    new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    error.getMessage();
                }
            })
            {
                @Override
                protected Map<String, String> getParams()
                {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);

                    if (activity.equalsIgnoreCase("ChooseAddressAdapter"))
                    {
                        params.put("address_id", id);
                    }
                    else
                    {
                        params.put("address_id", adress_id);
                    }


                    if (coupon_code == null) {
                        params.put("code", "");
                    } else {
                        params.put("code", coupon_code);
                    }

                    params.put("payment_gateway_id", paymentTypeAdapter.pay_id);
                    params.put("platform", "ANDROID");
                    Log.d("checkoutPARAM", params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
                    headers.put("X-Deviceid", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    return headers;
                }
            };

            req_members.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(PaymentDetails.this);
            requestQueue.add(req_members);
        }
        else
            {
            Toast.makeText(PaymentDetails.this, "Check Internet Connection..!", Toast.LENGTH_SHORT).show();
        }
    }



    private void getOTPMobileVerified() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.RESENDOTP,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                if (status.equals("10100")) {

                                    String message = jsonObject.getString("message");
                                    Toast.makeText(PaymentDetails.this, message, Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10200")) {

                                    Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10300")) {

                                    Toast.makeText(getApplicationContext(), "User Not Exist..", Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10400")) {

                                    Toast.makeText(getApplicationContext(), "Unable to Send Otp..", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            error.getMessage();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("mobile", contact_no);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    return headers;
                }

            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(PaymentDetails.this);
            requestQueue.add(stringRequest);
        } else {

            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean checkLocationPermission() {
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);


        List<String> listPermissionsNeeded = new ArrayList<>();

        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }


        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
                    REQUEST_ID_MULTIPLE_PERMISSIONS);

            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_SMS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CALL_PHONE, PackageManager.PERMISSION_GRANTED);

                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);

                    if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                    {
                        Intent intent = new Intent(PaymentDetails.this, TestActivity.class);
                        startActivity(intent);

                    }
                    else {

                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                            showDialogOK("Location Permissions Required For This App", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which) {
                                        case DialogInterface.BUTTON_POSITIVE:
                                            checkLocationPermission();
                                            break;

                                        case DialogInterface.BUTTON_NEGATIVE:
                                            break;
                                    }
                                }
                            });
                        } else {
                            Toast.makeText(this, "Goto Settings And Enable Permissions", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        }
    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this).setMessage(message).setPositiveButton("OK", okListener).create().show();
    }
}
