package in.innasoft.gofarmz.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class UpdateProfileActivity extends AppCompatActivity implements View.OnClickListener,RadioGroup.OnCheckedChangeListener
{

    EditText mobile_edt, email_edt,username_edt;
    RadioGroup rg;
    RadioButton male_rb, female_rb;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    ImageView close_img;
    Typeface light,regular,bold;
    TextView  gender_title;
    String  deviceId, token, user_id,email,mobile,gender,name,send_gender;
    Button update_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);

        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));
        checkInternet = NetworkChecking.isConnected(this);

           Bundle bundle=getIntent().getExtras();
            name= bundle.getString("name");
            email= bundle.getString("email");
            mobile= bundle.getString("mobile");
            gender= bundle.getString("gender");
            Log.d("DETAIL",email+"-----"+mobile+"------"+gender+"------"+name);

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        update_btn = findViewById(R.id.update_btn);
        update_btn.setOnClickListener(this);
        update_btn.setTypeface(bold);

        email_edt = findViewById(R.id.email_edt);
        email_edt.setTypeface(light);
        mobile_edt = findViewById(R.id.mobile_edt);
        mobile_edt.setTypeface(light);
        username_edt = findViewById(R.id.username_edt);
        username_edt.setTypeface(light);

        rg = findViewById(R.id.rg);
        rg.setOnCheckedChangeListener(this);
        male_rb = findViewById(R.id.male_rb);
        male_rb.setTypeface(light);
        female_rb = findViewById(R.id.female_rb);
        female_rb.setTypeface(light);

        gender_title = findViewById(R.id.gender_title);
        gender_title.setTypeface(regular);

        mobile_edt.setText(mobile);
        email_edt.setText(email);
        username_edt.setText(name);
        if(gender!=null){
            send_gender=gender;
            if (gender.equalsIgnoreCase("Male")) {
                male_rb.setChecked(true);
            } else if (gender.equalsIgnoreCase("Female")) {
                female_rb.setChecked(true);
            }
        }


    }

    @Override
    public void onClick(View v)
    {

        if (v == close_img) {

            finish();
        }
        if (v == update_btn) {
            String moblie=mobile_edt.getText().toString();
            if(moblie==null || moblie.length()==0)
            {
                Toast.makeText(this,"Enter Mobile No",Toast.LENGTH_SHORT).show();
            }
            else if(send_gender==null || send_gender.length()==0)
            {
                Toast.makeText(this,"Select Gender",Toast.LENGTH_SHORT).show();
            }else{
                updateProfile();
            }

        }
    }

    private void updateProfile()
    {
        if(checkInternet)
        {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.UPDATEPROFILE,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                String message = jsonObject.getString("message");
                                if (status.equals("10100"))
                                {
                                   // String message = jsonObject.getString("message");
                                    Toast.makeText(UpdateProfileActivity.this, message, Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(UpdateProfileActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                                if (status.equals("10200")) {
                                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10300")) {
                                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10400")) {
                                   Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10500")) {
                                   Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10600")) {
                                   Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                           }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.getMessage();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("name",username_edt.getText().toString());
                    params.put("email", email_edt.getText().toString());
                    params.put("mobile", mobile_edt.getText().toString());
                    params.put("gender",send_gender );
                    Log.d("PARAMM",params.toString());


                    return params;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("X-Platform", "ANDROID");
                    headers.put("X-Deviceid", deviceId);
                    headers.put("Authorization-Basic", token);
                    return headers;
                }

            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(UpdateProfileActivity.this);
            requestQueue.add(stringRequest);
        }
        else
        {
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId)
    {
        if(checkedId==R.id.male_rb){
            male_rb.setChecked(true);
            send_gender="Male";
        }else {
            female_rb.setChecked(true);
            send_gender="Female";
        }
    }
}
