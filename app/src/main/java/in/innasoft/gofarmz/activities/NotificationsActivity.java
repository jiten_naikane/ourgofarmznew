package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapters.NitificationAdapter;
import in.innasoft.gofarmz.models.NotificationModel;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.DividerItemDecorator;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class NotificationsActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close_img,no_notifictioon_img;
    TextView notify_toolbar_title;
    Typeface bold, light;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token;
    RotateLoading progress_indicator;
    RecyclerView notifction_recycleview;
    NitificationAdapter notificationAdapter;
    ArrayList<NotificationModel> notifiyList;
    ProgressDialog pprogressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        checkInternet = NetworkChecking.isConnected(this);
        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);

       // progress_indicator = findViewById(R.id.progress_indicator);
        //progress_indicator.start();
        pprogressDialog = new ProgressDialog(NotificationsActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        notifction_recycleview = findViewById(R.id.notifction_recycleview);

        no_notifictioon_img = findViewById(R.id.no_notifictioon_img);
        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        notify_toolbar_title = findViewById(R.id.notify_toolbar_title);
        notify_toolbar_title.setTypeface(bold);


        if (user_id!=null)
        {
            getNotification();
        }
        else
        {
            Intent intent = new Intent(NotificationsActivity.this, LoginActivity.class);
            startActivity(intent);
        }
    }

    public void getNotification() {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();
                String pushNotfyurl = AppUrls.BASE_URL + AppUrls.GET_PUSHNOTIFICATION + user_id + "/notification";
                Log.d("NTFYURL", pushNotfyurl);

            StringRequest strAllMember = new StringRequest(Request.Method.GET, pushNotfyurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("NTFYRESP", response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("status");
                        if (successResponceCode.equals("10100")) {
                            no_notifictioon_img.setVisibility(View.GONE);
                           pprogressDialog.dismiss();

                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            notifiyList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jdataobj = jsonArray.getJSONObject(i);
                                NotificationModel mygroup = new NotificationModel(jdataobj);
                                notifiyList.add(mygroup);
                            }
                            notificationAdapter = new NitificationAdapter(notifiyList, NotificationsActivity.this, R.layout.row_notification_list);
                            notifction_recycleview.setNestedScrollingEnabled(false);
                            notifction_recycleview.setLayoutManager(new LinearLayoutManager(NotificationsActivity.this, LinearLayoutManager.VERTICAL, false));
                            notifction_recycleview.setItemAnimator(new DefaultItemAnimator());
                            RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(NotificationsActivity.this, R.drawable.recycler_view_divider));
                            notifction_recycleview.addItemDecoration(dividerItemDecoration);
                            notifction_recycleview.setAdapter(notificationAdapter);

                        }else {
                            pprogressDialog.dismiss();
                            no_notifictioon_img.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();
                  error.getMessage();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("HEADER", "HEADDER " + headers.toString());
                    return headers;
                }
            };
            strAllMember.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(strAllMember);
        } else {
            progress_indicator.stop();
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == close_img) {
            finish();
        }
    }

}
