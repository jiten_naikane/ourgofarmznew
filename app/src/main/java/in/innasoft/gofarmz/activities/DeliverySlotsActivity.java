package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapters.AddCountriesAdapter;
import in.innasoft.gofarmz.adapters.DelivrySlotAdapter;
import in.innasoft.gofarmz.adapters.PaymentTypeAdapter;
import in.innasoft.gofarmz.models.DeliverySlotModel;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class DeliverySlotsActivity extends AppCompatActivity implements  View.OnClickListener
{
     ImageView close_img;
     TextView delivery_op_toolbar_title,addAddress_txt,delivery_title_txt,delivery_detail_txt,manage_address_txt,delivry_slot_title_txt,delivry_charge_txt,delivry_chargetitle_txt;
     Spinner spinDelivry_slot;
     RelativeLayout delivery_ll;
     Button proceed_toPay_btn;
    Typeface light, regular, bold;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token,delivery_charges,adress_id,activity,id;
    private boolean checkInternet;
    ProgressDialog pDialog;
    ArrayList<Object> defaultAddressList = new ArrayList<Object>();
    ArrayList<Object> delivrySlot = new ArrayList<Object>();
    AlertDialog dialog;


    //DelivrySlotAdapter delivrySlotAdapter;
   // ArrayList<DeliverySlotModel> delivrySlotModels = new ArrayList<>();
 //   ArrayList<String> countriesList = new ArrayList<String>();
    String from = "";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_slots);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));
        checkInternet = NetworkChecking.isConnected(this);
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);

        from = getIntent().getStringExtra("from");
        Log.d("SESSIONDATA", deviceId + "/n" + user_id + "/n" + token);
        checkInternet = NetworkChecking.isConnected(this);
        pDialog = new ProgressDialog(DeliverySlotsActivity.this);
        pDialog.setMessage("Please wait......");
        pDialog.setProgressStyle(R.style.DialogTheme);


        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        delivery_op_toolbar_title = findViewById(R.id.delivery_op_toolbar_title);
        delivery_op_toolbar_title.setTypeface(bold);

        proceed_toPay_btn = findViewById(R.id.proceed_toPay_btn);
        proceed_toPay_btn.setOnClickListener(this);
        proceed_toPay_btn.setTypeface(bold);

        spinDelivry_slot = findViewById(R.id.spinDelivry_slot);
        //textDelivry_slot.setOnClickListener(this);


        delivery_ll = findViewById(R.id.delivery_ll);

        addAddress_txt = findViewById(R.id.addAddress_txt);
        addAddress_txt.setTypeface(light);
        delivery_title_txt = findViewById(R.id.delivery_title_txt);
        delivery_title_txt.setTypeface(bold);
        delivery_detail_txt = findViewById(R.id.delivery_detail_txt);
        delivery_detail_txt.setTypeface(light);
        manage_address_txt = findViewById(R.id.manage_address_txt);
        manage_address_txt.setTypeface(light);
        delivry_slot_title_txt = findViewById(R.id.delivry_slot_title_txt);
        delivry_slot_title_txt.setTypeface(bold);

        delivry_chargetitle_txt = findViewById(R.id.delivry_chargetitle_txt);
        delivry_chargetitle_txt.setTypeface(bold);

        delivry_charge_txt = findViewById(R.id.delivry_charge_txt);
        delivry_charge_txt.setTypeface(light);

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        activity = bundle.getString("activity");
        assert activity != null;
        if (activity.equalsIgnoreCase("ChooseAddressAdapter")) {
            id = bundle.getString("id");
           // contact_no = bundle.getString("contact_no");
            //    addID = bundle.getString("id");
            //delAddress();
        }

        getDeliveryOptData();
      }

    private void getDeliveryOptData()
    {
        if(checkInternet)
        {
            pDialog.show();
            String delivSlotyurl = AppUrls.BASE_URL + AppUrls.DELIVERY_SLOT_DATA+"?user_id="+user_id;
            Log.d("SLOTURL", delivSlotyurl);
            StringRequest strDelivSlot=new StringRequest(Request.Method.GET, delivSlotyurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response)
                {
                    pDialog.dismiss();
                     try
                     {
                         JSONObject jsonObject = new JSONObject(response);
                         Log.d("RESPSLOT", response);
                         String status = jsonObject.getString("status");
                         String message = jsonObject.getString("message");
                         if (status.equalsIgnoreCase("10100"))
                         {
                             Type stringMap = new TypeToken<Map<String, Object>>() {}.getType();
                             Map<String, Object> data = new Gson().fromJson(jsonObject.optString("data"), stringMap);
                             if (data.get("address") instanceof Boolean)
                             {
                                 addAddress_txt.setVisibility(View.VISIBLE);
                             }
                             else
                             {
                                 delivery_ll.setVisibility(View.VISIBLE);
                                 defaultAddressList = (ArrayList<Object>) data.get("address");
                                 Map<String, Object> recordPos = (Map<String, Object>) defaultAddressList.get(0);
                                 adress_id = (String) recordPos.get("id");
                                 String name = (String) recordPos.get("name");
                                 String address_line1 = (String) recordPos.get("address_line1");
                                 String address_line2 = (String) recordPos.get("address_line2");
                                 String area = (String) recordPos.get("area");
                                 String city = (String) recordPos.get("city");
                                 String state = (String) recordPos.get("state");
                                 String pincode = (String) recordPos.get("pincode");
                                 String contact_no = (String) recordPos.get("contact_no");
                                 String alternate_contact_no = (String) recordPos.get("alternate_contact_no");
                                 boolean delivery_status = (Boolean) recordPos.get("delivery_status");

                                 if (activity.equalsIgnoreCase("ChooseAddressAdapter"))
                                 {
                                    // delAddress();
                                 }
                                 else
                                 {
                                     if(recordPos.get("delivery_charges") instanceof String)
                                         delivery_charges = (String) recordPos.get("delivery_charges");
                                     else {
                                         Double charge=(Double) recordPos.get("delivery_charges");
                                         delivery_charges = String.valueOf(charge);
                                     }
                                     delivry_charge_txt.setText(delivery_charges+"/-");
                                     delivery_detail_txt.setText(name + "\n" + address_line1 + "," + address_line2 + "," + area + "," + city + "," + state + "," + pincode + "\n" + contact_no + "\n" + alternate_contact_no);
                                 }
                             }

                             ArrayList<Object> tmpList = (ArrayList<Object>) data.get("available_delivery_slots");
                             delivrySlot=tmpList;

                            /* for (int i = 0; i < tmpList.size(); i++)
                             {
                                 ArrayList<Object> custompricesarr = new ArrayList<>(tmpList.size());
                                 DeliverySlotModel model = new DeliverySlotModel();
                                 Map<String, Object> tmpObj = (Map<String, Object>) tmpList.get(i);
                                 model.setDay((String) tmpObj.get("day"));
                                 model.setDate((String) tmpObj.get("date"));

                                 ArrayList<Object> optionstmparr = (ArrayList<Object>) tmpObj.get("slots");
                                 for (int k = 0; k < optionstmparr.size(); k++) {
                                     Map<String, Object> objectMap = new HashMap<>(tmpList.size());
                                     Map<String, Object> priceObj1 = (Map<String, Object>) optionstmparr.get(k);
                                     objectMap.put("id", priceObj1.get("id"));
                                     objectMap.put("start_time", priceObj1.get("start_time"));
                                     objectMap.put("end_time", priceObj1.get("end_time"));

                                     custompricesarr.add(objectMap);
                                     if (optionstmparr.size() == custompricesarr.size())
                                         model.setOptions(custompricesarr);
                                     //Log.v("LLLLLLLLLL","///"+custompricesarr);
                                 }
                                 delivrySlotModels.add(model);


                             }*/
                             DelivrySlotAdapter  delivrySlotAdapter=new DelivrySlotAdapter( DeliverySlotsActivity.this,delivrySlot);
                             spinDelivry_slot.setAdapter(delivrySlotAdapter);

                             spinDelivry_slot.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                 @Override
                                 public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
                                 {

                                 }

                                 @Override
                                 public void onNothingSelected(AdapterView<?> parent)
                                 {

                                 }
                             });

                          }
                         if (status.equalsIgnoreCase("10200"))
                         {

                             pDialog.dismiss();
                             Toast.makeText(DeliverySlotsActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                         }
                         if (status.equalsIgnoreCase("10400"))
                         {
                             pDialog.dismiss();

                         }
                     }
                     catch(JSONException je)
                     {
                         je.getMessage();
                     }
                }}, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pDialog.dismiss();
                    error.getMessage();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    return headers;
                }
            };

            strDelivSlot.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(strDelivSlot);
        }
        else
        {
            Toast.makeText(DeliverySlotsActivity.this, "No Internet COnnection...!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onClick(View v)
    {
       if(v==close_img)
       {
           Intent intent = new Intent(DeliverySlotsActivity.this, MyCartActivity.class);
           intent.putExtra("from","Home");
           startActivity(intent);
       }
       if(v==proceed_toPay_btn)
       {
           Intent intent = new Intent(DeliverySlotsActivity.this, PaymentDetails.class);
           intent.putExtra("activity", "DeliverySlotActivity");  //add_id,slot_id,contactno,
           startActivity(intent);
       }

    }

}
