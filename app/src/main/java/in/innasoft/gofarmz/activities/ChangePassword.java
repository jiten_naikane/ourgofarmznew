package in.innasoft.gofarmz.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class ChangePassword extends AppCompatActivity implements View.OnClickListener {

    EditText old_password_edt,new_password_edt,conf_password_edt;
    Button submit_btn;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String deviceId,user_id,token,old_pass,new_pass,confrm_pass;
    Typeface light, regular, bold;
    RotateLoading progress_indicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA",deviceId+"/n"+user_id+"/n"+token);

        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));

        progress_indicator = findViewById(R.id.progress_indicator);

        old_password_edt = findViewById(R.id.old_password_edt);
        old_password_edt.setTypeface(light);
        new_password_edt = findViewById(R.id.new_password_edt);
        new_password_edt.setTypeface(light);
        conf_password_edt = findViewById(R.id.conf_password_edt);
        conf_password_edt.setTypeface(light);
        submit_btn = findViewById(R.id.submit_btn);
        submit_btn.setTypeface(bold);
        submit_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {

        if (v == submit_btn)
        {
            if (validate()) {
                checkInternet = NetworkChecking.isConnected(this);
                if (checkInternet) {

                    Log.d("CHANGEPURL", AppUrls.BASE_URL + AppUrls.CHANGEPASSWORD);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.CHANGEPASSWORD,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.d("RESPCHANGEP", response);
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String status = jsonObject.getString("status");
                                        String message = jsonObject.getString("message");
                                        if (status.equals("10100")) {
                                            progress_indicator.stop();

                                            Toast.makeText(ChangePassword.this, message, Toast.LENGTH_SHORT).show();

                                            Intent intent = new Intent(ChangePassword.this, MainActivity.class);
                                            startActivity(intent);

                                        }
                                        if (status.equals("10200")) {
                                            progress_indicator.stop();
                                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                        }
                                        if (status.equals("10300")) {
                                            progress_indicator.stop();
                                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                        }
                                        if (status.equals("10400")) {
                                            progress_indicator.stop();
                                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                        }
                                        if (status.equals("10500")) {
                                            progress_indicator.stop();
                                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                        }
                                        if (status.equals("10600")) {
                                            progress_indicator.stop();
                                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();

                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    error.getMessage();
                                }
                            }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("user_id", user_id);
                            params.put("old_pwd", old_password_edt.getText().toString());
                            params.put("new_pwd", new_password_edt.getText().toString());
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> headers = new HashMap<>();
                            headers.put("X-Platform", "ANDROID");
                            headers.put("X-Deviceid", deviceId);
                            headers.put("Authorization-Basic", token);
                            return headers;
                        }

                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    RequestQueue requestQueue = Volley.newRequestQueue(ChangePassword.this);
                    requestQueue.add(stringRequest);
                } else {

                   /* Snackbar snackbar = Snackbar.make(v, "No Internet Connection...!", Snackbar.LENGTH_LONG);
                     snackbar.show();*/
                    Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        }

    }








    private boolean validate()
    {

        boolean result = true;
        int flag = 0;

        String strCurrentPassword = old_password_edt.getText().toString();

        if (strCurrentPassword == null || strCurrentPassword.equals("") ||strCurrentPassword.length()==0)
        {
            old_password_edt.setError("Please enter old password");
            result = false;
        }


        String strNewPassword = new_password_edt.getText().toString();

        if (strNewPassword == null || strNewPassword.equals("") || strNewPassword.matches("^-\\s")||strNewPassword.length()==0)
        {
            new_password_edt.setError("Please enter new password");
            result = false;

        }

        String strConformPassword = conf_password_edt.getText().toString();

        if (strConformPassword == null || strConformPassword.equals("")|| strConformPassword.matches("^-\\s") ||strConformPassword.length()==0)
        {
            conf_password_edt.setError("Please enter confirm password");
            result = false;

        }

     /*  if(strNewPassword != "" && strConformPassword != "" &&!strConformPassword.equals(strNewPassword) && flag == 0 )
        {
            new_password_edt.setError("New Password and Confirm password mismatch");
            conf_password_edt.setError("New Password and Confirm password mismatch");
            result = false;
        }
       else {

           new_password_edt.setError("New Password and Confirm password mismatch");
           conf_password_edt.setError("New Password and Confirm password mismatch");
       }*/

        return result;

    }
}
