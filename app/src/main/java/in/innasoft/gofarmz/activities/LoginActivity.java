package in.innasoft.gofarmz.activities;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.Task;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapters.NitificationAdapter;
import in.innasoft.gofarmz.models.NotificationModel;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.DividerItemDecorator;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener,GoogleApiClient.OnConnectionFailedListener {

    TextView login_txt, reg_txt, forgotPassword_txt, or_txt, next_txt, reg_or_txt, reg_next_txt,t_c_textview;
    EditText username_edt, password_edt, name_edt, email_edt, mobile_edt, regpassword_edt;
    Button text_fbLogin,text_gplusLogin,reg_text_fbLogin,reg_text_gplusLogin;
    Typeface light, regular, bold;
    RelativeLayout log_rl, reg_rl;
    UserSessionManager userSessionManager;
    String deviceId, name, email, password, mobile;
    private boolean checkInternet;
    CallbackManager callbackManager;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 007;
    String screen,user="",gmail="";
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    String MOBILE_REGEX = "^[6789]\\d{9}$";
    String nm = "", lnm = "";
    String sendFBAccessToken, sendFBUserID, token = "", verified_data, fb_user_id_data, email_data, name_data, gender_data, user_type,checnkVeriosn,android_version;
    RotateLoading progress_indicator;
    CheckBox t_c_chk;
    SweetAlertDialog  sd_update;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initializeFacebookSettings();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        PackageInfo eInfo = null;
        try {
            eInfo = getPackageManager().getPackageInfo("in.innasoft.gofarmz", 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        checnkVeriosn=eInfo.versionName;
        Log.d("APPVERSION",checnkVeriosn);
        getVerionUpdateData(checnkVeriosn);

        deviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        userSessionManager = new UserSessionManager(this);
        userSessionManager.createDeviceId(deviceId);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);

        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));

        progress_indicator = findViewById(R.id.progress_indicator);
        progress_indicator.start();

        login_txt = findViewById(R.id.login_txt);
        login_txt.setTypeface(light);
        login_txt.setOnClickListener(this);
        reg_txt = findViewById(R.id.reg_txt);
        reg_txt.setTypeface(light);
        reg_txt.setOnClickListener(this);
        username_edt = findViewById(R.id.username_edt);
        username_edt.setTypeface(light);
        password_edt = findViewById(R.id.password_edt);
        password_edt.setTypeface(light);
        forgotPassword_txt = findViewById(R.id.forgotPassword_txt);
        forgotPassword_txt.setTypeface(light);
        forgotPassword_txt.setOnClickListener(this);
        or_txt = findViewById(R.id.or_txt);
        or_txt.setTypeface(light);
        text_fbLogin = findViewById(R.id.text_fbLogin);
        text_fbLogin.setOnClickListener(this);
        text_gplusLogin = findViewById(R.id.text_gplusLogin);
        text_gplusLogin.setOnClickListener(this);
        next_txt = findViewById(R.id.next_txt);
        next_txt.setTypeface(bold);
        next_txt.setOnClickListener(this);

        name_edt = findViewById(R.id.name_edt);
        name_edt.setTypeface(light);
        email_edt = findViewById(R.id.email_edt);
        email_edt.setTypeface(light);
        mobile_edt = findViewById(R.id.mobile_edt);
        mobile_edt.setTypeface(light);
        regpassword_edt = findViewById(R.id.regpassword_edt);
        regpassword_edt.setTypeface(light);
        reg_or_txt = findViewById(R.id.reg_or_txt);
        reg_or_txt.setTypeface(light);

        reg_text_fbLogin = findViewById(R.id.reg_text_fbLogin);
        reg_text_gplusLogin = findViewById(R.id.reg_text_gplusLogin);
        reg_text_fbLogin.setOnClickListener(this);
        reg_text_gplusLogin.setOnClickListener(this);
        reg_next_txt = findViewById(R.id.reg_next_txt);
        reg_next_txt.setTypeface(bold);
        reg_next_txt.setOnClickListener(this);

        t_c_chk =findViewById(R.id.t_c_chk);
        t_c_textview = findViewById(R.id.t_c_textview);


        log_rl = findViewById(R.id.log_rl);
        reg_rl = findViewById(R.id.reg_rl);
        checkInternet = NetworkChecking.isConnected(this);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        SpannableString ss = new SpannableString("I agree to Terms and Conditions");
        SpannableString ss1 = new SpannableString(" & Privacy policy .");

        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "in.innasoft.gofarmz",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView)
            {
                Intent go=new Intent(LoginActivity.this, TermsConditionsAndPrivacy.class);
                go.putExtra("from","NORMAL");
                startActivity(go);
            }
            @Override
            public void updateDrawState(TextPaint ds)
            {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ClickableSpan policyclickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView)
            {
                Intent go=new Intent(LoginActivity.this, TermsConditionsAndPrivacy.class);
                go.putExtra("from","PrivacyPolicy");
                startActivity(go);
            }
            @Override
            public void updateDrawState(TextPaint ds)
            {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };
        ss.setSpan(clickableSpan, 11, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan( new UnderlineSpan(), 11, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan( new ForegroundColorSpan(Color.BLACK), 11, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss1.setSpan(policyclickableSpan, 3, 17, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss1.setSpan( new UnderlineSpan(), 3, 17, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss1.setSpan( new ForegroundColorSpan(Color.BLACK), 3, 17, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        t_c_textview.setTypeface(light);
        t_c_textview.setText(ss);
        t_c_textview.append(ss1);
        t_c_textview.setTextColor(Color.parseColor("#05914E"));
        t_c_textview.setMovementMethod(LinkMovementMethod.getInstance());
      //  t_c_textview.setHighlightColor(Color.MAGENTA);


        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.e("Login Sucessful", "Success Facebook Login");
                        JSONObject json = response.getJSONObject();
                        Log.d("LOGFBBB", json.toString());
                        if (json != null) {
                            try {
                                sendFBAccessToken = AccessToken.getCurrentAccessToken().getToken();
                                AccessToken token1 = AccessToken.getCurrentAccessToken();
                                if (token1 != null) {
                                    Log.d("ACCESSESTOKE", AccessToken.getCurrentAccessToken().getToken());
                                }
                                //  Log.d("ACCESSESTOKE", AccessToken.getCurrentAccessToken().getToken());

                                //  verified_data = json.getString("verified");
                                fb_user_id_data = json.getString("id");
                                sendFBUserID = fb_user_id_data;
                                email_data = json.getString("email");
                                name_data = json.getString("name");
                                //  gender_data = json.getString("gender");
                                String profile_ptah = "http://graph.facebook.com/" + fb_user_id_data + "/picture?type=large";
                                String provider = "Facebook";


                                String nm = "", lnm = "";

                                if (name_data.split("\\w+").length > 1) {

                                    lnm = name_data.substring(name_data.lastIndexOf(" ") + 1);
                                    nm = name_data.substring(0, name_data.lastIndexOf(' '));
                                } else {
                                    //  firstName = name;
                                }
                                if(screen.equalsIgnoreCase("Register")){
                                    name_edt.setText(name_data);
                                    email_edt.setText(email_data);
                                }else {


                                    getSocialLoginStatus(fb_user_id_data, email_data, provider, lnm, nm);
                                }
                                Log.d("FACEBOOKPROFILE", nm + "   " + lnm + "    " + name_data + "    " + verified_data + "," + fb_user_id_data + "," + sendFBUserID + ", " + email_data + ", " + email_data + ", " + gender_data + ",," + profile_ptah);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,gender,link,email,verified,birthday");
                //  parameters.putString("fields", "id,email,first_name,last_name,gender");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.e("Login Canceled", "Canceled Facebook Login");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e("Login Error", error.getMessage());

            }
        });

    }

    private void getVerionUpdateData( final String checnkVeriosn)
    {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            String pushNotfyurl = AppUrls.BASE_URL + AppUrls.APP_VERSION;
            Log.d("AVVPURL", pushNotfyurl);

            StringRequest strAllMember = new StringRequest(Request.Method.GET, pushNotfyurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("AVVPURLRESP", response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("status");
                        String message = jsonObject.getString("message");
                        if (successResponceCode.equals("10100"))
                        {
                            JSONObject jsonobjj = jsonObject.getJSONObject("data");
                            android_version=jsonobjj.getString("android_version");
                          //  ios_version=jsonobjj.getString("ios_version");

                            if(!android_version.equalsIgnoreCase(checnkVeriosn))
                            {
                                sd_update = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.NORMAL_TYPE);
                                sd_update.setTitleText("App Update");
                                sd_update.setContentText("Please update your app now to experience our latest enhancements..!");
                                sd_update.setConfirmText("Update Now");
                                sd_update.showCancelButton(true);
                                sd_update.setCanceledOnTouchOutside(false);
                                sd_update.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(final SweetAlertDialog sweetAlertDialog)
                                    {
                                        Intent intent = new Intent();
                                        intent.setAction(Intent.ACTION_VIEW);
                                        intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=in.innasoft.gofarmz&hl=en"));
                                        startActivity(intent);
                                        sd_update.cancel();
                                    }
                                });
                                sd_update.show();
                            }

                        }
                        if(successResponceCode.equals("10200"))
                        {
                        }
                        if(successResponceCode.equals("10300"))
                        {
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progress_indicator.stop();
                    error.getMessage();
                }
            }) {

            };
            strAllMember.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(strAllMember);
        } else {
            progress_indicator.stop();
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
          finishAffinity();
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("TT", "handleSignInResult" + result.isSuccess());
        if (result.isSuccess())
        {

            GoogleSignInAccount acct = result.getSignInAccount();

            Log.d("detailaname", "" + acct.getDisplayName());

            String personName = acct.getDisplayName();
            String email = acct.getEmail();
            String id = acct.getId();
            String profilePicPath = "";
            if (acct != null) {
                if (acct.getPhotoUrl() != null) {
                    profilePicPath = acct.getPhotoUrl().getPath();
                }
            }
            if (personName.split("\\w+").length > 1) {
            lnm = personName.substring(personName.lastIndexOf(" ") + 1);
            nm = personName.substring(0, personName.lastIndexOf(' '));
        } else {
            //  firstName = name;
        }
            String provider = "Google";

             user=personName;gmail=email;
            if(screen.equalsIgnoreCase("Register")){
                name_edt.setText(personName);
                email_edt.setText(email);
            }else {
                getSocialLoginStatus(id, email, provider, lnm, nm);

            }
            Log.e("infoooo", "" + personName + ", email: " + email + ", Image: " + profilePicPath);
        } else {

            // Signed out, show unauthenticated UI.

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN)
        {
            progress_indicator.stop();

            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            Log.d("TAG", "RESULLLL" + result);
            handleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }


    }
    @Override
    public void onClick(View v) {

        if (v == login_txt) {
            reg_rl.setVisibility(View.GONE);
            log_rl.setVisibility(View.VISIBLE);
            reg_txt.setBackgroundColor(this.getResources().getColor(R.color.light_green));
            login_txt.setBackgroundColor(this.getResources().getColor(R.color.green));
        }

        if (v == next_txt)
        {
            next_txt.setBackgroundResource(R.drawable.memberdetail_textview_background_fill_bg);
            next_txt.setTextColor(Color.parseColor("#FFFFFF"));

            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet) {

                final String l_email = username_edt.getText().toString();
                final String l_password = password_edt.getText().toString();
                 if(!isEmailValid(l_email) || l_email.length()==0) {
                     Toast.makeText(LoginActivity.this,"Enter valid email address",Toast.LENGTH_SHORT).show();
                 }else if(l_password.length()==0){
                     Toast.makeText(LoginActivity.this,"Enter password",Toast.LENGTH_SHORT).show();
                 }else {
                     Log.d("LOGINURL",AppUrls.BASE_URL + AppUrls.LOGIN);
                     StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.LOGIN,
                             new Response.Listener<String>() {
                                 @Override
                                 public void onResponse(String response) {
                           Log.d("LOGINRESP",response);
                                     try {
                                         JSONObject jsonObject = new JSONObject(response);
                                         String status = jsonObject.getString("status");

                                         if (status.equals("10100")) {
                                             progress_indicator.stop();
                                             String message = jsonObject.getString("message");

                                             JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                                             String jwt = jsonObject1.getString("jwt");
                                             String user_id = jsonObject1.getString("user_id");
                                             String user_name = jsonObject1.getString("user_name");
                                             String email = jsonObject1.getString("email");
                                             String mobile = jsonObject1.getString("mobile");

                                             Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();

                                             userSessionManager.createUserLoginSession(jwt, user_id, user_name, email, mobile);

                                             Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                             startActivity(intent);
                                             finish();

                                         }

                                         if (status.equals("10300")) {
                                             progress_indicator.stop();
                                             Toast.makeText(LoginActivity.this, "Invalid username or password.", Toast.LENGTH_SHORT).show();

                                         }
                                         if (status.equals("10200")) {
                                             progress_indicator.stop();
                                             Toast.makeText(LoginActivity.this, "Invalid Details.", Toast.LENGTH_SHORT).show();

                                         }

                                         if (status.equals("10400")) {
                                             progress_indicator.stop();
                                             Toast.makeText(LoginActivity.this, "Email Already Exists.", Toast.LENGTH_SHORT).show();

                                         }

                                     } catch (JSONException e) {
                                         e.printStackTrace();
                                     }
                                 }

                             }, new Response.ErrorListener() {
                         @Override
                         public void onErrorResponse(VolleyError error) {
                             error.getMessage();
                         }
                     }) {
                         @Override
                         protected Map<String, String> getParams() {
                             Map<String, String> params = new HashMap<String, String>();
                             params.put("email", l_email);
                             params.put("password", l_password);
                             params.put("browser_id", deviceId);
                             Log.d("Params", l_email + "/n" + l_password + "/n" + deviceId);
                             return params;
                         }

                         @Override
                         public Map<String, String> getHeaders() {
                             Map<String, String> headers = new HashMap<>();
                             headers.put("X-Platform", "ANDROID");
                             headers.put("X-Deviceid", deviceId);
                             Log.d("Headers", deviceId);
                             return headers;
                         }
                     };
                     RequestQueue requestQueue = Volley.newRequestQueue(this);
                     requestQueue.add(stringRequest);
                 }

            } else {
                Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }

        if (v == forgotPassword_txt) {
            Intent intent = new Intent(LoginActivity.this, ForgotPassword.class);
            intent.putExtra("mobile", mobile_edt.getText().toString());
            startActivity(intent);
        }

        if (v == reg_txt) {
            log_rl.setVisibility(View.GONE);
            reg_rl.setVisibility(View.VISIBLE);
            login_txt.setBackgroundColor(this.getResources().getColor(R.color.light_green));
            reg_txt.setBackgroundColor(this.getResources().getColor(R.color.green));
        }

        if(v==text_fbLogin){
            screen="Login";
            LoginManager.getInstance().logOut();
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email"));
        }
        if(v==text_gplusLogin){
            screen="Login";
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            //  updateUI(null);
                            //Toast.makeText(LoginActivity.this,"Logout",Toast.LENGTH_SHORT).show();
                            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                            startActivityForResult(signInIntent, RC_SIGN_IN);
                        }
                    });

        }

        if(v==reg_text_fbLogin){
            screen="Register";
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email"));
        }
        if(v==reg_text_gplusLogin){
            screen="Register";
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }

        if (v == reg_next_txt)
        {
            reg_next_txt.setBackgroundResource(R.drawable.memberdetail_textview_background_fill_bg);
            reg_next_txt.setTextColor(Color.parseColor("#FFFFFF"));

            if (checkInternet) {

                name = name_edt.getText().toString();
                email = email_edt.getText().toString();
                password = regpassword_edt.getText().toString();
                mobile = mobile_edt.getText().toString();
                if(name.length()==0){
                    Toast.makeText(LoginActivity.this,"Please enter name",Toast.LENGTH_SHORT).show();
                }else if(!isEmailValid(email) || email.length()==0) {
                    Toast.makeText(LoginActivity.this,"Please enter valid email",Toast.LENGTH_SHORT).show();
                }else if(mobile.length()==0 || !mobile.matches(MOBILE_REGEX)){
                    Toast.makeText(LoginActivity.this,"Please enter valid moblie no",Toast.LENGTH_SHORT).show();
                }else if(password.length()==0){
                    Toast.makeText(LoginActivity.this,"Please enter password",Toast.LENGTH_SHORT).show();
                }else if(!t_c_chk.isChecked())
                {
                    Toast.makeText(getApplicationContext(), "Please Check Terms & Conditions", Toast.LENGTH_SHORT).show();
                }
                else
                    {
                        Log.d("RLOGRESP",AppUrls.BASE_URL + AppUrls.REGISTRATION);
                    StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.REGISTRATION,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {

                                    Log.d("XXX",response);
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        String status = jsonObject.getString("status");

                                        if (status.equals("10100")) {
                                            progress_indicator.stop();
                                            String message = jsonObject.getString("message");
                                            Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();

                                            Intent intent = new Intent(LoginActivity.this, AccountVerificationActivity.class);
                                            intent.putExtra("mobile", mobile);
                                            intent.putExtra("activity","LOGIN");

                                            startActivity(intent);

                                        }

                                        if (status.equals("10200")) {
                                            progress_indicator.stop();
                                            Toast.makeText(LoginActivity.this, "Invalid Details.", Toast.LENGTH_SHORT).show();

                                        }
                                        if (status.equals("10300")) {
                                            progress_indicator.stop();
                                            Toast.makeText(LoginActivity.this, "Mobile no Already Exist", Toast.LENGTH_SHORT).show();

                                        }

                                        if (status.equals("10400")) {
                                            progress_indicator.stop();
                                            Toast.makeText(LoginActivity.this, "Email Already Exists.", Toast.LENGTH_SHORT).show();

                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                            }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            Toast.makeText(LoginActivity.this,""+error.getMessage(),Toast.LENGTH_LONG).show();
                            error.getMessage();
                        }
                    }) {
                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<String, String>();
                            params.put("name", name);
                            params.put("email", email);
                            params.put("mobile", mobile);
                            params.put("conf_pwd", password);
                            Log.d("Params", name + "/n" + email + "/n" + mobile + "/n" + password);
                            return params;
                        }

                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> headers = new HashMap<>();
                            headers.put("X-Platform", "ANDROID");
                            headers.put("X-Deviceid", deviceId);
                            Log.d("Headers", deviceId);
                            return headers;
                        }
                    };
                        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
                        requestQueue.add(stringRequest);
                }

            } else {
                Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void initializeFacebookSettings() {
        callbackManager = CallbackManager.Factory.create();
        FacebookSdk.sdkInitialize(getApplicationContext());

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void getSocialLoginStatus(final String id, final String email, final String provider, final String l_name, final String f_name) {

        if (checkInternet) {
            Log.d("SOCIALSTATUSURL", AppUrls.BASE_URL + AppUrls.SOCIAL_LOGIN);
            StringRequest strsocialLReq = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SOCIAL_LOGIN, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("SOCIALSTATURESP", response);
                    try {
                        String user_name="",email="",mobile="";
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("status");
                        if (successResponceCode.equals("10100")) {
                            progress_indicator.stop();
                            String message = jsonObject.getString("message");
                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                            String jwt = jsonObject1.getString("jwt");
                            String user_id = jsonObject1.getString("user_id");
                             user_name = jsonObject1.getString("user_name");
                             email = jsonObject1.getString("email");
                             mobile = jsonObject1.getString("mobile");

                            Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                            if(mobile!=null && mobile.length()>0)
                            userSessionManager.createUserLoginSession(jwt, user_id, user_name, email, mobile);
                            else
                                userSessionManager.createUserLoginSession(jwt, user_id, user_name, email, "");

                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();

                        } else if (successResponceCode.equals("10200"))
                        {
                            progress_indicator.stop();
                            Toast.makeText(getApplicationContext(), "Invalid input..!", Toast.LENGTH_SHORT).show();
                        } else if (successResponceCode.equals("10300")) {
                            progress_indicator.stop();
                            log_rl.setVisibility(View.GONE);
                            reg_rl.setVisibility(View.VISIBLE);
                            login_txt.setBackgroundColor(getResources().getColor(R.color.light_green));
                            reg_txt.setBackgroundColor(getResources().getColor(R.color.green));
                            if(provider.equalsIgnoreCase("Facebook")){
                                name_edt.setText(name_data);
                                email_edt.setText(email_data);
                            }else{
                                name_edt.setText(user);
                                email_edt.setText(gmail);
                            }


                        } else if (successResponceCode.equals("10400")) {
                            progress_indicator.stop();
                            Toast.makeText(getApplicationContext(), "Account is deactivated, please contact Admin.!", Toast.LENGTH_SHORT).show();
                        } else if (successResponceCode.equals("10500")) {
                            progress_indicator.stop();
                            Toast.makeText(getApplicationContext(), "Account is blocked, please contact Admin.!", Toast.LENGTH_SHORT).show();
                        } else if (successResponceCode.equals("10600")) {
                            progress_indicator.stop();
                            Toast.makeText(getApplicationContext(), "Invalid Login...!", Toast.LENGTH_SHORT).show();
                        } else if (successResponceCode.equals("10700")) {
                            progress_indicator.stop();
                            Toast.makeText(getApplicationContext(), "It seems you are login from different device...!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    error.getMessage();
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("email", email);
                    params.put("browser_id", deviceId);
                    params.put("provider", provider);
                    params.put("identifier", id);
                    params.put("name", f_name+" "+l_name);
                    Log.d("REGPARAMManual", params.toString());

                    return params;
                }
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("X-Platform", "ANDROID");
                    headers.put("X-Deviceid", deviceId);
                    Log.d("Headers", headers.toString());
                    return headers;
                }
            };

            strsocialLReq.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
            requestQueue.add(strsocialLReq);
        } else {

            Toast.makeText(getApplicationContext(), "No Internet Connection Please Check Your Internet Connection..!!", Toast.LENGTH_SHORT).show();

        }


    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
       return isValid;
    }


   /* String MOBILE_REGEX = "^[789]\\d{9}$";

        if (!mobile.matches(MOBILE_REGEX)) {
        usermobile_til.setError("Invalid Mobile Number");
        result = false;
    }else {
        usermobile_til.setError(null);
    }*/

}
