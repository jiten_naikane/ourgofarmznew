package in.innasoft.gofarmz.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapters.FormarAdapter;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class ImageNotificationActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close_img,prod_imag;
    TextView tittle_txt,msg_tittle,msg_desp;
    RotateLoading progress_indicator;
    Button purchase_btn;
    Typeface light, regular, bold;
    String DDDD,deviceId, user_id, token;
    UserSessionManager userSessionManager;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_notification);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

         DDDD = getIntent().getStringExtra("GETDATANOTFY");
   //     Log.d("IDIDIDI",DDDD);
        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        progress_indicator = findViewById(R.id.progress_indicator);
        //progress_indicator.start();

        prod_imag = findViewById(R.id.prod_imag);
        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        tittle_txt = findViewById(R.id.tittle_txt);
        tittle_txt.setTypeface(bold);

        msg_tittle = findViewById(R.id.msg_tittle);
        msg_tittle.setTypeface(bold);

        purchase_btn = findViewById(R.id.purchase_btn);
        purchase_btn.setTypeface(bold);
        purchase_btn.setOnClickListener(this);

        msg_desp = findViewById(R.id.msg_desp);
        msg_desp.setTypeface(light);

        getDataDetail();
    }

    public void getDataDetail()
    {
        String url = AppUrls.BASE_URL + AppUrls.GET_IMAGE_NOTIFICATION_DETAIL + user_id+"/notification/"+DDDD+"/details";
        Log.d("LIS", url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            Log.d("LisRESPONSE", response);

                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                            JSONObject jobjData=jsonObject.getJSONObject("data");


                            String id=jobjData.get("id").toString();
                            String title=jobjData.get("title").toString();
                            String message_body=jobjData.get("message").toString();
                            String image=jobjData.get("image").toString();

                            msg_tittle.setText(title);
                            msg_desp.setText(message_body);

                            Picasso.with(ImageNotificationActivity.this)
                                    .load(image)
                                    .placeholder(R.drawable.cart)
                                    .into(prod_imag);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.getMessage();
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization-Basic", token);
                headers.put("x-device-id", deviceId);
                headers.put("x-device-platform", "ANDROID");
                Log.d("GETOTPEADER", "HEADDER " + headers.toString());
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(ImageNotificationActivity.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v)
    {

        if (v == close_img) {

            Intent intent = new Intent(ImageNotificationActivity.this, NotificationsActivity.class);
            startActivity(intent);
        }
        if (v == purchase_btn)
        {


        }
    }

}
