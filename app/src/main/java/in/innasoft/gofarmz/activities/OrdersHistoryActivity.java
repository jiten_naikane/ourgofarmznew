package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapters.OrderHistoryAdapter;
import in.innasoft.gofarmz.models.OrderHistoryModel;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class OrdersHistoryActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close,no_order_img;
    TextView allsponsors_toolbar_title;
    RecyclerView order_history_recyclerview;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token;
    OrderHistoryAdapter myCartAdapter;
    ArrayList<OrderHistoryModel> recordList;
    Typeface light, regular, bold;
    private boolean checkInternet;

    String payurl="",Order_id;
    ProgressDialog pprogressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_history);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);


        pprogressDialog = new ProgressDialog(OrdersHistoryActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);



        no_order_img = findViewById(R.id.no_order_img);
        close = findViewById(R.id.close_img);
        close.setOnClickListener(this);

        allsponsors_toolbar_title = findViewById(R.id.allsponsors_toolbar_title);
        allsponsors_toolbar_title.setTypeface(bold);

        order_history_recyclerview = findViewById(R.id.order_history_recyclerview);
        payurl=getIntent().getStringExtra("payurl");
        Order_id=getIntent().getStringExtra("ORDERID");
        if(payurl!=null && payurl.length()>0){

            Log.d("PPPPPAY",payurl+"///"+Order_id);
            Intent order_detail= new Intent(OrdersHistoryActivity.this,PaymentWebViewActivity.class);
            order_detail.putExtra("payurl",payurl);
            order_detail.putExtra("ORDERID",Order_id);
            startActivity(order_detail);
            /*Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(payurl));
            startActivity(myIntent);*/
        }

        getOrderHisotryData();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getOrderHisotryData();
    }

    private void getOrderHisotryData() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            String myGroupurl = AppUrls.BASE_URL + AppUrls.GET_ORDER_HISTORY + user_id + "/orders";
            Log.d("MYGROUPURL", myGroupurl);

            StringRequest strAllMember = new StringRequest(Request.Method.GET, myGroupurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pprogressDialog.show();
                    Log.d("MYGROUPRESP", response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("status");
                        if (successResponceCode.equals("10100")) {

                            pprogressDialog.dismiss();
                            JSONObject jsonObjectData = jsonObject.getJSONObject("data");
                            JSONArray jsonArray = jsonObjectData.getJSONArray("recordData");

                            recordList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jdataobj = jsonArray.getJSONObject(i);
                                OrderHistoryModel mygroup = new OrderHistoryModel(jdataobj);
                                recordList.add(mygroup);
                            }

                            myCartAdapter = new OrderHistoryAdapter(recordList, OrdersHistoryActivity.this, R.layout.row_order_history);
                            order_history_recyclerview.setNestedScrollingEnabled(false);
                            order_history_recyclerview.setLayoutManager(new LinearLayoutManager(OrdersHistoryActivity.this, LinearLayoutManager.VERTICAL, false));
                            order_history_recyclerview.setItemAnimator(new DefaultItemAnimator());
                            order_history_recyclerview.setAdapter(myCartAdapter);
                        }
                       /* if (successResponceCode.equals("10200")) {
                            pprogressDialog.dismiss();
                            Toast.makeText(OrdersHistoryActivity.this, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                        }*/
                        else {
                            pprogressDialog.dismiss();
                            no_order_img.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    Log.e("Error", error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    Log.d("sdafds", "adsf " + headers.toString());
                    return headers;
                }
            };
            strAllMember.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(strAllMember);
        } else {
            pprogressDialog.dismiss();
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent it=new Intent(OrdersHistoryActivity.this, MainActivity.class);
        startActivity(it);
        finish();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.close_img){
            Intent it=new Intent(OrdersHistoryActivity.this, MainActivity.class);
            startActivity(it);
            finish();
        }
           // finish();
    }
}
