package in.innasoft.gofarmz.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.victor.loading.rotate.RotateLoading;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.utils.AppUrls;

public class OurStoryActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close_img;
    TextView tittle_txt;
    WebView os_web;
    RotateLoading progress_indicator;
    Typeface light, regular, bold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_our_story);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        tittle_txt = findViewById(R.id.tittle_txt);
        tittle_txt.setTypeface(bold);

        progress_indicator = findViewById(R.id.progress_indicator);
        progress_indicator.start();

        os_web = findViewById(R.id.os_web);

        os_web.setInitialScale(1);
        os_web.getSettings().setJavaScriptEnabled(true);
        os_web.getSettings().setLoadWithOverviewMode(true);
        os_web.getSettings().setUseWideViewPort(true);
        os_web.getSettings().setBuiltInZoomControls(true);
        os_web.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        os_web.setScrollbarFadingEnabled(false);
        os_web.setWebViewClient(new WebViewClient());
        CookieManager.getInstance().setAcceptCookie(true);
        os_web.loadUrl(AppUrls.BASE_URL+AppUrls.OUR_STORY);

        os_web.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {

                progress_indicator.stop();

            }
        });

    }

    @Override
    public void onClick(View v) {

        if (v == close_img){

            Intent intent = new Intent(OurStoryActivity.this, MainActivity.class);
            startActivity(intent);
        }
        
    }
}
