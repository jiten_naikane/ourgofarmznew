package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.Config;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener,RadioGroup.OnCheckedChangeListener {

    EditText picode_edt, email_edt;
    RadioGroup rg;
    RadioButton male_rb, female_rb;
   ImageView close_img;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    String deviceId, token, user_id, mobile_verufy_status, mobile,email,gender,name,gender_value;
    Typeface light, regular, bold;
    TextView address_text, user_name, mobile_no, gender_title, logout_text, change_password_text, edit_profile, verify_mob_text;
    Button save_btn;
    ImageView verf_mob_iv;
    RotateLoading progress_indicator;
    RelativeLayout location_layout;
    String fromWhere;
    ProgressDialog pprogressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dummy_activity_profile);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();

        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_id = userDetails.get(UserSessionManager.USER_ID);

        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        fromWhere = bundle.getString("activity");

        pprogressDialog = new ProgressDialog(ProfileActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
      //  progress_indicator = findViewById(R.id.progress_indicator);
       // progress_indicator.start();

        picode_edt = findViewById(R.id.picode_edt);
        picode_edt.setTypeface(light);
        email_edt = findViewById(R.id.email_edt);
        email_edt.setTypeface(light);

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        rg = findViewById(R.id.rg);
        rg.setOnCheckedChangeListener(this);
        male_rb = findViewById(R.id.male_rb);
        male_rb.setTypeface(light);
        female_rb = findViewById(R.id.female_rb);
        female_rb.setTypeface(light);

        logout_text = findViewById(R.id.logout_text);
        logout_text.setTypeface(bold);
        logout_text.setOnClickListener(this);
        change_password_text = findViewById(R.id.change_password_text);
        change_password_text.setTypeface(bold);
        change_password_text.setOnClickListener(this);
        edit_profile = findViewById(R.id.edit_profile);
        edit_profile.setTypeface(bold);
        edit_profile.setOnClickListener(this);

        gender_title = findViewById(R.id.gender_title);
        gender_title.setTypeface(bold);
        location_layout=findViewById(R.id.location_layout);
        address_text = findViewById(R.id.address_text);
        address_text.setTypeface(bold);
        user_name = findViewById(R.id.user_name);
        user_name.setTypeface(bold);
        mobile_no = findViewById(R.id.mobile_no);
        mobile_no.setTypeface(regular);
        mobile_no.setOnClickListener(this);

        save_btn = findViewById(R.id.save_btn);
        save_btn.setTypeface(bold);
        save_btn.setOnClickListener(this);

        verf_mob_iv = findViewById(R.id.verf_mob_iv);
        verify_mob_text = findViewById(R.id.verify_mob_text);
        verify_mob_text.setTypeface(regular);
        verify_mob_text.setOnClickListener(this);

        getProfileDetails();
    }

    private void getProfileDetails() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {

            pprogressDialog.show();
            Log.d("ProfileURLLL",AppUrls.BASE_URL + AppUrls.GETPROFILE + user_id);
            StringRequest string_request = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.GETPROFILE + user_id, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {

                    Log.d("Profile",response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");

                        if (status.equals("10100")) {
                            pprogressDialog.dismiss();
                          //  progress_indicator.stop();
                            JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                            //for (int i = 0; i < jsonObject1.length(); i++) {

                                String id = jsonObject1.getString("id");
                                name = jsonObject1.getString("name");
                                user_name.setText(name);
                               gender = jsonObject1.getString("gender");
                                if (gender.equalsIgnoreCase("Male")) {
                                    male_rb.setChecked(true);
                                    gender_value="Male";
                                } else if (gender.equalsIgnoreCase("Female")) {
                                    female_rb.setChecked(true);
                                    gender_value="Female";
                                }
                                email = jsonObject1.getString("email");
                                email_edt.setText(email);
                                mobile = jsonObject1.getString("mobile");
                                if(mobile.equalsIgnoreCase("")|| mobile.equalsIgnoreCase("null"))
                                {
                                    mobile_no.setText("Provide Mobile No");

                                }
                                else
                                 {
                                     mobile_no.setText(mobile);
                                     verf_mob_iv.setVisibility(View.VISIBLE);
                                     verify_mob_text.setVisibility(View.VISIBLE);

                                 }


                                mobile_verufy_status = jsonObject1.getString("mobile_verify_status");
                                if (mobile_verufy_status.equalsIgnoreCase("1"))
                                {
                                    verf_mob_iv.setImageResource(R.drawable.ic_verified);
                                    verify_mob_text.setClickable(false);
                                    verify_mob_text.setText("verified ");
                                }
                                else if (mobile_verufy_status.equalsIgnoreCase("0"))
                                {
                                    String mob=mobile_no.getText().toString();
                                    if(!mob.equalsIgnoreCase("Provide Mobile No"))
                                    {
                                        verify_mob_text.setVisibility(View.VISIBLE);
                                        verf_mob_iv.setImageResource(R.drawable.ic_un_verified);
                                    }
                                    else
                                    {

                                    }


                                }
                                JSONObject tmpObj=jsonObject1.getJSONObject("address");
                                if(tmpObj!=null)
                                {
                                    location_layout.setVisibility(View.VISIBLE);
                                    String address1=tmpObj.get("address_line1").toString();
                                    String address2=tmpObj.get("address_line2").toString();
                                    String area=tmpObj.get("area").toString();
                                    String city=tmpObj.get("city").toString();
                                    String state=tmpObj.get("state").toString();
                                    String country=tmpObj.get("country").toString();
                                    String pincode=tmpObj.get("pincode").toString();
                                    address_text.setText(address1+","+address2+","+area+","+city+","+state+","+country+","+pincode);
                                }



                           // }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pprogressDialog.dismiss();
                  //  progress_indicator.stop();
                    error.getMessage();
                }
            }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("X-Platform", "ANDROID");
                    headers.put("X-Deviceid", deviceId);
                    headers.put("Authorization-Basic", token);
                    Log.d("Heasesa",""+headers.toString());
                    return headers;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(string_request);
        } else {
            pprogressDialog.dismiss();
          //  progress_indicator.stop();
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onClick(final View v) {

        if (v == logout_text) {
            //if (userSessionManager.isUserLoggedIn())
            // {

            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet)
            {
                pprogressDialog.show();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.LOGOUT,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response)
                            {
                                Log.v("LOGOUTRES",response);
                                userSessionManager.logoutUser();

                                pprogressDialog.dismiss();
                                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                               error.getMessage();

                            }
                        }
                ) {
                    @Override
                    protected Response<String> parseNetworkResponse(NetworkResponse response) {
                        if (response.headers == null)
                        {
                            // cant just set a new empty map because the member is final.
                            response = new NetworkResponse(
                                    response.statusCode,
                                    response.data,
                                    Collections.<String, String>emptyMap(), // this is the important line, set an empty but non-null map.
                                    response.notModified,
                                    response.networkTimeMs);


                        }
                        return super.parseNetworkResponse(response);
                    }

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<String, String>();
                        params.put("user_id", user_id);
                        params.put("browser_id", deviceId);
                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("X-Platform", "ANDROID");
                        headers.put("X-Deviceid", deviceId);
                        headers.put("Authorization-Basic", token);
                        return headers;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(ProfileActivity.this);
                requestQueue.add(stringRequest);
            } else {
                Toast.makeText(this, "No Internet connection...!", Toast.LENGTH_SHORT).show();
            }
            //}
        }

        if (v == change_password_text) {
            Intent intent = new Intent(ProfileActivity.this, ChangePassword.class);
            startActivity(intent);
        }

        if (v == edit_profile || v==mobile_no)
        {
            String mob=mobile_no.getText().toString();
            Intent updateIntent = new Intent(ProfileActivity.this, UpdateProfileActivity.class);
            updateIntent.putExtra("name",name);
            updateIntent.putExtra("email",email);
            if(mob.equalsIgnoreCase("Provide Mobile No"))
            {
                updateIntent.putExtra("mobile","");
            }
            else
            {
                updateIntent.putExtra("mobile",mobile);
            }


            if(gender_value!=null && gender_value.length()>0)
            updateIntent.putExtra("gender",gender_value);
            startActivity(updateIntent);

        }

        if (v == save_btn) {

            updateProfile();
        }
        if (v == close_img) {

           finish();
        }
        if (v == verify_mob_text)
        {
            getOTPMobileVerified();

            Intent intent = new Intent(ProfileActivity.this, AccountVerificationActivity.class);
              intent.putExtra("mobile",mobile);
              intent.putExtra("activity",fromWhere);
            startActivity(intent);
            //Resend OTP Service Call
        }
    }

    private void updateProfile() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.UPDATEPROFILE,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                if (status.equals("10100")) {

                                    String message = jsonObject.getString("message");
                                    Toast.makeText(ProfileActivity.this, message, Toast.LENGTH_SHORT).show();

                                    Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();

                                }
                                if (status.equals("10200")) {

                                    Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10300")) {

                                    Toast.makeText(getApplicationContext(), "User Not Exist..", Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10400")) {

                                    Toast.makeText(getApplicationContext(), "Unable to Send Otp..", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            error.getMessage();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    //   params.put("name", name_edt.getText().toString());
                    //     params.put("email", email_edt.getText().toString());
                    //     params.put("mobile", mobile_edt.getText().toString());
                    if (male_rb.isChecked()) {
                        params.put("gender", "Male");
                    } else if (female_rb.isChecked()) {
                        params.put("gender", "Female");
                    }

                    return params;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("X-Platform", "ANDROID");
                    headers.put("X-Deviceid", deviceId);
                    headers.put("Authorization-Basic", token);
                    return headers;
                }

            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ProfileActivity.this);
            requestQueue.add(stringRequest);
        } else {

            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void getOTPMobileVerified() {
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.RESENDOTP,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                if (status.equals("10100")) {

                                    String message = jsonObject.getString("message");
                                    Toast.makeText(ProfileActivity.this, message, Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10200")) {

                                    Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10300")) {

                                    Toast.makeText(getApplicationContext(), "User Not Exist..", Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10400")) {

                                    Toast.makeText(getApplicationContext(), "Unable to Send Otp..", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            error.getMessage();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("mobile", mobile);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    return headers;
                }

            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ProfileActivity.this);
            requestQueue.add(stringRequest);
        } else {

            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if(checkedId==R.id.male_rb){
            male_rb.setChecked(true);
            gender_value="Male";
        }else {
            female_rb.setChecked(true);
            gender_value="Female";
        }

    }
}
