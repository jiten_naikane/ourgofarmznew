package in.innasoft.gofarmz.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chaos.view.PinView;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class ForgotPassword extends AppCompatActivity implements View.OnClickListener {

    TextView otp_txt, txtresend;
    PinView pinView;
    EditText password_edt;
    Button verify_btn;
    Typeface light, regular, bold;
    private boolean checkInternet;
    String mobile, deviceId, send_mobile,senPin;
    UserSessionManager userSessionManager;
    RotateLoading progress_indicator;
    Dialog dialog;
    private Boolean exit = false;
    SmsVerifyCatcher smsVerifyCatcher;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        mobile = bundle.getString("mobile"); // need to check

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);

        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));

        progress_indicator = findViewById(R.id.progress_indicator);
        progress_indicator.start();

        password_edt = findViewById(R.id.password_edt);
        otp_txt = findViewById(R.id.otp_txt);
        otp_txt.setTypeface(bold);
        txtresend = findViewById(R.id.txtresend);
        txtresend.setPaintFlags(txtresend.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtresend.setTypeface(light);
        txtresend.setOnClickListener(this);

        pinView = findViewById(R.id.pinView);
        pinView.setTypeface(light);

        verify_btn = findViewById(R.id.verify_btn);
        verify_btn.setTypeface(light);
        verify_btn.setOnClickListener(this);


        getAlertBox();
        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = message.replaceAll("[^0-9]", "");
                pinView.setText(code);
                Log.v("message >>> ", message.replaceAll("[^0-9]", ""));
            }
        });
    }

    private void getAlertBox()
    {
        dialog = new Dialog(ForgotPassword.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog_mobile);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.rounded_layout_dialog);
         TextView dialog_title = dialog.findViewById(R.id.dialog_title);
        final EditText edt_send_mobile = dialog.findViewById(R.id.edt_send_mobile);
        final TextView sendmobileText =  dialog.findViewById(R.id.sendmobileText);
        final ImageView close_dialog =  dialog.findViewById(R.id.close_dialog);
        dialog_title.setTypeface(bold);
        sendmobileText.setTypeface(bold);
        edt_send_mobile.setTypeface(bold);


        dialog.setCanceledOnTouchOutside(false);
        sendmobileText.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.d("eererere","tdgssdfertdf");
                sendmobileText.setClickable(false);
                sendmobileText.setFocusableInTouchMode(false);
                 send_mobile=edt_send_mobile.getText().toString();

                if(send_mobile == null || send_mobile.equals("") || send_mobile.length() < 0)
                {
                    edt_send_mobile.setError("Please Enter Mobile No...!");
                }
                else
                {
                    dialog.cancel();
                    Log.d("eererere",send_mobile);
                    getOTP(send_mobile);


                }
            }
        });

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
              dialog.dismiss();
              finish();
            }
        });


        dialog.show();

    }
    @Override
    protected void onStart() {
        super.onStart();
        smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        smsVerifyCatcher.onStop();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void getOTP(final String send_mobile)
    {
        Log.d("fsdafsdf",send_mobile);
        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {

            Log.d("RESESURL",AppUrls.BASE_URL + AppUrls.FORGOTOTP);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.FORGOTOTP,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            Log.d("RESES",response);

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                if (status.equals("10100")) {
                                    progress_indicator.stop();

                                    String message = jsonObject.getString("message");
                                    Toast.makeText(ForgotPassword.this, message, Toast.LENGTH_SHORT).show();

                                }
                                if (status.equals("10200")) {
                                    progress_indicator.stop();
                                    Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10300")) {
                                    progress_indicator.stop();
                                    finish();
                                    Toast.makeText(getApplicationContext(), "User Not Exist..", Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10400")) {
                                    progress_indicator.stop();
                                    Toast.makeText(getApplicationContext(), "Unable to Send Otp..", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {


                            error.getMessage();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("mobile", send_mobile);
                    Log.d("MMMMMMM",send_mobile);
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("X-Platform", "ANDROID");
                    headers.put("X-Deviceid", deviceId);
                    return headers;
                }

            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(ForgotPassword.this);
            requestQueue.add(stringRequest);
        } else {

            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onClick(View v) {

        if (v == verify_btn) {
            verifyOtp();
        }

        if (v == txtresend)
        {

          //  getOTP();
        }
    }

    private boolean validate() {
        boolean result = true;
        String otp = pinView.getText().toString().trim();
        if ((otp == null || otp.equals("")) || otp.length() != 6) {
            pinView.setError("Invalid OTP");
            result = false;
        }
        return result;
    }

    public void verifyOtp() {
        checkInternet = NetworkChecking.isConnected(this);
        if (validate()) {

            if (checkInternet) {//716504
                Log.d("REESDSURLL",AppUrls.BASE_URL + AppUrls.VERIFYFORGOTOTP);
                StringRequest strveriFyOtp = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.VERIFYFORGOTOTP, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("OOOOOO",response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String successResponceCode = jsonObject.getString("status");
                            String message = jsonObject.getString("message");
                            if (successResponceCode.equals("10100"))
                            {
                                progress_indicator.stop();
                                Intent intent = new Intent(ForgotPassword.this, LoginActivity.class);
                                startActivity(intent);
                                Toast.makeText(ForgotPassword.this,"Password Reset Succesfully..!", Toast.LENGTH_SHORT).show();



                            }
                            if (successResponceCode.equals("10200")) {
                                progress_indicator.stop();
                                Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                            }

                            if (successResponceCode.equals("10300")) {
                                progress_indicator.stop();
                                Toast.makeText(getApplicationContext(), "User Not Exist..!", Toast.LENGTH_SHORT).show();
                            }
                            if (successResponceCode.equals("10400")) {
                                progress_indicator.stop();
                                Toast.makeText(getApplicationContext(), "Unable to process your request, Try again.!", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        error.getMessage();
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("otp", pinView.getText().toString());
                        params.put("mobile", send_mobile);
                        params.put("browser_id", deviceId);
                        params.put("new_pwd", password_edt.getText().toString());
                        Log.d("XXXX",send_mobile+"----"+pinView.getText().toString()+"---"+deviceId+"---"+password_edt.getText().toString());
                        return params;
                    }

                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("X-Platform", "ANDROID");
                        headers.put("X-Deviceid", deviceId);
                        return headers;
                    }
                };
                strveriFyOtp.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                RequestQueue requestQueue = Volley.newRequestQueue(ForgotPassword.this);
                requestQueue.add(strveriFyOtp);
            } else {
                Toast.makeText(ForgotPassword.this, "No Internet Connection...!", Toast.LENGTH_LONG).show();
            }
        }
    }



}
