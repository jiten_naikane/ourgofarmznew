package in.innasoft.gofarmz.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.victor.loading.rotate.RotateLoading;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.utils.AppUrls;

public class FarmerNetworkActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close_img;
    TextView tittle_txt;
    WebView fn_web;
    RotateLoading progress_indicator;
    Typeface light, regular, bold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_network);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        tittle_txt = findViewById(R.id.tittle_txt);
        tittle_txt.setTypeface(bold);

        progress_indicator = findViewById(R.id.progress_indicator);
        progress_indicator.start();

        fn_web = findViewById(R.id.fn_web);

        fn_web.setInitialScale(1);
        fn_web.getSettings().setJavaScriptEnabled(true);
        fn_web.getSettings().setLoadWithOverviewMode(true);
        fn_web.getSettings().setUseWideViewPort(true);
        fn_web.getSettings().setBuiltInZoomControls(true);
        fn_web.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        fn_web.setScrollbarFadingEnabled(false);
        fn_web.setWebViewClient(new WebViewClient());
        CookieManager.getInstance().setAcceptCookie(true);
        fn_web.loadUrl(AppUrls.BASE_URL+AppUrls.FARMER_NETWORK);

        fn_web.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {

                progress_indicator.stop();

            }
        });

    }

    @Override
    public void onClick(View v) {

        if (v == close_img) {
            Intent intent = new Intent(FarmerNetworkActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }
}
