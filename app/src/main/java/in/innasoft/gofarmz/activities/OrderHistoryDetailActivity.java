package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapters.OrderHisDetailAdapter;
import in.innasoft.gofarmz.adapters.OrderTrackingAdapter;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class OrderHistoryDetailActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView recycycle_view_product, recycler_view_track;
    OrderHisDetailAdapter orderHisDetailAdapter;
    OrderTrackingAdapter orderTrackingAdapter;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token, order_ID;
    Map<String, Object> orderDetailList = new HashMap<>();
    ArrayList<Object> orderDetailProduct = new ArrayList<>();
    ArrayList<Object> orderTrackProduct = new ArrayList<>();
    Typeface light, regular, bold;
    TextView order_id, order_status_top, order_approv_title, order_approve_date, expected_delevry_title, expected_delevry_date, total_mrp_title, total_mrp_txt,
            discount_title, discounttxt, shipping_charges_tilte, shipping_charges_txt, final_price_title, final_price_txt, payment_mode, customer_name,
            customer_mobile, area_text, address_text, state_country, city_pincode, pro_detail_text, pro_sheepingdetail, pro_pricedetail, pro_trackOrder, toolbar_title;
    TableRow row_discount;
    private boolean mWithLinePadding;
    public final static String EXTRA_WITH_LINE_PADDING = "EXTRA_WITH_LINE_PADDING";
    RotateLoading progress_indicator;
    ImageView close_img;
    LinearLayout ll_expected_date;
    View running_view_line;
    ProgressDialog pprogressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history_detail);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        light = Typeface.createFromAsset(this.getAssets(), this.getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(this.getAssets(), this.getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(this.getAssets(), this.getResources().getString(R.string.bold));

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);

        pprogressDialog = new ProgressDialog(OrderHistoryDetailActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);
        Bundle bundle = getIntent().getExtras();
        order_ID = bundle.getString("ORDERID");
        getOrderDetail(order_ID);


       // progress_indicator = findViewById(R.id.progress_indicator);
       // progress_indicator.start();

        mWithLinePadding = getIntent().getBooleanExtra(OrderHistoryDetailActivity.EXTRA_WITH_LINE_PADDING, false);
        recycler_view_track = findViewById(R.id.recycler_view_track);
        recycycle_view_product = findViewById(R.id.recycycle_view_product);
        row_discount = findViewById(R.id.row_discount);

        order_id = findViewById(R.id.order_id);
        order_id.setTypeface(bold);

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        ll_expected_date = findViewById(R.id.ll_expected_date);
        running_view_line = findViewById(R.id.running_view_line);

        order_status_top = findViewById(R.id.order_status_top);
        order_status_top.setTypeface(light);

        order_approv_title = findViewById(R.id.order_approv_title);
        order_approv_title.setTypeface(light);
        order_approve_date = findViewById(R.id.order_approve_date);
        order_approve_date.setTypeface(light);
        expected_delevry_title = findViewById(R.id.expected_delevry_title);
        expected_delevry_title.setTypeface(light);
        expected_delevry_date = findViewById(R.id.expected_delevry_date);
        expected_delevry_date.setTypeface(light);
        total_mrp_title = findViewById(R.id.total_mrp_title);
        total_mrp_title.setTypeface(bold);
        total_mrp_txt = findViewById(R.id.total_mrp_txt);
        //total_mrp_txt.setTypeface(light);
        discount_title = findViewById(R.id.discount_title);
        discount_title.setTypeface(bold);
        discounttxt = findViewById(R.id.discounttxt);
        //discounttxt.setTypeface(light);
        shipping_charges_tilte = findViewById(R.id.shipping_charges_tilte);
        shipping_charges_tilte.setTypeface(bold);
        shipping_charges_txt = findViewById(R.id.shipping_charges_txt);
        //shipping_charges_txt.setTypeface(light);
        final_price_title = findViewById(R.id.final_price_title);
        final_price_title.setTypeface(bold);
        final_price_txt = findViewById(R.id.final_price_txt);
        //final_price_txt.setTypeface(light);
        payment_mode = findViewById(R.id.payment_mode);
        payment_mode.setTypeface(bold);

        pro_detail_text = findViewById(R.id.pro_detail_text);
        pro_detail_text.setTypeface(bold);
        pro_sheepingdetail = findViewById(R.id.pro_sheepingdetail);
        pro_sheepingdetail.setTypeface(bold);
        pro_pricedetail = findViewById(R.id.pro_pricedetail);
        pro_pricedetail.setTypeface(bold);
        pro_trackOrder = findViewById(R.id.pro_trackOrder);
        pro_trackOrder.setTypeface(bold);

        customer_name = findViewById(R.id.customer_name);
        customer_name.setTypeface(light);
        customer_mobile = findViewById(R.id.customer_mobile);
        //customer_mobile.setTypeface(light);
        area_text = findViewById(R.id.area_text);
        area_text.setTypeface(light);
        address_text = findViewById(R.id.address_text);
        address_text.setTypeface(light);
        state_country = findViewById(R.id.state_country);
        state_country.setTypeface(light);
        city_pincode = findViewById(R.id.city_pincode);
        city_pincode.setTypeface(light);
    }

    public void getOrderDetail(String order_ID)
    {
        pprogressDialog.show();
        String url = AppUrls.BASE_URL + AppUrls.GET_ORDER_DETAIL + user_id + "/orders/" + order_ID;
        Log.d("detailorderURL", url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pprogressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            Log.d("detailorderPONSE", response);

                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");
                            if (status.equalsIgnoreCase("10100")) {

                                Type stringMap = new TypeToken<Map<String, Object>>() {
                                }.getType();
                                Map<String, Object> data = new Gson().fromJson(jsonObject.optString("data"), stringMap);

                                orderDetailList = (Map<String, Object>) data.get("order_details");     //ORDER DETAILS
                                String reference_id = (String) orderDetailList.get("reference_id");
                                String order_confirmationid = (String) orderDetailList.get("order_confirmation_reference_id");
                                String total_mrp_price = (String) orderDetailList.get("total_mrp_price");
                                String discount = (String) orderDetailList.get("discount");
                                String shipping_charges = (String) orderDetailList.get("shipping_charges");
                                String final_price = (String) orderDetailList.get("final_price");
                                String cancel_charges = (String) orderDetailList.get("cancel_charges");
                                String refund_price = (String) orderDetailList.get("refund_price");
                                String pincode = (String) orderDetailList.get("pincode");
                                String medium = (String) orderDetailList.get("medium");
                                String order_status = (String) orderDetailList.get("order_status");
                                String statuss = (String) orderDetailList.get("status");
                                String getaway_name = (String) orderDetailList.get("getaway_name");
                                String ip_address = (String) orderDetailList.get("ip_address");
                                String expected_delivery = (String) orderDetailList.get("expected_delivery");
                                String order_date = (String) orderDetailList.get("order_date");

                                if(statuss.equalsIgnoreCase("1")||statuss.equalsIgnoreCase("4")||statuss.equalsIgnoreCase("6"))
                                {
                                  ll_expected_date.setVisibility(View.VISIBLE);
                                  running_view_line.setVisibility(View.VISIBLE);
                                 expected_delevry_date.setText(expected_delivery);
                                }



                                order_id.setText(order_confirmationid);

                                if (order_status.equalsIgnoreCase("Confirmed")){
                                    order_status_top.setTextColor(Color.parseColor("#05914E"));
                                    order_status_top.setText(order_status);
                                }else if (order_status.equalsIgnoreCase("Pending")){
                                    order_status_top.setTextColor(Color.parseColor("#FF8000"));
                                    order_status_top.setText(order_status);
                                }else if (order_status.equalsIgnoreCase("Failed")){
                                    order_status_top.setTextColor(Color.RED);
                                    order_status_top.setText(order_status);
                                }else if (order_status.equalsIgnoreCase("Cancelled")){
                                    order_status_top.setTextColor(Color.RED);
                                    order_status_top.setText(order_status);
                                }else if (order_status.equalsIgnoreCase("Dispatched")){
                                    order_status_top.setTextColor(Color.parseColor("#5CD199"));
                                    order_status_top.setText(order_status);
                                }else if (order_status.equalsIgnoreCase("Shipped")){
                                    order_status_top.setTextColor(Color.parseColor("#5CD199"));
                                    order_status_top.setText(order_status);
                                }else if (order_status.equalsIgnoreCase("Delivered")){
                                    order_status_top.setTextColor(Color.parseColor("#05914E"));
                                    order_status_top.setText(order_status);
                                }else if (order_status.equalsIgnoreCase("Not Delivered")){
                                    order_status_top.setTextColor(Color.RED);
                                    order_status_top.setText(order_status);
                                }else if (order_status.equalsIgnoreCase("Received")){
                                    order_status_top.setTextColor(Color.BLUE);
                                    order_status_top.setText(order_status);
                                }

                                order_approve_date.setText(order_date);

                                total_mrp_txt.setText(total_mrp_price + "/-");
                                int dis = discount.compareTo("0");
                                if (dis > 0) {
                                    row_discount.setVisibility(View.VISIBLE);
                                    discounttxt.setText(discount + "/-");
                                }
                                shipping_charges_txt.setText(shipping_charges + "/-");
                                final_price_txt.setText(final_price + "/-");
                                payment_mode.setText("Payment Mode - " + getaway_name);

                                orderDetailList = (Map<String, Object>) data.get("address");         //SHIPPING ADDRESS
                                String name = (String) orderDetailList.get("name");
                                String mobile = (String) orderDetailList.get("mobile");
                                String alternate_contact_no = (String) orderDetailList.get("alternate_contact_no");
                                String country = (String) orderDetailList.get("country");
                                String state = (String) orderDetailList.get("state");
                                String city = (String) orderDetailList.get("city");
                                String area = (String) orderDetailList.get("area");
                                String address = (String) orderDetailList.get("address");
                                String pincode_add = (String) orderDetailList.get("pincode");
                                customer_name.setText(name);
                                customer_mobile.setText(mobile + " " + alternate_contact_no);
                                area_text.setText(area);
                                address_text.setText(address);
                                state_country.setText(state + ", " + country);
                                city_pincode.setText(city + " - " + pincode_add);

                                //PRODUCTS LIST DETAIL
                                orderDetailProduct = (ArrayList<Object>) data.get("products");

                                orderHisDetailAdapter = new OrderHisDetailAdapter(OrderHistoryDetailActivity.this, orderDetailProduct, R.layout.row_orderhistory_detail);
                                recycycle_view_product.setHasFixedSize(true);
                                recycycle_view_product.setLayoutManager(new LinearLayoutManager(OrderHistoryDetailActivity.this, LinearLayoutManager.VERTICAL, false));
                                recycycle_view_product.setItemAnimator(new DefaultItemAnimator());
                                recycycle_view_product.setAdapter(orderHisDetailAdapter);

                                //Track Status LIST DETAIL
                                orderTrackProduct = (ArrayList<Object>) data.get("status");
                                orderTrackingAdapter = new OrderTrackingAdapter(OrderHistoryDetailActivity.this, orderTrackProduct, mWithLinePadding);
                                recycler_view_track.setHasFixedSize(true);
                                recycler_view_track.setLayoutManager(new LinearLayoutManager(OrderHistoryDetailActivity.this, LinearLayoutManager.VERTICAL, false));
                                recycler_view_track.setItemAnimator(new DefaultItemAnimator());
                                recycler_view_track.setAdapter(orderTrackingAdapter);

                            }

                            if (status.equalsIgnoreCase("10200")) {
                               // progress_indicator.stop();
                                pprogressDialog.dismiss();
                                Toast.makeText(OrderHistoryDetailActivity.this, "Invalid Input", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.getMessage();
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization-Basic", token);
                headers.put("X-Deviceid", deviceId);
                headers.put("x-device-platform", "ANDROID");
                return headers;
            }

        };
        RequestQueue requestQueue = Volley.newRequestQueue(OrderHistoryDetailActivity.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {

        if (v == close_img) {
            Intent it = new Intent(OrderHistoryDetailActivity.this, OrdersHistoryActivity.class);
            startActivity(it);
            finish();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent it = new Intent(OrderHistoryDetailActivity.this, OrdersHistoryActivity.class);
        startActivity(it);
        finish();
    }
}
