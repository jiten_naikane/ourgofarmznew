package in.innasoft.gofarmz.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.victor.loading.rotate.RotateLoading;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.adapters.CartRecordChild;
import in.innasoft.gofarmz.adapters.MyCartAdapter;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.DividerItemDecorator;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class MyCartActivity extends AppCompatActivity implements View.OnClickListener {

    Button total_btn, proceed_btn;
    RecyclerView cart_recyclerview;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token;
    Double finalTotal;
    MyCartAdapter myCartAdapter;
    ImageView close_img;
    ArrayList<Object> recordList = new ArrayList<Object>();
    ArrayList<Object> recordChildList = new ArrayList<Object>();

    CartRecordChild cartRecordChild;
    String from = "",pid="";
    Typeface light, regular, bold;
    private boolean checkInternet;
    RotateLoading progress_indicator;
    TextView progress_dialog_txt,former_title_txt;
    ProgressDialog pprogressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cart);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);

        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));

     //   progress_indicator = findViewById(R.id.progress_indicator);
      //  progress_indicator.start();

        pprogressDialog = new ProgressDialog(MyCartActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        progress_dialog_txt = findViewById(R.id.progress_dialog_txt);
        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        former_title_txt = findViewById(R.id.former_title_txt);
        former_title_txt.setTypeface(bold);
        total_btn = findViewById(R.id.total_btn);
        //total_btn.setTypeface(bold);
        proceed_btn = findViewById(R.id.proceed_btn);
        //proceed_btn.setTypeface(bold);
        proceed_btn.setOnClickListener(this);
        from = getIntent().getStringExtra("from");
        pid = getIntent().getStringExtra("pid");

        cart_recyclerview = findViewById(R.id.cart_recyclerview);
        cart_recyclerview.setHasFixedSize(true);
        cart_recyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        cart_recyclerview.setItemAnimator(new DefaultItemAnimator());
        myCartAdapter = new MyCartAdapter(MyCartActivity.this, recordList, R.layout.row_cart, from,pid);
        RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(MyCartActivity.this, R.drawable.recycler_view_divider));
        cart_recyclerview.addItemDecoration(dividerItemDecoration);

        getCartData();

    }

    public void getCartData() {

        checkInternet = NetworkChecking.isConnected(this);
        if (checkInternet) {
            pprogressDialog.show();
            String URL = AppUrls.BASE_URL + AppUrls.GETCARTDATA + user_id + "&browser_id=" + deviceId;
            Log.d("CARTURL", URL);

            StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                if (status.equals("10100")) {
                                    pprogressDialog.dismiss();
                                    progress_dialog_txt.setVisibility(View.GONE);

                                    Log.d("CARTRES", response);
                                    cart_recyclerview.setVisibility(View.VISIBLE);
                                    String message = jsonObject.getString("message");
                                    Type stringMap = new TypeToken<Map<String, Object>>() {
                                    }.getType();
                                    Map<String, Object> data = new Gson().fromJson(jsonObject.optString("data"), stringMap);

                                    String currency = (String) data.get("currency");
                                     finalTotal = (Double) data.get("finalTotal");
                                    Double finalDiscount = (Double) data.get("finalDiscount");
                                    recordList = (ArrayList<Object>) data.get("records");
                                    Map<String, Object> recordPos = (Map<String, Object>) recordList.get(0);
                                    String idd = (String) recordPos.get("id");
                                    Double total_price = (Double) recordPos.get("total_price");
                                    Double grand_total = (Double) recordPos.get("grand_total");
                                    String product_id = (String) recordPos.get("product_id");
                                    String images = (String) recordPos.get("images");
                                    String mrp_price = (String) recordPos.get("mrp_price");
                                    String type = (String) recordPos.get("type");
                                    String purchase_quantity = (String) recordPos.get("purchase_quantity");
                                    String unit_name = (String) recordPos.get("unit_name");
                                    Double discount = (Double) recordPos.get("discount");

                                    myCartAdapter = new MyCartAdapter(MyCartActivity.this, recordList, R.layout.row_cart, from,pid);
                                    RecyclerView.ItemDecoration dividerItemDecoration = new DividerItemDecorator(ContextCompat.getDrawable(MyCartActivity.this, R.drawable.recycler_view_divider));
                                    cart_recyclerview.addItemDecoration(dividerItemDecoration);
                                    cart_recyclerview.setAdapter(myCartAdapter);

                                    total_btn.setText("Total : " +  Math.round(finalTotal) );
                                    recordChildList = (ArrayList<Object>) recordPos.get("ESSENTIAL");
                                    // cartRecordChild = new CartRecordChild(MyCartActivity.this, recordChildList);

                                }
                                if (status.equals("10200")) {

                                    Toast.makeText(getApplicationContext(), "Invalid Input..", Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10300")) {

                                    pprogressDialog.dismiss();
                                    myCartAdapter.notifyDataSetChanged();
                                    cart_recyclerview.setVisibility(View.GONE);
                                   // Toast.makeText(getApplicationContext(), "No Data Found..", Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10400")) {

                                    Toast.makeText(getApplicationContext(), "Unable to Send Otp..", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.getMessage();
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
                    headers.put("X-Deviceid", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    return headers;
                }

            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(MyCartActivity.this);
            requestQueue.add(stringRequest);
        } else {
            pprogressDialog.dismiss();
            Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (from.equalsIgnoreCase("Home")) {
            Intent intent = new Intent(MyCartActivity.this, MainActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(MyCartActivity.this, CustomBoxProductsActivity.class);
            intent.putExtra("pid",pid);
            intent.putExtra("from","main");
            startActivity(intent);
            finish();
        }
    }

    public void refreshCartPage() {
        myCartAdapter.notifyDataSetChanged();
        getCartData();
    }

    @Override
    public void onClick(View v) {

        if (v == proceed_btn) {
            checkInternet = NetworkChecking.isConnected(this);
            if (checkInternet)
            {
               /* Intent intent = new Intent(MyCartActivity.this, PaymentDetails.class);
                intent.putExtra("activity", "MyCartActivity");
                startActivity(intent);*/

                String totalprice =total_btn.getText().toString();
                Log.d("TOTOTO",totalprice);
                String[] split = totalprice.split(":");
                int total = Integer.parseInt(split[1].trim());

                if (total >= 300)
                {
                    Intent intent = new Intent(MyCartActivity.this, DeliverySlotsActivity.class);
                    intent.putExtra("activity", "MyCartActivity");
                    startActivity(intent);
                }
                else {
                Toast.makeText(this, "Minimun order is 300/-", Toast.LENGTH_SHORT).show();
            }


            }
            else {
                Toast.makeText(this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
            }
        }
        if (v == close_img){
            if (from.equalsIgnoreCase("Home")) {
                Intent intent = new Intent(MyCartActivity.this, MainActivity.class);
                 startActivity(intent);
            } else {
                Intent intent = new Intent(MyCartActivity.this, CustomBoxProductsActivity.class);
                intent.putExtra("pid",pid);
                intent.putExtra("from","main");
                startActivity(intent);
                finish();
            }
        }
           // finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCartData();
    }
}
