package in.innasoft.gofarmz.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.victor.loading.rotate.RotateLoading;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;

public class SupportActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close_img;
    TextView tittle_txt,about_us_tittle,about_us_desp,mail,phone;
    RotateLoading progress_indicator;
    Typeface light, regular, bold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));

        progress_indicator = findViewById(R.id.progress_indicator);
        //progress_indicator.start();

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);

        tittle_txt = findViewById(R.id.tittle_txt);
        tittle_txt.setTypeface(bold);
        about_us_tittle = findViewById(R.id.about_us_tittle);
        about_us_tittle.setTypeface(bold);
        about_us_desp = findViewById(R.id.about_us_desp);
        about_us_desp.setTypeface(light);
        mail = findViewById(R.id.mail);
        mail.setTypeface(light);
        phone = findViewById(R.id.phone);
        phone.setTypeface(light);
        phone.setOnClickListener(this);
        mail.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        if (v == close_img){

            Intent intent = new Intent(SupportActivity.this, MainActivity.class);
            startActivity(intent);
        }

        if (v == mail)
        {

                String[] recipients = new String[]{"support@gofarmz.com", ""};
                Intent testIntent = new Intent(android.content.Intent.ACTION_SEND);
                testIntent.setType("message/rfc822");
                testIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
                testIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");
                testIntent.putExtra(android.content.Intent.EXTRA_EMAIL, recipients);
                startActivity(testIntent);

        }


        if (v == phone){
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + Uri.encode("+91 8106670757")));
            callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(callIntent);
        }




    }
}
