package in.innasoft.gofarmz.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.victor.loading.rotate.RotateLoading;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;

public class PaymentWebViewActivity extends AppCompatActivity implements View.OnClickListener  {


    ImageView close_img;
    TextView tittle_txt;
    WebView pay_url_web;
    Typeface light, regular, bold;
    RotateLoading progress_indicator;
    String payurl,Order_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_web_view);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));

        payurl=getIntent().getStringExtra("payurl");

        close_img = findViewById(R.id.close_img);
        close_img.setOnClickListener(this);
        Order_id=getIntent().getStringExtra("ORDERID");
        Log.d("OSPSO",payurl+"////"+Order_id);
        tittle_txt = findViewById(R.id.tittle_txt);
        tittle_txt.setTypeface(bold);

        progress_indicator = findViewById(R.id.progress_indicator);
        progress_indicator.start();


        pay_url_web = findViewById(R.id.pay_url_web);
        pay_url_web.setInitialScale(1);
        pay_url_web.getSettings().setJavaScriptEnabled(true);
        pay_url_web.getSettings().setBuiltInZoomControls(true);
        pay_url_web.getSettings().setLoadWithOverviewMode(true);
        pay_url_web.getSettings().setUseWideViewPort(true);
        pay_url_web.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        pay_url_web.setScrollbarFadingEnabled(false);
       // pay_url_web.setWebViewClient(new WebViewClient());
     //   CookieManager.getInstance().setAcceptCookie(true);


        pay_url_web.setWebViewClient(new MyBrowser());
        pay_url_web.loadUrl(payurl);
       /* {

            public void onPageFinished(WebView view, String url) {

                progress_indicator.stop();

            }


        });*/





       // pay_url_web.goBack();
    }


    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            view.addJavascriptInterface(new Object()
            {
                @JavascriptInterface
                public void performClick() throws Exception //method which you call on button click on HTML page
                {

                    Intent order_detail= new Intent(PaymentWebViewActivity.this,OrderHistoryDetailActivity.class);
                    order_detail.putExtra("ORDERID",Order_id);
                    startActivity(order_detail);
                   /* Log.d("LOGIN::", "Clicked");
                    Toast.makeText(PaymentWebViewActivity.this, "Login clicked", Toast.LENGTH_LONG).show();*/
                }
            }, "BtnLogin");// identify which button you click
            return true;
        }
    }



    @Override
    public void onClick(View v) {

        if (v == close_img)
        {
            Intent order_detail= new Intent(PaymentWebViewActivity.this,OrderHistoryDetailActivity.class);
            order_detail.putExtra("ORDERID",Order_id);
            startActivity(order_detail);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent order_detail= new Intent(PaymentWebViewActivity.this,OrderHistoryDetailActivity.class);
        order_detail.putExtra("ORDERID",Order_id);
        startActivity(order_detail);
    }
}
