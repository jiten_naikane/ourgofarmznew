package in.innasoft.gofarmz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.itemClickListerners.CountriesItemClickListener;

public class DeliverySlotHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView day_text,time_slot_text;
    CountriesItemClickListener countriesItemClickListener;

    public DeliverySlotHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        day_text = itemView.findViewById(R.id.day_text);
        time_slot_text = itemView.findViewById(R.id.time_slot_text);

    }

    @Override
    public void onClick(View view) {

        this.countriesItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(CountriesItemClickListener ic)
    {
        this.countriesItemClickListener = ic;
    }
}
