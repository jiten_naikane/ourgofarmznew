package in.innasoft.gofarmz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.itemClickListerners.AddressesItemClickListener;

public class AddressesHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView edit_text,delete_text;
    public TextView name_txt,address_one_txt,address_two_txt,area_txt,city_txt,state_txt,pincode_txt,mobile_txt,selectAddText;
    AddressesItemClickListener addressesItemClickListener;

    public AddressesHolder(View itemView) {
        super(itemView);
      //  itemView.setOnClickListener(this);

        edit_text = itemView.findViewById(R.id.edit_text);
        delete_text = itemView.findViewById(R.id.delete_text);
        name_txt = itemView.findViewById(R.id.name_txt);
        address_one_txt = itemView.findViewById(R.id.address_one_txt);
        address_two_txt = itemView.findViewById(R.id.address_two_txt);
        area_txt = itemView.findViewById(R.id.area_txt);
        city_txt = itemView.findViewById(R.id.city_txt);
        state_txt = itemView.findViewById(R.id.state_txt);
        pincode_txt = itemView.findViewById(R.id.pincode_txt);
        mobile_txt = itemView.findViewById(R.id.mobile_txt);
        selectAddText = itemView.findViewById(R.id.selectAddText);

    }

    @Override
    public void onClick(View view) {

        this.addressesItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(AddressesItemClickListener ic)
    {
        this.addressesItemClickListener = ic;
    }
}
