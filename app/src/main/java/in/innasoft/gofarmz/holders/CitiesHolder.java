package in.innasoft.gofarmz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.itemClickListerners.CitiesItemClickListener;

public class CitiesHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView city_txt;
    CitiesItemClickListener citiesItemClickListener;

    public CitiesHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        city_txt = itemView.findViewById(R.id.city_txt);

    }

    @Override
    public void onClick(View view) {

        this.citiesItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(CitiesItemClickListener ic)
    {
        this.citiesItemClickListener = ic;
    }
}
