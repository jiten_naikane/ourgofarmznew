package in.innasoft.gofarmz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.itemClickListerners.MyCartItemClickListener;



public class PromoCodeHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
   // public RadioButton code_radio_dynamic_button;
    public TextView pro_coupon_code,pro_descrption,apply_text;
    MyCartItemClickListener myCartItemClickListener;

    public PromoCodeHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
      //  code_radio_dynamic_button = itemView.findViewById(R.id.code_radio_dynamic_button);
        pro_coupon_code = itemView.findViewById(R.id.pro_coupon_code);
        pro_descrption = itemView.findViewById(R.id.pro_descrption);
        apply_text = itemView.findViewById(R.id.apply_text);
    }

    @Override
    public void onClick(View view) {

        this.myCartItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(MyCartItemClickListener ic) {
        this.myCartItemClickListener = ic;
    }
}
