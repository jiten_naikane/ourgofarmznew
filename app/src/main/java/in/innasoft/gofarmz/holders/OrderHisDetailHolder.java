package in.innasoft.gofarmz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.itemClickListerners.MyCartItemClickListener;

public class OrderHisDetailHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView product_img,delete_img,rupee_img;
    public TextView qty_btn;
    public TextView product_name,product_type,view_txt,price_txt;
    private MyCartItemClickListener myCartItemClickListener;

    public OrderHisDetailHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        product_img = itemView.findViewById(R.id.product_img);
        delete_img = itemView.findViewById(R.id.delete_img);
        qty_btn = itemView.findViewById(R.id.qty_btn);
        product_name = itemView.findViewById(R.id.product_name);
        product_type = itemView.findViewById(R.id.product_type);
        view_txt = itemView.findViewById(R.id.view_txt);
        rupee_img = itemView.findViewById(R.id.rupee_img);
        price_txt = itemView.findViewById(R.id.price_txt);

    }

    @Override
    public void onClick(View view) {

        this.myCartItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(MyCartItemClickListener ic)
    {
        this.myCartItemClickListener = ic;
    }
}
