package in.innasoft.gofarmz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.itemClickListerners.StatesItemClickListener;

public class StatesHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView state_txt;
    StatesItemClickListener statesItemClickListener;

    public StatesHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        state_txt = itemView.findViewById(R.id.state_txt);

    }

    @Override
    public void onClick(View view) {

        this.statesItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(StatesItemClickListener ic)
    {
        this.statesItemClickListener = ic;
    }
}
