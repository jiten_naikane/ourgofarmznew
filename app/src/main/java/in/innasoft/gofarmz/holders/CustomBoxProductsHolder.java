package in.innasoft.gofarmz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.itemClickListerners.CustomBoxProductsItemClickListener;

public class CustomBoxProductsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView product_img,minus_img,add_img;
    public TextView product_name_txt,qty_txt,price_txt;
    public Spinner qty_spinner;
    public CheckBox add_chk;
    private CustomBoxProductsItemClickListener customBoxProductsItemClickListener;

    public CustomBoxProductsHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        product_img = itemView.findViewById(R.id.product_img);
        minus_img = itemView.findViewById(R.id.minus_img);
        add_img = itemView.findViewById(R.id.add_img);
        product_name_txt = itemView.findViewById(R.id.product_name_txt);
        qty_spinner = itemView.findViewById(R.id.qty_spinner);
        add_chk = itemView.findViewById(R.id.add_chk);
        qty_txt = itemView.findViewById(R.id.qty_txt);
        price_txt = itemView.findViewById(R.id.price_txt);

    }

    @Override
    public void onClick(View view) {

        this.customBoxProductsItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(CustomBoxProductsItemClickListener ic)
    {
        this.customBoxProductsItemClickListener = ic;
    }
}
