package in.innasoft.gofarmz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.itemClickListerners.AreasItemClickListener;

public class AreasHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView area_txt;
    AreasItemClickListener areasItemClickListener;

    public AreasHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        area_txt = itemView.findViewById(R.id.area_txt);

    }

    @Override
    public void onClick(View view) {

        this.areasItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(AreasItemClickListener ic)
    {
        this.areasItemClickListener = ic;
    }
}
