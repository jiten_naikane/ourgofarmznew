package in.innasoft.gofarmz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.itemClickListerners.CategoryItemClickListener;

public class CategoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView category_img;
    public TextView category_name_txt;
    public View arrow_img;
    public RelativeLayout relativ_category,relativ_ca;
    private CategoryItemClickListener categoryListItemClickListener;

    public CategoryHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        category_img = itemView.findViewById(R.id.category_img);
        category_name_txt = itemView.findViewById(R.id.category_name_txt);
        arrow_img = itemView.findViewById(R.id.arrow_img);
        relativ_category = itemView.findViewById(R.id.relativ_category);

    }

    @Override
    public void onClick(View view) {

        this.categoryListItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(CategoryItemClickListener ic)
    {
        this.categoryListItemClickListener = ic;
    }
}
