package in.innasoft.gofarmz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.itemClickListerners.CountriesItemClickListener;

public class CountriesHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView country_txt;
    CountriesItemClickListener countriesItemClickListener;

    public CountriesHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        country_txt = itemView.findViewById(R.id.country_txt);

    }

    @Override
    public void onClick(View view) {

        this.countriesItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(CountriesItemClickListener ic)
    {
        this.countriesItemClickListener = ic;
    }
}
