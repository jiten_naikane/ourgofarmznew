package in.innasoft.gofarmz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.itemClickListerners.AreasItemClickListener;

public class NotificationHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView notify_title_name,notify_description,notify_dateandtime;
    public ImageView imag_notification;
    public LinearLayout linear_layout_row;
    AreasItemClickListener areasItemClickListener;

    public NotificationHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        imag_notification = itemView.findViewById(R.id.imag_notification);
        notify_title_name = itemView.findViewById(R.id.notify_title_name);
        notify_description = itemView.findViewById(R.id.notify_description);
        notify_dateandtime = itemView.findViewById(R.id.notify_dateandtime);
        linear_layout_row = itemView.findViewById(R.id.linear_layout_row);

    }

    @Override
    public void onClick(View view) {

        this.areasItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(AreasItemClickListener ic)
    {
        this.areasItemClickListener = ic;
    }
}
