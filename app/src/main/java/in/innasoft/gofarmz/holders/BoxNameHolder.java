package in.innasoft.gofarmz.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.itemClickListerners.BoxNameItemClickListener;

public class BoxNameHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public Button box_btn;
    BoxNameItemClickListener boxNameItemClickListener;

    public BoxNameHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

        box_btn = itemView.findViewById(R.id.box_btn);

    }

    @Override
    public void onClick(View view) {

        this.boxNameItemClickListener.onItemClick(view, getLayoutPosition());
    }

    public void setItemClickListener(BoxNameItemClickListener ic)
    {
        this.boxNameItemClickListener = ic;
    }
}
