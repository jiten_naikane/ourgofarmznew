package in.innasoft.gofarmz.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.ImageNotificationActivity;
import in.innasoft.gofarmz.activities.NotificationsActivity;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    private NotificationUtils notificationUtils;
    String title,message,image,notification_id;
    Bitmap bitmap;
    Intent intent;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d("sssss", "From" + remoteMessage.getData());
      //  Log.d("tttttt", "From1" + remoteMessage.getNotification());

        Log.d(TAG, "From" + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0)
        {
            Log.d(TAG, "Messagedatapayload" + remoteMessage.getData());
            Map<String, String> params = remoteMessage.getData();
            JSONObject object = new JSONObject(params);
            Log.d("JSON_OBJECT", object.toString());
            try {

                title=object.getString("title");
                message=object.getString("message");
                image=object.getString("image");
                notification_id=object.getString("notification_id");

                Log.d("DATAOBJECT",title+"////"+message+"///"+image);
                bitmap = getBitmapfromUrl(image);

                if(image != null && !image.isEmpty())
                {
                    Log.d("DDDDDd","DDDDDD");
                    Intent myintent = new Intent(this, ImageNotificationActivity.class);
                    myintent.putExtra("body", message);
                    myintent.putExtra("GETDATANOTFY", notification_id);
                    myintent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    NotificationCompat.Builder noBuilder;
                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, myintent, PendingIntent.FLAG_UPDATE_CURRENT);
                    Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    noBuilder = new NotificationCompat.Builder(this)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setAutoCancel(true)
                            .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(bitmap))
                            .setContentTitle(title)
                            .setContentText(message)
                            .setSound(sound)
                            .setContentIntent(pendingIntent);
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    assert notificationManager != null;
                    notificationManager.notify(0, noBuilder.build());
                }
                else
                {
                    Log.d("NNNNNN","NNNNNN");
                    Intent myintent = new Intent(this, NotificationsActivity.class);
                    myintent.putExtra("body", message);
                    myintent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    NotificationCompat.Builder noBuilder;
                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, myintent, PendingIntent.FLAG_UPDATE_CURRENT);
                    Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    noBuilder = new NotificationCompat.Builder(this)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setAutoCancel(true)
                            .setContentTitle(title)
                            .setContentText(message)
                            .setSound(sound)
                            .setContentIntent(pendingIntent);
                    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    assert notificationManager != null;
                    notificationManager.notify(0, noBuilder.build());
                    //handleNotificationWithoutImg(title,message);


                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }


      /*  if (remoteMessage.getNotification() != null) {
           // Log.e(TAG, "NotificationBody" + remoteMessage.getNotification().getBody());

        }*/
    }



    private void handleNotificationWithoutImg(String title, String message)
    {
        Intent myintent = new Intent(this, NotificationsActivity.class);
        myintent.putExtra("body", message);
        myintent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        NotificationCompat.Builder noBuilder;
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, myintent, PendingIntent.FLAG_UPDATE_CURRENT);
        Uri sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        noBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true)
               // .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(imspath))
                .setContentTitle(title)
                .setContentText(message)
                .setSound(sound)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        notificationManager.notify(0, noBuilder.build());
    }


    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }

    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;

        }
    }
}
