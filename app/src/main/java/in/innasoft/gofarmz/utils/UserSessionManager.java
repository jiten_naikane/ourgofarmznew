package in.innasoft.gofarmz.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import java.util.HashMap;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.activities.GetStartedScreensActivity;
import in.innasoft.gofarmz.activities.LoginActivity;

public class UserSessionManager {

    private SharedPreferences pref,preferences;
    private Editor editor,editor2;
    private Context _context;
    private static final String PREFER_NAME = "gofarmz";
    private static final String IS_USER_LOGIN = "IsUserLoggedIn";

    public static final String INTRO_SLIDE = "intorslide";
    public static final String ISFIRTTIMEAPP = "isfirsttime";
    public static final String DEVICE_ID = "device_id";
    public static final String KEY_ACCSES = "access_key";
    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_MOBILE = "user_mobile";
    public static final String USER_EMAIL = "user_email";


    @SuppressLint("CommitPrefEdits")
    public UserSessionManager(Context context){
        this._context = context;
        int PRIVATE_MODE = 0;
        preferences = context.getSharedPreferences(INTRO_SLIDE, PRIVATE_MODE);
        editor2 = preferences.edit();

        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createDeviceId(String deviceId) {
        editor.putString(DEVICE_ID, deviceId);
        editor.apply();
    }

    public void createIsFirstTimeAppLunch()
    {
        editor2.putBoolean(ISFIRTTIMEAPP, true);
        editor2.apply();
    }

    public boolean checkLogin(){

        if(this.isUserLoggedIn()){

            Intent i = new Intent(_context, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);
            return true;
        }
        return false;
    }

    public void createUserLoginSession(String token,String user_id,String username, String email, String mobile) {
        editor.putBoolean(IS_USER_LOGIN, true);
        editor.putString(KEY_ACCSES, token);
        editor.putString(USER_ID, user_id);
        editor.putString(USER_NAME, username);
        editor.putString(USER_EMAIL, email);
        editor.putString(USER_MOBILE, mobile);
        editor.apply();
    }

    public HashMap<String, String> getUserDetails() {

        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_ACCSES, pref.getString(KEY_ACCSES, null));
        user.put(USER_ID, pref.getString(USER_ID, null));
        user.put(USER_NAME, pref.getString(USER_NAME, null));
        user.put(USER_MOBILE, pref.getString(USER_MOBILE, null));
        user.put(USER_EMAIL, pref.getString(USER_EMAIL, null));
        user.put(DEVICE_ID, pref.getString(DEVICE_ID, null));
        return user;
    }


    public void logoutUser(){
        editor.clear();
        editor.apply();
        Intent i = new Intent(_context, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);
    }

    public boolean isUserLoggedIn(){
        return !pref.getBoolean(IS_USER_LOGIN, false);
    }

    public boolean isFirstTimeAppLunch(){
        return preferences.getBoolean(ISFIRTTIMEAPP, false);
    }
}