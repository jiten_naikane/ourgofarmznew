package in.innasoft.gofarmz.utils;

public class AppUrls {

    /*Live URL's*/
   /* public static String BASE_URL = "http://13.232.141.21/";
    public static String IMAGE_URL = "http://13.232.141.21/images/products/";
    public static String BASEIMAGEURL_70x70 = "http://13.232.141.21/images/products/70x70/";
    public static String BASEIMAGEURL_400x400 = "http://13.232.141.21/images/products/400x400/";
    public static String BASEIMAGEURL_280x280 = "http://13.232.141.21/images/products/280x280/";*/


    /*Test URL's*/
    // http://gofarmzv2.learningslot.in/

    public static String BASE_URL = "http://gofarmzv2.learningslot.in/";
    public static String IMAGE_URL = "http://gofarmzv2.learningslot.in//images/products/";
    public static String BASEIMAGEURL_70x70 = "http://gofarmzv2.learningslot.in//images/products/70x70/";
    public static String BASEIMAGEURL_400x400 = "http://gofarmzv2.learningslot.in//images/products/400x400/";
    public static String BASEIMAGEURL_280x280 = "http://gofarmzv2.learningslot.in//images/products/280x280/";


    public static String LOGIN = "api/verify-login";
    public static String SOCIAL_LOGIN = "api/verify-login-social";
    public static String REGISTRATION = "api/registration";
    public static String RESENDOTP = "api/resend-otp";
    public static String VERIFYOTP = "api/verify-otp";
    public static String FORGOTOTP = "api/forgot-password";
    public static String CHANGEPASSWORD = "api/user/change-password";
    public static String VERIFYFORGOTOTP = "api/verify-otp-forgot-password";
    public static String GETPROFILE = "api/user/profile?user_id=";
    public static String UPDATEPROFILE = "api/user/profile";
    public static String CATEGORIES = "api/category/main";
    public static String BOXPRODUCTS = "api/products?main_category=";
    public static String CARTCOUNT = "api/cart/count?user_id=";
    public static String LOGOUT = "api/user/logout";
    public static String ADDTOCART = "api/cart";
    public static String DELETECARTDATA = "api/cart/delete";
    public static String GETCARTDATA = "api/cart?user_id=";
    public static String ADDADDRESS = "api/user/";
    public static String GET_ORDER_HISTORY = "api/user/";
    public static String GET_ORDER_DETAIL = "api/user/";
    public static String GETCOUNTRIES = "api/countries";
    public static String GETSTATES = "api/states?country_id=";
    public static String GETCITIES = "api/cities?state_id=";
    public static String GETAREAS = "api/areas?city_id=";
    public static String VALIDATE_COUPON_CODE = "api/user/";
    public static String GET_CHECKOUT_DATA = "api/checkout"; // getting pay data
    public static String GET_CHECKOUT = "api/checkout";     // do payment transactioon
    public static String GET_FORMAR_DETAILS = "api/farmers/products/";
    public static String APP_BANARES = "api/banners";
    public static String INCRESE_QTY = "api/cart/quantity";
    public static String PROMOCODES = "api/promocodes";
    public static String FCM = "api/fcm-token";
    public static String GET_PUSHNOTIFICATION = "api/user/";
    public static String GET_IMAGE_NOTIFICATION_DETAIL = "api/user/";


    public static String TERMS_CONDITION = "api/urls/terms-conditions";
    public static String PRIVACY_POLICY = "api/urls/privacy-policy";
    public static String FAQ = "api/urls/faqs";
    public static String KNOW_FARMER = "api/urls/know-your-farmer";
    public static String OUR_STORY = "api/urls/our-story";
    public static String FARMER_NETWORK = "api/urls/farmer-network";

    public static String APP_BANNERS_SLIDERS = "api/banners";
    public static String REFER_FARMER = "api/refer_farmer";
    public static String APP_VERSION = "api/app/version";
    public static String BOTTOM_ADS_SCROLL = "api/banners/promo";


    public static String VIEW_CHILD_CART_DATA = "api/cart/items";
    public static String VIEW_CHILD_CART_INCRESE_QTY = "api/cart/update-product-addon";
    public static String DELETE_CHILD_CART = "api/cart/delete-product-addon";

    public static String DELIVERY_SLOT_DATA = "api/delivery-details";

}

