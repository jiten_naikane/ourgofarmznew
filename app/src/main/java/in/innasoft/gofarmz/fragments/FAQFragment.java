package in.innasoft.gofarmz.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.victor.loading.rotate.RotateLoading;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.utils.AppUrls;

/**
 * A simple {@link Fragment} subclass.
 */
public class FAQFragment extends Fragment {

    View view;
    WebView faq_web;
   RotateLoading progress_indicator;

    public FAQFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_faq, container, false);

        faq_web = view.findViewById(R.id.faq_web);
       progress_indicator = view.findViewById(R.id.progress_indicator);
   //    progress_indicator.start();

        faq_web.setInitialScale(1);
        faq_web.getSettings().setJavaScriptEnabled(true);
        faq_web.getSettings().setLoadWithOverviewMode(true);
        faq_web.getSettings().setBuiltInZoomControls(true);
        faq_web.getSettings().setUseWideViewPort(true);
        faq_web.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        faq_web.setScrollbarFadingEnabled(false);
        faq_web.setWebViewClient(new WebViewClient());
        CookieManager.getInstance().setAcceptCookie(true);

        faq_web.loadUrl(AppUrls.BASE_URL+AppUrls.FAQ);

        faq_web.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {

         //   progress_indicator.stop();

            }
        });




        return view;




    }

}
