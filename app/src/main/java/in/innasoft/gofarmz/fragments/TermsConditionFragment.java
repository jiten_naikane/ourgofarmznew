package in.innasoft.gofarmz.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.victor.loading.rotate.RotateLoading;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.utils.AppUrls;

/**
 * A simple {@link Fragment} subclass.
 */
public class TermsConditionFragment extends Fragment {

    View view;
    WebView term_condition_web;
   RotateLoading progress_indicator;

    public TermsConditionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        view= inflater.inflate(R.layout.fragment_terms_condition, container, false);

        term_condition_web = view.findViewById(R.id.term_condition_web);
       progress_indicator = view.findViewById(R.id.progress_indicator);
     //   progress_indicator.start();

        term_condition_web.setInitialScale(1);
        term_condition_web.getSettings().setJavaScriptEnabled(true);
        term_condition_web.getSettings().setLoadWithOverviewMode(true);
        term_condition_web.getSettings().setBuiltInZoomControls(true);
        term_condition_web.getSettings().setUseWideViewPort(true);
        term_condition_web.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        term_condition_web.setScrollbarFadingEnabled(false);

        term_condition_web.setWebViewClient(new WebViewClient());
        CookieManager.getInstance().setAcceptCookie(true);

        term_condition_web.loadUrl(AppUrls.BASE_URL+AppUrls.TERMS_CONDITION);

        term_condition_web.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {

         //     progress_indicator.stop();

            }
        });


        return view;
    }

}
