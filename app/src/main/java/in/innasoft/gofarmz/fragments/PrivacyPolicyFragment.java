package in.innasoft.gofarmz.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.victor.loading.rotate.RotateLoading;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.utils.AppUrls;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrivacyPolicyFragment extends Fragment {

    View view;

    WebView privacyPolicy_web;
    RotateLoading progress_indicator;

    public PrivacyPolicyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view= inflater.inflate(R.layout.fragment_privacy_policy, container, false);

        privacyPolicy_web = view.findViewById(R.id.privacyPolicy_web);
     progress_indicator = view.findViewById(R.id.progress_indicator);
   //  progress_indicator.start();

        privacyPolicy_web.setInitialScale(1);
        privacyPolicy_web.getSettings().setJavaScriptEnabled(true);
        privacyPolicy_web.getSettings().setLoadWithOverviewMode(true);
        privacyPolicy_web.getSettings().setBuiltInZoomControls(true);
        privacyPolicy_web.getSettings().setUseWideViewPort(true);
        privacyPolicy_web.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        privacyPolicy_web.setScrollbarFadingEnabled(false);
        privacyPolicy_web.setWebViewClient(new WebViewClient());
        CookieManager.getInstance().setAcceptCookie(true);

        privacyPolicy_web.loadUrl(AppUrls.BASE_URL+AppUrls.PRIVACY_POLICY);

        privacyPolicy_web.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {

        //   progress_indicator.stop();

            }
        });

        return view;


    }

}
