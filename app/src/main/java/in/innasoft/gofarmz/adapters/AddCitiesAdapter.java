package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.ChooseAddressActivity;
import in.innasoft.gofarmz.activities.EditAddressActivity;
import in.innasoft.gofarmz.filters.AddCitiesFilter;
import in.innasoft.gofarmz.filters.CitiesFilter;
import in.innasoft.gofarmz.holders.CitiesHolder;
import in.innasoft.gofarmz.itemClickListerners.CitiesItemClickListener;
import in.innasoft.gofarmz.models.CitiesModel;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class AddCitiesAdapter extends RecyclerView.Adapter<CitiesHolder> implements Filterable {

    public ArrayList<CitiesModel> citiesModels,filterList;
    AddCitiesFilter filter;
    private ChooseAddressActivity context;
    private LayoutInflater layoutInflater;
    private int resource;
    private Typeface light, regular, bold;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token, user_name, mobile;

    public AddCitiesAdapter(ArrayList<CitiesModel> citiesModels, ChooseAddressActivity context, int resource) {
        this.citiesModels = citiesModels;
        this.filterList = citiesModels;
        this.context = context;
        this.resource = resource;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        light = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));

        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_name = userDetails.get(UserSessionManager.USER_NAME);
        mobile = userDetails.get(UserSessionManager.USER_MOBILE);
        Log.d("SESSIONDATA", deviceId + "/n" + user_id + "/n" + token);
    }

    @Override
    public CitiesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = layoutInflater.inflate(resource, parent, false);
        CitiesHolder slh = new CitiesHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final CitiesHolder holder, final int position) {

        holder.city_txt.setText(citiesModels.get(position).getName());
        holder.city_txt.setTypeface(light);


        holder.setItemClickListener(new CitiesItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                context.setCityName(citiesModels.get(pos).getName(),citiesModels.get(pos).getId());
            }
        });

    }

    @Override
    public int getItemCount() {
        return citiesModels.size();
    }

    @Override
    public Filter getFilter()
    {
        if(filter==null)
        {
            filter = new AddCitiesFilter(filterList,this);
        }

        return filter;
    }

}
