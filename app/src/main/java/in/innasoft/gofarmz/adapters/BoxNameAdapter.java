package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Map;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.holders.BoxNameHolder;
import in.innasoft.gofarmz.itemClickListerners.BoxNameItemClickListener;
import in.innasoft.gofarmz.utils.NetworkChecking;

public class BoxNameAdapter extends RecyclerView.Adapter<BoxNameHolder> {

    private MainActivity context;
    private ArrayList<Object> boxNameList = new ArrayList<Object>();
    private ArrayList<Object> boxProductsList = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private GridView gridview;
    private int resource;
    private Typeface light, regular, bold;
    public SparseBooleanArray selectedButton = new SparseBooleanArray();
    SharedPreferences sharedpreferences;
    public int selectedPos = 0;
    private boolean checkInternet;
    public String  imgPath;

    public BoxNameAdapter(ArrayList<Object> boxNameList, MainActivity context, int layout, GridView gridView) {
        this.boxNameList = boxNameList;
        this.context = context;
        this.gridview = gridView;
        resource = layout;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        light = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));
    }

    @Override
    public BoxNameHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = layoutInflater.inflate(resource, parent, false);
        BoxNameHolder slh = new BoxNameHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final BoxNameHolder holder, final int position) {

        final Map<String, Object> boxPos = (Map<String, Object>) boxNameList.get(position);


       //context.setImagePath(boxPos.get("images").toString());
       /// context.setImagePath(boxPos.get("images").toString());

        holder.box_btn.setText(boxPos.get("pdtName").toString());

        holder.box_btn.setTypeface(light);

        if (selectedButton.get(position)) {

            holder.box_btn.setBackgroundResource(R.drawable.selectedbox_background);
            holder.box_btn.setTextColor(context.getResources().getColor(R.color.white));

        } else {
            holder.box_btn.setBackgroundResource(R.drawable.unselectedbox_background);
            holder.box_btn.setTextColor(context.getResources().getColor(R.color.black));
        }

        holder.box_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet)
                {

                    ((MainActivity) context).price(boxPos.get("price").toString());
                    ((MainActivity) context).productId(boxPos.get("id").toString());
                    ((MainActivity) context).boxpdtName(boxPos.get("pdtName").toString());

                    sharedpreferences = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("position", position);
                    editor.apply();

                    selectedPos = position;
                    selection(position);

                    boxProductsList = (ArrayList<Object>) boxPos.get("ESSENTIAL");
                    BoxProductsAdapter adapter = new BoxProductsAdapter(context, boxProductsList);
                    gridview.setAdapter(adapter);
                } else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.setItemClickListener(new BoxNameItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return boxNameList.size();
    }

    public void selection(int position) {
        selectedButton.clear();
        selectView(position, true);
    }

    public void selectView(int position, boolean value) {

        if (value)
            selectedButton.put(position, value);
        else
            selectedButton.delete(position);

        notifyDataSetChanged();
    }

}
