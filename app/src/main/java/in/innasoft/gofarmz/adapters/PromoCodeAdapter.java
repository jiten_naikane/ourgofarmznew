package in.innasoft.gofarmz.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import java.util.ArrayList;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.PromoCouponListActivity;
import in.innasoft.gofarmz.holders.PromoCodeHolder;
import in.innasoft.gofarmz.itemClickListerners.MyCartItemClickListener;
import in.innasoft.gofarmz.models.PromoCodeModel;

public class PromoCodeAdapter extends RecyclerView.Adapter<PromoCodeHolder> {
    public ArrayList<PromoCodeModel> promolistModels;
    PromoCouponListActivity context;
    LayoutInflater li;
    int resource;
    Typeface regular, bold;
    private RadioButton lastCheckedRB = null;
    public String send_code_Value;

    public PromoCodeAdapter(ArrayList<PromoCodeModel> promolistModels, PromoCouponListActivity context, int resource) {
        this.promolistModels = promolistModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        regular = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));


    }

    @Override
    public PromoCodeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        PromoCodeHolder slh = new PromoCodeHolder(layout);
        return slh;
    }
// public TextView ,pro_descrption,apply_text;
    @Override
    public void onBindViewHolder(final PromoCodeHolder holder, final int position) {

        send_code_Value= promolistModels.get(position).code;

         holder.pro_coupon_code.setText(send_code_Value);

        holder.pro_descrption.setText(Html.fromHtml(promolistModels.get(position).description));

        holder.pro_coupon_code.setTypeface(bold);
        holder.pro_descrption.setTypeface(regular);
        holder.apply_text.setTypeface(regular);


        holder.apply_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                context.setCode(promolistModels.get(position).code);
                context.validateCouponCode(promolistModels.get(position).code);
            }
        });

       /* View.OnClickListener rbClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioButton checked_rb = (RadioButton) v;
                if (lastCheckedRB != null)
                {
                    lastCheckedRB.setChecked(false);
                   send_check_Value = holder.code_radio_dynamic_button.getText().toString();


                }
                lastCheckedRB = checked_rb;
                send_check_Value = holder.code_radio_dynamic_button.getText().toString();
                context.setCode(send_check_Value);
            }
        };
        holder.code_radio_dynamic_button.setOnClickListener(rbClick);*/

        holder.setItemClickListener(new MyCartItemClickListener()
        {
            @Override
            public void onItemClick(View v, int pos) {


            }
        });
    }

    @Override
    public int getItemCount() {
        return this.promolistModels.size();
    }
}
