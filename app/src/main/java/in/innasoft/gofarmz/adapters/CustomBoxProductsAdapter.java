package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.FormarActivity;
import in.innasoft.gofarmz.holders.CustomBoxProductsHolder;
import in.innasoft.gofarmz.itemClickListerners.CustomBoxProductsItemClickListener;
import in.innasoft.gofarmz.models.CustomModel;
import in.innasoft.gofarmz.models.OptionsModel;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class CustomBoxProductsAdapter extends RecyclerView.Adapter<CustomBoxProductsHolder> implements View.OnClickListener {

    private Context context;
    private ArrayList<CustomModel> customBoxProductsModels;
    private ArrayList<OptionsModel> customBoxOptionsModels;
    ArrayList<Object> subProductList = new ArrayList<Object>();
    private LayoutInflater li;
    public String prdo_id;
    private int resource;
    private Typeface light, regular, bold;
    private boolean checkInternet;
    String deviceId, user_id, token, price, qtyprice = "";
    UserSessionManager userSessionManager;
    ArrayList<String> selchkboxlist = new ArrayList<String>();
    public ArrayList<String> cartarr = new ArrayList<String>();
    TextView total_price_txt;
    int total = 0;
    int selectedpos = 0;
    String cartstr = "";
    String productId = "",pid="";
    Map<String, Object> stringObjectMap = new HashMap<>();
    boolean isfirsttime = true;
    boolean iscartfirsttime = true;
    ArrayList<Integer> pricearr = new ArrayList<>();
    ArrayList<Integer> selcetedpricearr = new ArrayList<>();

    public CustomBoxProductsAdapter(Context context, ArrayList<CustomModel> customBoxProductsModels, int resource, TextView textview, String productId,String pid) {
        this.customBoxProductsModels = customBoxProductsModels;

        this.context = context;
        this.resource = resource;
        total_price_txt = textview;
        this.productId = productId;
        this.pid = pid;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);

        light = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));
    }

    @Override
    public CustomBoxProductsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        CustomBoxProductsHolder slh = new CustomBoxProductsHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final CustomBoxProductsHolder holder, final int position) {

        final ArrayList<String> priceList = new ArrayList<String>(customBoxProductsModels.size());
        final ArrayList<String> qtypriceList = new ArrayList<String>(customBoxProductsModels.size());
        final ArrayList<Integer> qtylist = new ArrayList<Integer>(customBoxProductsModels.size());

        holder.product_name_txt.setTypeface(bold);
        holder.product_name_txt.setText(customBoxProductsModels.get(position).getPdtName());

        Picasso.with(context)
                .load(AppUrls.IMAGE_URL + "70x70/" + customBoxProductsModels.get(position).getImages())
                .placeholder(R.drawable.cart)
                .into(holder.product_img);
        holder.product_img.setOnClickListener(this);
        holder.product_img.setTag(position);

        checkInternet = NetworkChecking.isConnected(context);

        if (checkInternet) {
            subProductList = (ArrayList<Object>) customBoxProductsModels.get(position).getOptions();
            final ArrayList<String> qtyLists = new ArrayList<String>(customBoxProductsModels.size());
            qtyLists.clear();
            priceList.clear();
            qtypriceList.clear();

            for (int i = 0; i < subProductList.size(); i++) {

                Map<String, Object> stringObjectMap = (Map<String, Object>) subProductList.get(i);
                String optId = (String) stringObjectMap.get("optId");
                String pdtId = (String) stringObjectMap.get("pdtId");
                String optName = (String) stringObjectMap.get("optName");
                String price = (String) stringObjectMap.get("price");
                Double dqtyprice = (Double) stringObjectMap.get("qtyPrice");
                Double isinCart = (Double) stringObjectMap.get("isInCart");
                Double dqty = (Double) stringObjectMap.get("cartQty");
                int tqty=dqty.intValue();
                int qty=0;
                if(tqty==0)
                    qty=1;
                else
                    qty=tqty;
                if(dqtyprice==0)
                    qtyprice=price;
                else
                    qtyprice=String.valueOf(dqtyprice.intValue());
                qtylist.add(qty);
                priceList.add(qtyprice);
                qtypriceList.add(qtyprice);
                qtyLists.add(optName + " for Rs." + price);
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(context, R.layout.qty_spinner, qtyLists);
                 Log.v("TTTTTTTTTTTTTTTT",""+qtyLists);
                holder.qty_spinner.setAdapter(spinnerArrayAdapter);
                if (isinCart == 1) {


                   /* Map<String, Object> spinnerObject = (Map<String, Object>) subProductList.get(i);

                    String optName1 = (String) spinnerObject.get("optName");
                    String price1 = (String) spinnerObject.get("price");
                    String selected=optName1 + " for Rs." + price1;
                    final int pos=qtyLists.indexOf(selected);
                    holder.qty_spinner.post(new Runnable() {
                        public void run() {
                            holder.qty_spinner.setSelection(pos,false);
                        }
                    });*/

                   // Log.v("Posss", "///" + qtyLists.indexOf(selected));
                    /*
                    String cpdtId = (String) spinnerObject.get("pdtId");
                    String coptId = (String) spinnerObject.get("optId");
                    Double cdqty = (Double) spinnerObject.get("cartQty");
                    int tmpqty = cdqty.intValue();
                    int cqty = 0;
                    if (tmpqty == 0)
                        cqty = 1;
                    else
                        cqty = tmpqty;
                 *//*   SharedPreferences sharedpreferences = context.getSharedPreferences("Products", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("pdtId", cpdtId);
                    editor.putString("optId", coptId);
                    editor.putInt("qty", cqty);
                    editor.apply();*//*
                    cartstr = cpdtId + "_" + coptId + "_" + cqty;
                    String selectedPrice = (String) spinnerObject.get("price");
                    if (!cartarr.contains(cartstr)) {
                        cartarr.add(cartstr);
                        pricearr.add(Integer.parseInt(selectedPrice));
                    }
*/
                    cartarr=addCartArr();

                    Log.v("CartArr", "///" + cartarr.toString());
                    selchkboxlist.add(customBoxProductsModels.get(i).getId().toString());
                    customBoxProductsModels.get(position).setSelected(true);
                    setTotalPrice();
                }else{
                  //  holder.qty_spinner.setSelection(selectedpos,false);

                }
            }

            String pricestr = qtypriceList.get(0);
            holder.price_txt.setText("Rs." + pricestr);
            holder.qty_txt.setText("" + qtylist.get(0));


           /* ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(context, R.layout.qty_spinner, qtyLists);


            Log.v("TTTTTTTTTTTTTTTT",""+qtyLists);
            holder.qty_spinner.setAdapter(spinnerArrayAdapter);*/

            Map<String, Object> stringObjectMap = (Map<String, Object>) subProductList.get(0);
            String optId = (String) stringObjectMap.get("optId");
            String pdtId = (String) stringObjectMap.get("pdtId");
            //int qty = (int) stringObjectMap.get("cartQty");

            holder.qty_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                    selectedpos = i;

                    String pricestr = priceList.get(i);
                    holder.price_txt.setText("Rs." + pricestr);
                    holder.qty_txt.setText("" + qtylist.get(i));
                    ArrayList<Object> tmparr = (ArrayList<Object>) customBoxProductsModels.get(position).getOptions();


                    if (i > 0) {
                        Map<String, Object> spinnerObject = (Map<String, Object>) tmparr.get(holder.qty_spinner.getSelectedItemPosition());
                        Map<String, Object> tmpObj = new HashMap<>();
                        tmpObj.put("optId", spinnerObject.get("optId"));
                        tmpObj.put("pdtId", spinnerObject.get("pdtId"));
                        tmpObj.put("optName", spinnerObject.get("optName"));
                        tmpObj.put("price", spinnerObject.get("price"));
                        tmpObj.put("qtyPrice", spinnerObject.get("qtyPrice"));
                        tmpObj.put("isInCart",spinnerObject.get("isInCart"));
                        tmpObj.put("cartQty", spinnerObject.get("cartQty"));
                        int itemPos = tmparr.indexOf(tmpObj);
                        tmparr.remove(itemPos);
                        tmparr.add(0, tmpObj);
                        customBoxProductsModels.get(position).setOptions(tmparr);
                        notifyDataSetChanged();

                        if (selchkboxlist.size() > 0) {
                            Map<String, Object> spinnerObject1 = (Map<String, Object>) tmparr.get(0);
                            String pdtIdstr = (String) spinnerObject1.get("pdtId");
                            String optIdstr = (String) spinnerObject1.get("optId");
                            Double drecentqty1 = (Double) spinnerObject1.get("cartQty");
                            int tmdrecentqty1=drecentqty1.intValue();
                            int recentqty1=0;
                            if(tmdrecentqty1==0)
                                recentqty1=1;
                            else
                                recentqty1=tmdrecentqty1;
                           /*SharedPreferences sharedpreferences1 = context.getSharedPreferences("Products", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedpreferences1.edit();
                            editor.putString("pdtId", pdtIdstr);
                            editor.putString("optId", optIdstr);
                            editor.putInt("qty", recentqty1);
                            editor.apply();*/
                            if (holder.add_chk.isChecked()) {

                                //selpricechkboxlist.set(selpricechkboxlist.size()-1,result);
                                /*String pdtId = (String) spinnerObject.get("pdtId");
                                String optId = (String) spinnerObject.get("optId");
                               // int qty1 = (Integer) spinnerObject.get("cartQty");
                                int recentqty = Integer.parseInt(holder.qty_txt.getText().toString());
                                SharedPreferences sharedpreferences = context.getSharedPreferences("Products", Context.MODE_PRIVATE);
                                String oldpdtid = sharedpreferences.getString("pdtId", "");
                                String oldoptid = sharedpreferences.getString("optId", "");
                                Integer oldqty = sharedpreferences.getInt("qty", 0);
                                cartstr = pdtId + "_" + optId + "_" + recentqty;
                                String tmpstr = oldpdtid + "_" + oldoptid + "_" + oldqty;
                                int cartPos = cartarr.indexOf(tmpstr);
                              //  if(cartPos>=0) {
                                    cartarr.remove(cartPos);
                                    cartarr.add(cartPos, cartstr);

                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putString("pdtId", pdtIdstr);
                                editor.putString("optId", optIdstr);
                                editor.putInt("qty", recentqty1);
                                editor.apply();*/
                                cartarr=addCartArr();
                                //}
                                /*else {
                                    cartarr.add(0, cartstr);
                                }*/
                                Log.v("CartArr", "))))))" + cartarr.toString());
                                setTotalPrice();

                            }
                        }

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
        if (productId != null && productId.length() > 0) {
            if (isfirsttime) {
                int itemPos = 0;
                for (int i = 0; i < customBoxProductsModels.size(); i++) {
                    if (productId.equals(customBoxProductsModels.get(i).getId())) {
                        itemPos = i;
                    }
                }
                ArrayList<Object> tmparr = (ArrayList<Object>) customBoxProductsModels.get(itemPos).getOptions();
                Map<String, Object> spinnerObject = (Map<String, Object>) tmparr.get(0);
                String pdtId = (String) spinnerObject.get("pdtId");
                String optId = (String) spinnerObject.get("optId");
                Double dqty = (Double) spinnerObject.get("cartQty");
                int qty=0;
                if(dqty==0)
                    qty=1;
                else
                    qty=dqty.intValue();
                cartstr = pdtId + "_" + optId + "_" + qty;
                cartarr.add(cartstr);
                Log.v("CartArr", "%%%%" + cartarr.toString());
                selchkboxlist.add(customBoxProductsModels.get(itemPos).getId().toString());
                String selectedPrice = (String) spinnerObject.get("price");
                String totalprice = total_price_txt.getText().toString();
                String[] split = totalprice.split(":");
                total_price_txt.setText("  Total Price : " + (Integer.parseInt(split[1].trim()) + Integer.parseInt(selectedPrice.trim())));
                Log.v("DDDDDDDDDDD","&&&&"+(Integer.parseInt(split[1].trim()) + Integer.parseInt(selectedPrice.trim())));
                // Toast.makeText(context, selchkboxlist.toString().replace("[","").replace("]",""), Toast.LENGTH_SHORT).show();
                Log.d("TOTALPRICES", selectedPrice);
                customBoxProductsModels.get(itemPos).setSelected(true);
                isfirsttime = false;
            }
        }

        holder.add_chk.setOnCheckedChangeListener(null);
        holder.add_chk.setChecked(customBoxProductsModels.get(position).isSelected());
        holder.add_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int qty = Integer.parseInt(holder.qty_txt.getText().toString());

                if(qty==8)
                {
                    Toast.makeText(context,"Sorry,You Can't add more qunatity!!!",Toast.LENGTH_LONG).show();
                }
                else
                {
                    if (qty != 0)
                    {
                        ArrayList<Object> tmparr = (ArrayList<Object>) customBoxProductsModels.get(position).getOptions();
                        Map<String, Object> spinnerObject = (Map<String, Object>) tmparr.get(0);
                        //if (qty == 1)
                        price = qtypriceList.get(0);
                        qty++;

                        String selectedprice = (String) spinnerObject.get("price");
                        holder.qty_txt.setText(String.valueOf(qty));
                        double finalPrice = qty * Double.parseDouble(selectedprice);
                        String result = new DecimalFormat("##.##").format(finalPrice);
                        holder.price_txt.setText("Rs." + result);
                        Map<String, Object> tmpObj = new HashMap<>();
                        tmpObj.put("optId", spinnerObject.get("optId"));
                        tmpObj.put("pdtId", spinnerObject.get("pdtId"));
                        tmpObj.put("optName", spinnerObject.get("optName"));
                        tmpObj.put("price", spinnerObject.get("price"));
                        tmpObj.put("qtyPrice", new Double(result));
                        tmpObj.put("cartQty", new Double(qty));
                        tmpObj.put("isInCart",spinnerObject.get("isInCart"));
                        tmparr.set(0, tmpObj);
                        customBoxProductsModels.get(position).setOptions(tmparr);
                        notifyDataSetChanged();
                        if (selchkboxlist.size() > 0) {
                            if (holder.add_chk.isChecked()) {
                            /*String pdtId = (String) spinnerObject.get("pdtId");
                            String optId = (String) spinnerObject.get("optId");
                            Double dqty1 = (Double) spinnerObject.get("cartQty");
                            int tqty=dqty1.intValue();
                            int qty1=0;
                            if(tqty==0)
                                qty1=1;
                            else
                                qty1=tqty;
                            int recentqty = Integer.parseInt(holder.qty_txt.getText().toString());
                            SharedPreferences sharedpreferences = context.getSharedPreferences("Products", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedpreferences.edit();
                            editor.putString("pdtId", pdtId);
                            editor.putString("optId", optId);
                            editor.putInt("qty", recentqty);
                            editor.apply();
                            cartstr = pdtId + "_" + optId + "_" + recentqty;
                            String tmpstr = pdtId + "_" + optId + "_" + qty1;
                            int itemPos = cartarr.indexOf(tmpstr);
                            if(itemPos>=0) {
                                cartarr.remove(itemPos);
                                cartarr.add(itemPos, cartstr);
                            }*//*else {
                                cartarr.add(0, cartstr);
                            }*/
                                cartarr=addCartArr();
                                setTotalPrice();

                                //cartarr.set(position,cartstr);
                                Log.v("CartArr", "######" + cartarr.toString());

                            }
                        }

                    } else {
                        Toast.makeText(context, "Minimum 1 Quantity Required...!", Toast.LENGTH_SHORT).show();
                    }
                }


            }
        });

        holder.minus_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int qty = Integer.parseInt(holder.qty_txt.getText().toString());
                if (qty > 1) {

                    qty--;
                    if (qty == 1)
                        price = priceList.get(0);
                    ArrayList<Object> tmparr = (ArrayList<Object>) customBoxProductsModels.get(position).getOptions();
                    Map<String, Object> spinnerObject = (Map<String, Object>) tmparr.get(0);
                    String selectedprice = (String) spinnerObject.get("price");
                    holder.qty_txt.setText(String.valueOf(qty));
                    double finalPrice = qty * Double.parseDouble(selectedprice);
                    String result = new DecimalFormat("##.##").format(finalPrice);
                    holder.price_txt.setText("Rs." + result);
                    Map<String, Object> tmpObj = new HashMap<>();
                    tmpObj.put("optId", spinnerObject.get("optId"));
                    tmpObj.put("pdtId", spinnerObject.get("pdtId"));
                    tmpObj.put("optName", spinnerObject.get("optName"));
                    tmpObj.put("price",  spinnerObject.get("price"));
                    tmpObj.put("qtyPrice",new Double(result));
                    tmpObj.put("cartQty", new Double(qty));
                    tmpObj.put("isInCart",spinnerObject.get("isInCart"));
                    tmparr.set(0, tmpObj);
                    customBoxProductsModels.get(position).setOptions(tmparr);
                    notifyDataSetChanged();
                    if (selchkboxlist.size() > 0) {
                        try {
                            if (holder.add_chk.isChecked()) {
                                /*String pdtId = (String) spinnerObject.get("pdtId");
                                String optId = (String) spinnerObject.get("optId");
                                Double dqty1 = (Double) spinnerObject.get("cartQty");
                                int tdqty1=dqty1.intValue();
                                int qty1=0;
                                if(tdqty1==0)
                                    qty1=1;
                                else
                                    qty1=tdqty1;
                                int recentqty = Integer.parseInt(holder.qty_txt.getText().toString());
                                SharedPreferences sharedpreferences = context.getSharedPreferences("Products", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putString("pdtId", pdtId);
                                editor.putString("optId", optId);
                                editor.putInt("qty", recentqty);
                                editor.apply();
                                cartstr = pdtId + "_" + optId + "_" + recentqty;
                                String tmpstr = pdtId + "_" + optId + "_" + qty1;
                                int itemPos = cartarr.indexOf(tmpstr);
                                if(itemPos>=0) {
                                    cartarr.remove(itemPos);
                                    cartarr.add(itemPos, cartstr);
                                }*//*else {
                                    cartarr.add(itemPos, cartstr);
                                }*/
                                cartarr=addCartArr();
                                setTotalPrice();
                             Log.v("CartArr", "$$$$" + cartarr.toString());

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } else {
                    Toast.makeText(context, "Minimum 1 Quantity Required...!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        holder.add_chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                int pos = (int) compoundButton.getTag();
                customBoxProductsModels.get(holder.getAdapterPosition()).setSelected(isChecked);
                if (isChecked) {
                    /*ArrayList<Object> tmparr = (ArrayList<Object>) customBoxProductsModels.get(position).getOptions();
                    Map<String, Object> spinnerObject = (Map<String, Object>) tmparr.get(0);
                    String pdtId = (String) spinnerObject.get("pdtId");
                    String optId = (String) spinnerObject.get("optId");
                    Double dqty = (Double) spinnerObject.get("cartQty");
                    int tqty=dqty.intValue();
                    int qty=0;
                    if(tqty==0)
                        qty=1;
                    else
                        qty=tqty;
                    cartstr = pdtId + "_" + optId + "_" + qty;
                    SharedPreferences sharedpreferences = context.getSharedPreferences("Products", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("pdtId", pdtId);
                    editor.putString("optId", optId);
                    editor.putInt("qty", qty);
                    editor.apply();
                    cartarr.add(cartstr);*/
                    cartarr=addCartArr();
                    Log.v("CartArr", "@@@@" + cartarr.toString());
                    selchkboxlist.add(customBoxProductsModels.get(pos).getId().toString());
                    setTotalPrice();


                } else {
                    selchkboxlist.remove(customBoxProductsModels.get(pos).getId().toString());
                    cartarr=addCartArr();

                    /*ArrayList<Object> tmparr = (ArrayList<Object>) customBoxProductsModels.get(position).getOptions();
                    Map<String, Object> spinnerObject = (Map<String, Object>) tmparr.get(0);
                    String pdtId = (String) spinnerObject.get("pdtId");
                    String optId = (String) spinnerObject.get("optId");
                    Double dqty = (Double) spinnerObject.get("cartQty");
                    int tqty=dqty.intValue();
                    int qty=0;
                    if(tqty==0)
                        qty=1;
                    else
                        qty=tqty;
                    cartstr = pdtId + "_" + optId + "_" + qty;
                    cartarr.remove(cartstr);*/
                    Log.v("CartArr", "****" + cartarr.toString());
                    setTotalPrice();

                }
            }
        });

        holder.setItemClickListener(new CustomBoxProductsItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });

        holder.product_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent it = new Intent(context, FormarActivity.class);
                it.putExtra("IDD", customBoxProductsModels.get(position).getId());
                it.putExtra("Name", customBoxProductsModels.get(position).getPdtName());
                it.putExtra("Image", customBoxProductsModels.get(position).getImages());
                it.putExtra("FROM","FROMBROWSE");
                context.startActivity(it);
            }
        });

        holder.add_chk.setTag(position);
    }

    @Override
    public int getItemCount() {

        return this.customBoxProductsModels.size();
    }

    private void chkSelected() {

    }

    @Override
    public void onClick(View view) {
        int pos = (int) view.getTag();
        Map<String, Object> tmpObj = new HashMap<>();
        tmpObj.put("pdtId", customBoxProductsModels.get(pos).getId());
        tmpObj.put("childPdtName", customBoxProductsModels.get(pos).getPdtName());
        tmpObj.put("images", customBoxProductsModels.get(pos).getImages());
        tmpObj.put("pid", pid);
        Intent it = new Intent(context, FormarActivity.class);
        it.putExtra("productobj", new HashMap<>(tmpObj));
        context.startActivity(it);
    }

    private int sum(ArrayList<Integer> arr ){
        int sum =0;
        for(int i : arr) {
            sum += i;
        }
        return sum;
    }


    private  void setTotalPrice(){
        selcetedpricearr.clear();
        for(int i=0;i<customBoxProductsModels.size();i++){
            if(customBoxProductsModels.get(i).isSelected()){
                ArrayList<Object> tmparr1 = (ArrayList<Object>) customBoxProductsModels.get(i).getOptions();
                Map<String, Object> spinnerObject1 = (Map<String, Object>) tmparr1.get(0);
                Double selectedqPrice = (Double) spinnerObject1.get("qtyPrice");
                String  selectedPrice = (String) spinnerObject1.get("price");
                int price=0;
                if(selectedqPrice==0)
                    price=Double.valueOf(selectedPrice).intValue();
                else
                    price=selectedqPrice.intValue();
                selcetedpricearr.add(price);
            }

        }
        int toatal=sum(selcetedpricearr);
        total_price_txt.setText("  Total Price : " +toatal);
    }

    private  ArrayList<String> addCartArr(){
     ArrayList<String> cartarr=new ArrayList<>();
        for(int i=0;i<customBoxProductsModels.size();i++){
            if(customBoxProductsModels.get(i).isSelected()){
                ArrayList<Object> tmparr1 = (ArrayList<Object>) customBoxProductsModels.get(i).getOptions();
                Map<String, Object> spinnerObject1 = (Map<String, Object>) tmparr1.get(0);
                String pdtId = (String) spinnerObject1.get("pdtId");
                String optId = (String) spinnerObject1.get("optId");
                Double dqty = (Double) spinnerObject1.get("cartQty");
                int tqty=dqty.intValue();
                int qty=0;
                if(tqty==0)
                    qty=1;
                else
                    qty=tqty;
                cartstr = pdtId + "_" + optId + "_" + qty;
                cartarr.add(cartstr);
            }

        }
        return cartarr;

    }
}
