package in.innasoft.gofarmz.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.CustomBoxProductsActivity;
import in.innasoft.gofarmz.activities.MyCartActivity;
import in.innasoft.gofarmz.activities.ViewDetails;
import in.innasoft.gofarmz.holders.MyCartHolder;
import in.innasoft.gofarmz.itemClickListerners.MyCartItemClickListener;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class MyCartAdapter extends RecyclerView.Adapter<MyCartHolder> {

    private ArrayList<Object> cartList;
    private ArrayList<Object> carChldtList = new ArrayList<>();
    private MyCartActivity context;
    private LayoutInflater li;
    private int resource;
    private Typeface light, regular, bold;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token, price;
    Dialog dialog;
    String from,pid="",TYPE;
    SweetAlertDialog sd;

    public MyCartAdapter(MyCartActivity context, ArrayList<Object> cartList, int resource, String from,String pid) {
        this.cartList = cartList;
        this.context = context;
        this.resource = resource;
        this.from = from;
        this.pid = pid;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);

        light = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));
    }

    @Override
    public MyCartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        MyCartHolder slh = new MyCartHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final MyCartHolder holder, final int position) {



        final Map<String, Object> productObject = (Map<String, Object>) cartList.get(position);

       // Log.d("JKJKJK",productObject.get("id").toString());
        holder.product_name.setTypeface(bold);
        holder.product_name.setText(productObject.get("product_name").toString());
        holder.qty_btn.setVisibility(View.GONE);

        TYPE=productObject.get("type").toString();
        TYPE = TYPE.replaceAll("_", " ").toLowerCase();
        holder.product_type.setText(TYPE);

       /* if(TYPE.equalsIgnoreCase("COMBO_CUSTOM"))
        {
            holder.product_type.setText("COMBO CUSTOM");
            holder.product_type.setTypeface(bold);
        }
        else
        {
            holder.product_type.setText("COMBO");
            holder.product_type.setTypeface(bold);
        }*/



        //holder.price_txt.setTypeface(bold);
        String price=productObject.get("total_price").toString();
        Double d=Double.parseDouble(price);
        holder.price_txt.setText(""+Math.round(d));

       // holder.qty_btn.setTypeface(bold);
        holder.cart_qty_txt.setText(productObject.get("purchase_quantity").toString());

        Picasso.with(context)
                .load(AppUrls.IMAGE_URL + "70x70/" + productObject.get("images").toString())
                .placeholder(R.drawable.cart)
                .into(holder.product_img);

        holder.delete_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet)
                {
                    sd = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);

                    sd.setTitleText("Are you sure?");
                    sd.setContentText("This product will be delete from cart!");
                    sd.setCancelText("No,cancel pls!");
                    sd.setConfirmText("Yes,delete it!");
                    sd.showCancelButton(true);

                    sd.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(final SweetAlertDialog sweetAlertDialog)
                        {
                            Map<String, Object> productPos = (Map<String, Object>) cartList.get(position);
                            final String id = (String) productPos.get("id");
                            Log.d("ProductListData", id);

                            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.DELETECARTDATA,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {

                                            try {
                                                JSONObject jsonObject = new JSONObject(response);
                                                String status = jsonObject.getString("status");
                                                if (status.equals("10100")) {
                                                    sweetAlertDialog.cancel();
                                                    cartList.remove(position);
                                                    String message = jsonObject.getString("message");
                                                    if (cartList.size() == 0) {
                                                        if (from.equalsIgnoreCase("Home")) {
                                                            Intent intent = new Intent(context, MainActivity.class);
                                                            context.startActivity(intent);
                                                        } else {
                                                            Intent intent = new Intent(context, CustomBoxProductsActivity.class);
                                                            intent.putExtra("pid",pid);
                                                            intent.putExtra("from","main");
                                                            context.startActivity(intent);
                                                        }
                                                    }

                                                    context.refreshCartPage();
                                                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                                }
                                                if (status.equals("10200")) {

                                                    Toast.makeText(context, "Invalid Input..", Toast.LENGTH_SHORT).show();
                                                }

                                            } catch (JSONException e) {
                                                e.printStackTrace();

                                            }
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {


                                            error.getMessage();
                                        }
                                    }) {
                                @Override
                                protected Map<String, String> getParams() {
                                    Map<String, String> params = new HashMap<String, String>();
                                    params.put("user_id", user_id);
                                    params.put("cart_id", id);
                                    params.put("browser_id", id);
                                    return params;
                                }

                                @Override
                                public Map<String, String> getHeaders() {
                                    Map<String, String> headers = new HashMap<>();
                                    headers.put("X-Platform", "ANDROID");
                                    headers.put("X-Deviceid", deviceId);
                                    return headers;
                                }

                            };
                            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            RequestQueue requestQueue = Volley.newRequestQueue(context);
                            requestQueue.add(stringRequest);
                        }
                    });


                    sd.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.cancel();
                        }
                    });
                    sd.show();
                } else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });


        holder.view_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
                    String type = (String) productObject.get("type");
                    String name=productObject.get("product_name").toString();
                    if (type.equalsIgnoreCase("COMBO_CUSTOM"))
                        carChldtList = (ArrayList<Object>) productObject.get("ADDONS");
                    else
                        carChldtList = (ArrayList<Object>) productObject.get("ESSENTIAL");
                    Intent it=new Intent(context, ViewDetails.class);
                  //  it.putExtra("productlist",carChldtList);
                    it.putExtra("TYPE",type);
                    it.putExtra("IDD",(String) productObject.get("id"));
                    context.startActivity(it);

                }else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
       holder.cart_add_img.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               String qtystr=holder.cart_qty_txt.getText().toString();
               if(qtystr.equals("8"))
               {
                   Toast.makeText(context,"Sorry,You Can't add more qunatity!!!",Toast.LENGTH_LONG).show();
               }
               else
               {

                   int qty=Integer.parseInt(qtystr);
                   final Map<String, Object> productObject = (Map<String, Object>) cartList.get(position);
                   final String id = (String) productObject.get("id");
                   if(qty!=0)
                       qty++;
                   increaseQuatity(id,qty);
               }


           }
       });
        holder.cart_minus_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Map<String, Object> productObject = (Map<String, Object>) cartList.get(position);
                final String id = (String) productObject.get("id");
                String qtystr=holder.cart_qty_txt.getText().toString();
                int qty = Integer.parseInt(qtystr);
                if (qty > 1)
                    qty--;
                increaseQuatity(id,qty);
            }
        });

        holder.setItemClickListener(new MyCartItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {


            }
        });
    }

    private void increaseQuatity(final String id, final int qty){
        boolean checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {
            Log.d("IncreaseQty", AppUrls.BASE_URL + AppUrls.INCRESE_QTY);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.INCRESE_QTY,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("IncQtyRES", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                if (status.equals("10100")) {
                                  context.getCartData();
                                }
                                if (status.equals("10200")) {
                                    //Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("cart_id", id);
                    params.put("purchase_quantity", String.valueOf(qty));
                    Log.d("REPORT_PARAM:", "PARMS" + params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("X-Platform", "ANDROID");
                    headers.put("X-Deviceid", deviceId);
                    headers.put("Authorization-Basic", token);
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public int getItemCount() {

        return this.cartList.size();
    }
}
