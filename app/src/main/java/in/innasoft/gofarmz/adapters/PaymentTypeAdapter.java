package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.PaymentDetails;
import in.innasoft.gofarmz.itemClickListerners.MyCartItemClickListener;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.NetworkChecking;

public class PaymentTypeAdapter extends RecyclerView.Adapter<PaymentTypeAdapter.PayTypeHolder> {

    private ArrayList<Object> orderHistDetailList;
    private PaymentDetails context;
    private LayoutInflater li;
    private int resource;
    private Typeface light, regular, bold;
    private RadioButton lastCheckedRB = null;
    public String send_check_Value, pay_id;
    private boolean checkInternet;

    public PaymentTypeAdapter(PaymentDetails context, ArrayList<Object> orderHistDetailList, int resource) {

        this.orderHistDetailList = orderHistDetailList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        light = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));
    }

    @Override
    public PayTypeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        PayTypeHolder slh = new PayTypeHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final PayTypeHolder holder, final int position) {

        final Map<String, Object> orderListProductObject = (Map<String, Object>) orderHistDetailList.get(position);


        Picasso.with(context)
                .load(AppUrls.BASE_URL + orderListProductObject.get("logo").toString())
                .placeholder(R.drawable.cart)
                .into(holder.pay_logo);

        String str_reason = orderListProductObject.get("name").toString();
        String converted_string = str_reason.substring(0, 1).toUpperCase() + str_reason.substring(1);
        holder.pay_radio_dynamic_button.setTypeface(light);
        holder.pay_radio_dynamic_button.setText(converted_string);

       if (position == 0) {
            if (lastCheckedRB == null) {
                pay_id = orderListProductObject.get("id").toString();
                holder.pay_radio_dynamic_button.setChecked(true);
                lastCheckedRB = holder.pay_radio_dynamic_button;
                 Log.d("eeee", "" + pay_id);
            }
        }

        View.OnClickListener rbClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
                    RadioButton checked_rb = (RadioButton) v;
                    int pos = (int) v.getTag();
                    final Map<String, Object> orderListProductObject = (Map<String, Object>) orderHistDetailList.get(pos);
                    if (lastCheckedRB != null) {

                        lastCheckedRB.setChecked(false);
                        pay_id = orderListProductObject.get("id").toString();
                        Log.d("zzzzzz", "" + pay_id);
                        send_check_Value = holder.pay_radio_dynamic_button.getText().toString();
                    }
                    lastCheckedRB = checked_rb;
                    pay_id = orderListProductObject.get("id").toString();
                    send_check_Value = holder.pay_radio_dynamic_button.getText().toString();
                }else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        };

        holder.pay_radio_dynamic_button.setOnClickListener(rbClick);
        holder.pay_radio_dynamic_button.setTag(position);

        holder.setItemClickListener(new MyCartItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });

    }

    @Override
    public int getItemCount() {

        return this.orderHistDetailList.size();
    }

    public class PayTypeHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public RadioButton pay_radio_dynamic_button;
        public ImageView pay_logo;
        MyCartItemClickListener citiesItemClickListener;

        public PayTypeHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            pay_radio_dynamic_button = itemView.findViewById(R.id.pay_radio_dynamic_button);
            pay_logo = itemView.findViewById(R.id.pay_logo);

        }

        @Override
        public void onClick(View view) {

            this.citiesItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(MyCartItemClickListener ic) {
            this.citiesItemClickListener = ic;
        }
    }
}
