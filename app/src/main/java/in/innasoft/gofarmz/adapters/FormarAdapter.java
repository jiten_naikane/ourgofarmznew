package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.itemClickListerners.MyCartItemClickListener;
import in.innasoft.gofarmz.models.FarmerSupplierModel;
import in.innasoft.gofarmz.utils.AppUrls;

public class FormarAdapter extends RecyclerView.Adapter<FormarAdapter.FormarNameHolder> {
    Context context;
    ArrayList<FarmerSupplierModel> detailList = new ArrayList<>();
    private LayoutInflater li;
    private int resource;
    private Typeface light;
    String  Type;

    public FormarAdapter(Context context, ArrayList<FarmerSupplierModel> detailList, int resource) {
        this.context = context;
        this.detailList = detailList;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        light = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.light));

    }

    @Override
    public FormarAdapter.FormarNameHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View layout = li.inflate(resource, viewGroup, false);
        FormarNameHolder slh = new FormarNameHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(@NonNull FormarAdapter.FormarNameHolder holder, int i) {


        Type=detailList.get(i).type;


        holder.farmer_name_text.setText(detailList.get(i).farmer_name);
        holder.farm_name_text.setText(Html.fromHtml(detailList.get(i).farm_name));


        if(Type.equalsIgnoreCase("Supplier"))
        {
            holder.farmername_titletext.setText("Supplier Name : ");
            holder.farm_name_title_text.setText("Store Name : ");
        }
        else
        {

        }


        holder.type_text.setText(Html.fromHtml(detailList.get(i).type));

        Picasso.with(context)
                .load(detailList.get(i).farm_photo)
                .placeholder(R.drawable.farmer_dummy)
                .into(holder.farmer_image);

        holder.farmername_titletext.setTypeface(light);
        holder.farm_name_title_text.setTypeface(light);
        holder.type_text.setTypeface(light);
        holder.farmer_name_text.setTypeface(light);
        holder.farm_name_text.setTypeface(light);
    }

    @Override
    public int getItemCount() {
        return detailList.size();
    }

    public class FormarNameHolder extends RecyclerView.ViewHolder {

        public TextView farmername_titletext,farmer_name_text,farm_name_title_text,farm_name_text,type_text;

        public ImageView farmer_image;

        MyCartItemClickListener citiesItemClickListener;

        public FormarNameHolder(View itemView) {
            super(itemView);

            farmername_titletext = itemView.findViewById(R.id.farmername_titletext);
            farmer_name_text = itemView.findViewById(R.id.farmer_name_text);
            farm_name_title_text = itemView.findViewById(R.id.farm_name_title_text);
            farm_name_text = itemView.findViewById(R.id.farm_name_text);

            type_text = itemView.findViewById(R.id.type_text);

            farmer_image = itemView.findViewById(R.id.farmer_image);

        }
    }
}
