package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;

import in.innasoft.gofarmz.R;

public class SlotListAdapter extends BaseAdapter {
    Context context;
    ArrayList<Object> slotarr;

    public SlotListAdapter(Context context, ArrayList<Object> info) {
        this.context = context;
        slotarr = info;

    }

    @Override
    public int getCount() {
        return slotarr.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null)
        {
            holder = new ViewHolder();
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.row_slot_list, null);

            holder.time_slot_text = convertView.findViewById(R.id.time_slot_text);
            convertView.setTag(holder);
        }
        else
            {
            holder = (ViewHolder) convertView.getTag();
        }

        final Map<String, Object> tmpObj = (Map<String, Object>) slotarr.get(position);

        Log.d("KLLLK", ((Map<String, Object>) slotarr.get(position)).get("start_time").toString());
        holder.time_slot_text.setText((String) tmpObj.get("start_time") +" -- "+(String) tmpObj.get("end_time"));



        return convertView;
    }

    public static class ViewHolder {
        public TextView  time_slot_text;
    }
}
