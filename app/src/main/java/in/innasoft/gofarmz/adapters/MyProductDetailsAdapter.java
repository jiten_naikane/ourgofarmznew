package in.innasoft.gofarmz.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.PaymentDetails;
import in.innasoft.gofarmz.activities.ViewDetails;
import in.innasoft.gofarmz.holders.MyCartHolder;
import in.innasoft.gofarmz.itemClickListerners.MyCartItemClickListener;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class MyProductDetailsAdapter extends RecyclerView.Adapter<MyCartHolder> {

    private ArrayList<Object> cartList;
    private ArrayList<Object> carChldtList = new ArrayList<>();
    private PaymentDetails context;
    private LayoutInflater li;
    private int resource;
    private Typeface light, regular, bold;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token, price,TYPE;
    Dialog dialog;

    public MyProductDetailsAdapter(PaymentDetails context, ArrayList<Object> cartList, int resource) {
        this.cartList = cartList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);

        light = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));
    }

    @Override
    public MyCartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        MyCartHolder slh = new MyCartHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final MyCartHolder holder, final int position) {

        final Map<String, Object> productObject = (Map<String, Object>) cartList.get(position);

        holder.delete_img.setVisibility(View.GONE);
        holder.cart_qty_ll.setVisibility(View.GONE);

        holder.view_txt.setTypeface(light);

        holder.product_name.setTypeface(bold);
        holder.product_name.setText(productObject.get("product_name").toString());

        TYPE=productObject.get("type").toString();
        TYPE = TYPE.replaceAll("_", " ").toLowerCase();
        holder.product_type.setText(TYPE);
        holder.product_type.setTypeface(bold);


        String price=productObject.get("total_price").toString();
        Double d=Double.parseDouble(price);
        holder.price_txt.setText(""+Math.round(d));
        //holder.price_txt.setTypeface(bold);

        //holder.qty_btn.setTypeface(bold);
        holder.qty_btn.setText("Qty : " + productObject.get("purchase_quantity").toString());

        Picasso.with(context)
                .load(AppUrls.IMAGE_URL + "70x70/" + productObject.get("images").toString())
                .placeholder(R.drawable.cart)
                .into(holder.product_img);

        holder.delete_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("Delete");
                    builder.setMessage("Are you sure?");
                    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialog, int which) {
                            Map<String, Object> productPos = (Map<String, Object>) cartList.get(position);
                            final String id = (String) productPos.get("id");
                            Log.d("ProductListData", id);

                            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.DELETECARTDATA,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {

                                            try {
                                                JSONObject jsonObject = new JSONObject(response);
                                                String status = jsonObject.getString("status");
                                                if (status.equals("10100")) {

                                                    String message = jsonObject.getString("message");
                                                    context.refreshCartPage();
                                                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                                }
                                                if (status.equals("10200")) {

                                                    Toast.makeText(context, "Invalid Input..", Toast.LENGTH_SHORT).show();
                                                }

                                            } catch (JSONException e) {
                                                e.printStackTrace();

                                            }
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {


                                            error.getMessage();
                                        }
                                    }) {
                                @Override
                                protected Map<String, String> getParams() {
                                    Map<String, String> params = new HashMap<String, String>();
                                    params.put("user_id", user_id);
                                    params.put("cart_id", id);
                                    params.put("browser_id", id);
                                    return params;
                                }

                                @Override
                                public Map<String, String> getHeaders() {
                                    Map<String, String> headers = new HashMap<>();
                                    headers.put("X-Platform", "ANDROID");
                                    headers.put("X-Deviceid", deviceId);
                                    return headers;
                                }

                            };
                            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            RequestQueue requestQueue = Volley.newRequestQueue(context);
                            requestQueue.add(stringRequest);

                        }
                    });

                    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                        }
                    });

                    AlertDialog alert = builder.create();
                    alert.show();
                } else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });


        holder.view_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
                    String type = (String) productObject.get("type");
                    String name=productObject.get("product_name").toString();
                    if (type.equalsIgnoreCase("COMBO_CUSTOM"))
                        carChldtList = (ArrayList<Object>) productObject.get("ADDONS");
                    else
                        carChldtList = (ArrayList<Object>) productObject.get("ESSENTIAL");
                    Intent it=new Intent(context, ViewDetails.class);
                    it.putExtra("productlist",carChldtList);
                    it.putExtra("TYPE",type);
                    context.startActivity(it);

                }else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });


        holder.setItemClickListener(new MyCartItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {


            }
        });
    }

    @Override
    public int getItemCount() {

        return this.cartList.size();
    }
}
