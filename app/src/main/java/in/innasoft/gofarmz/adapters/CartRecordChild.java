package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.ViewDetails;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class CartRecordChild extends BaseAdapter {

    private ViewDetails context;
    private ArrayList<Object> cartRecordChild = new ArrayList<>();
    private Typeface light, regular, bold;
    String type_get,IDD, deviceId, user_id, token;
    UserSessionManager userSessionManager;




    public CartRecordChild(ViewDetails ctx, ArrayList<Object> tmp,String type) {
        context = ctx;
        cartRecordChild = tmp;
        type_get=type;
        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);
        light = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));
    }

    @Override
    public int getCount() {
        return cartRecordChild.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.row_cart_child_record_list, null, true);

        final Map<String, Object> tmp = (Map<String, Object>) cartRecordChild.get(position);

      /*  IDD=(String) tmp.get("cart_product_addon_id");
        Log.d("IDDDDDCHILD",IDD);*/

        ImageView child_img = view.findViewById(R.id.child_img);
        Picasso.with(context)
                .load(AppUrls.IMAGE_URL + "70x70/" + (String) tmp.get("images"))
                .placeholder(R.drawable.cart)
                .into(child_img);

        LinearLayout cart_qty_ll = view.findViewById(R.id.cart_qty_ll);
       final  TextView qty_txt = view.findViewById(R.id.qty_txt);
        ImageView cart_minus_img = view.findViewById(R.id.cart_minus_img);
        ImageView cart_add_img = view.findViewById(R.id.cart_add_img);
        ImageView delete_img = view.findViewById(R.id.delete_img);


        TextView child_name_txt = view.findViewById(R.id.child_name_txt);
        child_name_txt.setTypeface(light);
        child_name_txt.setText((String) tmp.get("pdtName"));

         TextView child_price_txt = view.findViewById(R.id.child_price_txt);
        child_price_txt.setText((String) tmp.get("price") + "/-");

        TextView child_qty_txt = view.findViewById(R.id.child_qty_txt);
        //child_qty_txt.setTypeface(light);
        child_qty_txt.setText((String) tmp.get("unitValue") + (String) tmp.get("unitName"));

//          Log.d("sTYTPEPT",type_get);

        if(type_get.equalsIgnoreCase("COMBO"))
        {
      //nothing
        }
        else
        {

            cart_qty_ll.setVisibility(View.VISIBLE);
            delete_img.setVisibility(View.VISIBLE);

            qty_txt.setTypeface(light);
            qty_txt.setText((String) tmp.get("quantity"));

            cart_add_img.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    String qtystr=qty_txt.getText().toString();
                    int qty=Integer.parseInt(qtystr);
                    final Map<String, Object> productObject = (Map<String, Object>) cartRecordChild.get(position);
                     String id = (String) productObject.get("cart_product_addon_id");
                    if(qty!=0)
                        qty++;
                   // qty_txt.setText(String.valueOf(qty));

                  Log.d("ADDDETAIL",id+"///"+qty);
                    increaseQuatity(id,qty);




                }
            });


            cart_minus_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String qtystr=qty_txt.getText().toString();
                    int qty = Integer.parseInt(qtystr);
                      final Map<String, Object> productObject = (Map<String, Object>) cartRecordChild.get(position);
                     String id = (String) tmp.get("cart_product_addon_id");

                    if (qty > 1)
                        qty--;

                   Log.d("DETAILMINUS",id+"///"+qty);
                      increaseQuatity(id,qty);
                  //  qty_txt.setText(String.valueOf(qty));

                }
            });


            delete_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    String delete_id = (String) tmp.get("cart_product_addon_id");
                   // deleteItem(delete_id);
                    boolean checkInternet = NetworkChecking.isConnected(context);
                    if (checkInternet) {
                        Log.d("deleteURL", AppUrls.BASE_URL + AppUrls.DELETE_CHILD_CART+"?cart_product_addon_id="+delete_id);
                        StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.DELETE_CHILD_CART+"?cart_product_addon_id="+delete_id ,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        Log.d("RESdelete", response);
                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            String status = jsonObject.getString("status");
                                            if (status.equals("10100"))
                                            {
                                                cartRecordChild.remove(position);
                                                Toast.makeText(context, "Item Deleted..!", Toast.LENGTH_SHORT).show();
                                                context. getViewDetailChild();

                                                // String price = jsonObject.getString("price");
                                                // Log.d("price---",price);
                                                //  child_price_txt.setText((String) price + "/-");

                                            }
                                            if (status.equals("10200")) {
                                                //Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();

                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        error.getMessage();
                                    }
                                }) {
                            @Override
                            public Map<String, String> getHeaders() {
                                Map<String, String> headers = new HashMap<>();
                                headers.put("X-Platform", "ANDROID");
                                headers.put("X-Deviceid", deviceId);
                                headers.put("Authorization-Basic", token);
                                return headers;
                            }
                        };
                        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        RequestQueue requestQueue = Volley.newRequestQueue(context);
                        requestQueue.add(stringRequest);
                    } else {
                        Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }

        return view;
    }

    private void increaseQuatity(final String id, final int qty)
    {
        boolean checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {
            Log.d("ChlildIncreaseQty", AppUrls.BASE_URL + AppUrls.VIEW_CHILD_CART_INCRESE_QTY );
            StringRequest stringRequest = new StringRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.VIEW_CHILD_CART_INCRESE_QTY ,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("RESIncQtyRES", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                if (status.equals("10100"))
                                {
                                    String price = jsonObject.getString("price");
                                    context.getViewDetailChild();

                                   // child_price_txt.setText((String) price + "/-");

                                  //  context.setPrice(price);
                                   // Log.d("price---",price);
                                  // child_price_txt.setText((String) price + "/-");

                                }
                                if (status.equals("10200")) {
                                    //Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                           error.getMessage();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams()  {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("cart_product_addon_id", id);
                    params.put("purchase_quantity", String.valueOf(qty));
                    Log.d("REPORT_PARAM", "PARMS" + params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("X-Platform", "ANDROID");
                    headers.put("X-Deviceid", deviceId);
                    headers.put("Authorization-Basic", token);
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(context, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

}