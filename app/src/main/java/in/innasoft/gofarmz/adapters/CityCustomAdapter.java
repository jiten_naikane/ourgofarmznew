package in.innasoft.gofarmz.adapters;


import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.models.CitiesModel;
import in.innasoft.gofarmz.models.CityModel;
import in.innasoft.gofarmz.models.StatesModel;

public class CityCustomAdapter extends ArrayAdapter<CitiesModel>
{
    private List<CitiesModel> stateList = new ArrayList<>();
    Typeface light;

    public CityCustomAdapter(@NonNull Context context, int resource, int spinnerText, @NonNull List<CitiesModel> stateList)
    {
        super(context, resource, spinnerText, stateList);
        this.stateList = stateList;
        light = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.light));
    }

    @Override
    public CitiesModel getItem(int position) {
        return stateList.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position);

    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position);
    }

    /**
     * Gets the state object by calling getItem and
     * Sets the state name to the drop-down TextView.
     *
     * @param position the position of the item selected
     * @return returns the updated View
     */
    private View initView(int position) {
        CitiesModel cirty = getItem(position);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.row_cities_list, null);
        TextView textView =  v.findViewById(R.id.city_txt);
        textView.setText(cirty.getName());
        textView.setTypeface(light);
        return v;

    }
}



