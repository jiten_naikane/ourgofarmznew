package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.utils.AppUrls;

public class OrderHisDetailChildADapter extends BaseAdapter {

    private Context context;
    private ArrayList<Object> orderHisDetailChild = new ArrayList<>();
    private Typeface light, regular, bold;

    public OrderHisDetailChildADapter(Context ctx, ArrayList<Object> tmp) {

        context = ctx;
        orderHisDetailChild = tmp;

        light = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));
    }

    @Override
    public int getCount() {
        return orderHisDetailChild.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.row_order_hist_child_record_list, null, true);

        Map<String, Object> tmp = (Map<String, Object>) orderHisDetailChild.get(position);

        ImageView child_img = view.findViewById(R.id.child_img);

        Picasso.with(context)
                .load(AppUrls.IMAGE_URL + "70x70/" + (String) tmp.get("images"))
                .placeholder(R.drawable.cart)
                .into(child_img);

        String type=(String) tmp.get("type");

         Log.d("TTTTYPE",type);

        TextView child_name_txt = view.findViewById(R.id.child_name_txt);
        child_name_txt.setTypeface(light);
        child_name_txt.setText((String) tmp.get("product_name"));

        if(type.equalsIgnoreCase("ADDON"))
        {
            TextView child_finalprice_txt = view.findViewById(R.id.child_finalprice_txt);
            child_finalprice_txt.setTypeface(light);
            child_finalprice_txt.setVisibility(View.VISIBLE);
            child_finalprice_txt.setText((String) tmp.get("final_price"));

            TextView child_qty_txt = view.findViewById(R.id.child_qty_txt);
            child_qty_txt.setTypeface(light);
            child_qty_txt.setVisibility(View.VISIBLE);
            child_qty_txt.setText("Quantity - "+(String) tmp.get("quantity"));

            TextView mrp_price_txt = view.findViewById(R.id.mrp_price_txt);
            mrp_price_txt.setTypeface(light);
            mrp_price_txt.setVisibility(View.VISIBLE);
            mrp_price_txt.setText("Rs. "+(String) tmp.get("price")+"/-");

        }
        else
        {
          ///
        }

        TextView child_unit_txt = view.findViewById(R.id.child_unit_txt);
        child_unit_txt.setTypeface(light);
        child_unit_txt.setText((String) tmp.get("unit_value") + (String) tmp.get("unit_name"));

        TextView child_qty_txt = view.findViewById(R.id.child_qty_txt);
        child_qty_txt.setTypeface(light);
        child_qty_txt.setVisibility(View.VISIBLE);
        child_qty_txt.setText("Quantity - "+(String) tmp.get("quantity"));


        return view;
    }
}