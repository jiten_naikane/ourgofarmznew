package in.innasoft.gofarmz.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.OrderChildeViewDetails;
import in.innasoft.gofarmz.activities.OrderHistoryDetailActivity;
import in.innasoft.gofarmz.holders.OrderHisDetailHolder;
import in.innasoft.gofarmz.itemClickListerners.MyCartItemClickListener;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.NetworkChecking;

public class OrderHisDetailAdapter extends RecyclerView.Adapter<OrderHisDetailHolder> {

    private ArrayList<Object> orderHistDetailList;
    private ArrayList<Object> orderChildHistDetailList = new ArrayList<>();
    private OrderHistoryDetailActivity context;
    private LayoutInflater li;
    private int resource;
    private Typeface light, regular, bold;
    String deviceId, user_id, token, price;
    Dialog dialog;
    private boolean checkInternet;

    public OrderHisDetailAdapter(OrderHistoryDetailActivity context, ArrayList<Object> orderHistDetailList, int resource) {
        this.orderHistDetailList = orderHistDetailList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        light = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));
    }

    @Override
    public OrderHisDetailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        OrderHisDetailHolder slh = new OrderHisDetailHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final OrderHisDetailHolder holder, final int position) {

        final Map<String, Object> orderListProductObject = (Map<String, Object>) orderHistDetailList.get(position);

        holder.product_name.setTypeface(bold);
        holder.product_name.setText(orderListProductObject.get("product_name").toString());
       // holder.price_txt.setTypeface(bold);
        holder.price_txt.setText(orderListProductObject.get("total_price").toString());
        holder.qty_btn.setTypeface(bold);
        holder.qty_btn.setText("Qty: "+orderListProductObject.get("purchase_quantity").toString());

        Picasso.with(context)
                .load(AppUrls.IMAGE_URL + "70x70/" + orderListProductObject.get("images").toString())
                .placeholder(R.drawable.cart)
                .into(holder.product_img);

        holder.view_txt.setTypeface(light);

        holder.view_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                orderChildHistDetailList = (ArrayList<Object>) orderListProductObject.get("child_products");
                Intent it=new Intent(context, OrderChildeViewDetails.class);
                it.putExtra("productlist",orderChildHistDetailList);
                it.putExtra("TITLE",orderListProductObject.get("product_name").toString());
                context.startActivity(it);
                /*checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet)
                {
                    dialog = new Dialog(context);
                    dialog.setContentView(R.layout.cart_record_child_dialog);
                    TextView headet_text_category = dialog.findViewById(R.id.headet_text_category);
                    headet_text_category.setText(orderListProductObject.get("product_name").toString());
                    headet_text_category.setTypeface(bold);
                    ListView listRecordChild = dialog.findViewById(R.id.listRecordChild);
                    ImageView close = dialog.findViewById(R.id.close);


                    OrderHisDetailChildADapter adapter = new OrderHisDetailChildADapter(context, orderChildHistDetailList);
                    listRecordChild.setAdapter(adapter);

                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialog.show();

                    close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                }else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }*/
            }
        });

        holder.setItemClickListener(new MyCartItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {


            }
        });

    }

    @Override
    public int getItemCount() {

        return this.orderHistDetailList.size();
    }
}
