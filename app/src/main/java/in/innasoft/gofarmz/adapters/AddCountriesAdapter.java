package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.ChooseAddressActivity;
import in.innasoft.gofarmz.activities.EditAddressActivity;
import in.innasoft.gofarmz.filters.AddCountriesFilter;
import in.innasoft.gofarmz.filters.CountriesFilter;
import in.innasoft.gofarmz.holders.CountriesHolder;
import in.innasoft.gofarmz.itemClickListerners.CountriesItemClickListener;
import in.innasoft.gofarmz.models.CountriesModel;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class AddCountriesAdapter extends RecyclerView.Adapter<CountriesHolder> implements Filterable {

    public ArrayList<CountriesModel> countriesModels,filterList;
    AddCountriesFilter filter;
    private ChooseAddressActivity context;
    private LayoutInflater layoutInflater;
    private int resource;
    private Typeface light, regular, bold;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token, user_name, mobile;

    public AddCountriesAdapter(ArrayList<CountriesModel> countriesModels, ChooseAddressActivity context, int resource) {
        this.countriesModels = countriesModels;
        this.filterList = countriesModels;
        this.context = context;
        this.resource = resource;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        light = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));

        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_name = userDetails.get(UserSessionManager.USER_NAME);
        mobile = userDetails.get(UserSessionManager.USER_MOBILE);
        Log.d("SESSIONDATA", deviceId + "/n" + user_id + "/n" + token);
    }

    @Override
    public CountriesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = layoutInflater.inflate(resource, parent, false);
        CountriesHolder slh = new CountriesHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final CountriesHolder holder, final int position) {

        holder.country_txt.setText(countriesModels.get(position).getName());
        holder.country_txt.setTypeface(light);


        holder.setItemClickListener(new CountriesItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                context.setCountryName(countriesModels.get(pos).getName(),countriesModels.get(pos).getId());
            }
        });

    }

    @Override
    public int getItemCount() {
        return countriesModels.size();
    }

    @Override
    public Filter getFilter()
    {
        if(filter==null)
        {
            filter = new AddCountriesFilter(filterList,this);
        }

        return filter;
    }

}
