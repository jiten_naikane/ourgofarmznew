package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.EditAddressActivity;
import in.innasoft.gofarmz.filters.AreasFilter;
import in.innasoft.gofarmz.holders.AreasHolder;
import in.innasoft.gofarmz.itemClickListerners.AreasItemClickListener;
import in.innasoft.gofarmz.models.AreasModel;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class AreasAdapter extends RecyclerView.Adapter<AreasHolder> implements Filterable {

    public ArrayList<AreasModel> areasModels,filterList;
    AreasFilter filter;
    private EditAddressActivity context;
    private LayoutInflater layoutInflater;
    private int resource;
    private Typeface light, regular, bold;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token, user_name, mobile;

    public AreasAdapter(ArrayList<AreasModel> areasModels, EditAddressActivity context, int resource) {
        this.areasModels = areasModels;
        this.filterList = areasModels;
        this.context = context;
        this.resource = resource;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        light = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));

        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_name = userDetails.get(UserSessionManager.USER_NAME);
        mobile = userDetails.get(UserSessionManager.USER_MOBILE);
        Log.d("SESSIONDATA", deviceId + "/n" + user_id + "/n" + token);
    }

    @Override
    public AreasHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = layoutInflater.inflate(resource, parent, false);
        AreasHolder slh = new AreasHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final AreasHolder holder, final int position) {

        holder.area_txt.setText(areasModels.get(position).getName());
        holder.area_txt.setTypeface(light);


        holder.setItemClickListener(new AreasItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
               // context.setAreaName(areasModels.get(pos).getName(),areasModels.get(pos).getId());
            }
        });

    }

    @Override
    public int getItemCount() {
        return areasModels.size();
    }

    @Override
    public Filter getFilter()
    {
        if(filter==null)
        {
            filter=new AreasFilter(filterList,this);
        }

        return filter;
    }

}
