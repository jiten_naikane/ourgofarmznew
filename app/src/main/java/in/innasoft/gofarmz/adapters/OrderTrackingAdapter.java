package in.innasoft.gofarmz.adapters;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.OrderHistoryDetailActivity;
import in.innasoft.gofarmz.holders.TimeLineViewHolder;
import in.innasoft.gofarmz.utils.VectorDrawableUtils;

public class OrderTrackingAdapter extends RecyclerView.Adapter<TimeLineViewHolder> {

    private ArrayList<Object> orderTrackingList;
    private OrderHistoryDetailActivity context;
    private LayoutInflater li;
    private int resource;
    private Typeface light, regular, bold;
    String deviceId, user_id, token, price;
    Dialog dialog;
    private Context mContext;
    private boolean mWithLinePadding;
    private LayoutInflater mLayoutInflater;

    public OrderTrackingAdapter(OrderHistoryDetailActivity context, ArrayList<Object> orderTrackingList, boolean withLinePadding) {
        this.orderTrackingList = orderTrackingList;
        this.context = context;
        mWithLinePadding = withLinePadding;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        light = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));
    }

    @Override
    public TimeLineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        mContext = parent.getContext();
        mLayoutInflater = LayoutInflater.from(mContext);
        View view;
        view = mLayoutInflater.inflate(mWithLinePadding ? R.layout.item_timeline_line_padding : R.layout.item_timeline, parent, false);
        return new TimeLineViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(final TimeLineViewHolder holder, final int position) {

        final Map<String, Object> orderListProductObject = (Map<String, Object>) orderTrackingList.get(position);
        holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(mContext, R.drawable.ic_marker_active, R.color.green));
        holder.mMessage.setText(orderListProductObject.get("message").toString());
        holder.status.setText(orderListProductObject.get("order_status").toString());
        holder.mDate.setText(orderListProductObject.get("created_on").toString());

    }

    @Override
    public int getItemCount() {

        return this.orderTrackingList.size();
    }
}
