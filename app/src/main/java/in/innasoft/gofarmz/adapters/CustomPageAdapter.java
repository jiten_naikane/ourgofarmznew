package in.innasoft.gofarmz.adapters;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.ReferFarmerActivity;
import in.innasoft.gofarmz.models.AdBottomSliderModel;

public class CustomPageAdapter extends PagerAdapter {

    private MainActivity context;
    private ArrayList<AdBottomSliderModel> dataObjectList;
    private LayoutInflater layoutInflater;
    Typeface light,regular,bold;

    public CustomPageAdapter(MainActivity context, ArrayList<AdBottomSliderModel> dataObjectList) {
        this.context = context;
        this.dataObjectList = dataObjectList;
        this.layoutInflater = (LayoutInflater)this.context.getSystemService(this.context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return dataObjectList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View)object);
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {

        View view = this.layoutInflater.inflate(R.layout.row_ad_refer_bootom, container, false);

        light = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));


        TextView title =  view.findViewById(R.id.title);
        title.setText(dataObjectList.get(position).getTitle());

        TextView textdescr =  view.findViewById(R.id.textdescr);
        textdescr.setText(Html.fromHtml(dataObjectList.get(position).getDesc()));

        TextView do_txt = view.findViewById(R.id.do_txt);

        Button view_detail_btn =  view.findViewById(R.id.view_detail_btn);
        view_detail_btn.setText("View Details");
        view_detail_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
              if(position==0)
              {
                  Intent referfarmer=new Intent(context, ReferFarmerActivity.class);
                  context.startActivity(referfarmer);
              }
              else
              {
                /*
                   Intent offerdetail=new Intent(context, OfferDetailACtivity.class);
                  context.startActivity(offerdetail);*/
              }
            }
        });

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }


}
