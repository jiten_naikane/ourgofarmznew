package in.innasoft.gofarmz.adapters;


import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.EditAddressActivity;
import in.innasoft.gofarmz.activities.ReferFarmerActivity;
import in.innasoft.gofarmz.filters.StatesFilter;
import in.innasoft.gofarmz.holders.StatesHolder;
import in.innasoft.gofarmz.itemClickListerners.StatesItemClickListener;
import in.innasoft.gofarmz.models.StatesModel;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class StateCustomAdapter extends ArrayAdapter<StatesModel>
{
    private List<StatesModel> stateList = new ArrayList<>();
    Typeface light;

    public StateCustomAdapter(@NonNull Context context, int resource, int spinnerText, @NonNull List<StatesModel> stateList)
    {
        super(context, resource, spinnerText, stateList);
        this.stateList = stateList;
        light = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.light));
    }

    @Override
    public StatesModel getItem(int position) {
        return stateList.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position);

    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position);
    }

    /**
     * Gets the state object by calling getItem and
     * Sets the state name to the drop-down TextView.
     *
     * @param position the position of the item selected
     * @return returns the updated View
     */
    private View initView(int position) {
        StatesModel state = getItem(position);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.row_states_list, null);
        TextView textView =  v.findViewById(R.id.state_txt);
        textView.setText(state.getName());
        textView.setTypeface(light);
        return v;

    }
}



