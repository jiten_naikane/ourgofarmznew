package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.OrderHistoryDetailActivity;
import in.innasoft.gofarmz.activities.OrdersHistoryActivity;
import in.innasoft.gofarmz.itemClickListerners.MyCartItemClickListener;
import in.innasoft.gofarmz.models.OrderHistoryModel;
import in.innasoft.gofarmz.utils.NetworkChecking;

public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.OrderHistoryHolder> {

    public ArrayList<OrderHistoryModel> orderHisListModels;
    OrdersHistoryActivity context;
    LayoutInflater li;
    int resource;
    private Typeface light, regular, bold;
    private boolean checkInternet;

    public OrderHistoryAdapter(ArrayList<OrderHistoryModel> orderHisListModels, OrdersHistoryActivity context, int resource) {

        this.orderHisListModels = orderHisListModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        light = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));

    }

    @Override
    public OrderHistoryAdapter.OrderHistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        OrderHistoryAdapter.OrderHistoryHolder slh = new OrderHistoryAdapter.OrderHistoryHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final OrderHistoryAdapter.OrderHistoryHolder holder, final int position) {

        holder.order_id.setText(Html.fromHtml(orderHisListModels.get(position).order_id));
        holder.order_id.setTypeface(light);

        String coupn = orderHisListModels.get(position).coupon_code;

        if (coupn != null)
        {
            holder.coup_ll.setVisibility(View.VISIBLE);
            holder.coupan_apply.setVisibility(View.VISIBLE);
            holder.coupan_apply.setText(Html.fromHtml(orderHisListModels.get(position).coupon_code) + "  Coupon Applied");
        }


        holder.order_date.setText(Html.fromHtml(orderHisListModels.get(position).order_date));

        if (orderHisListModels.get(position).status.equalsIgnoreCase("0")) {
            holder.order_status.setTextColor(Color.parseColor("#FF8000"));
            holder.order_status.setText("Pending");
        } else if (orderHisListModels.get(position).status.equalsIgnoreCase("1")) {
            holder.order_status.setTextColor(Color.parseColor("#05914E"));
            holder.order_status.setText("Confirmed");
        } else if (orderHisListModels.get(position).status.equalsIgnoreCase("2")) {
            holder.order_status.setTextColor(Color.RED);
            holder.order_status.setText("Failed");
        } else if (orderHisListModels.get(position).status.equalsIgnoreCase("3")) {
            holder.order_status.setTextColor(Color.RED);
            holder.order_status.setText("Cancelled");
        } else if (orderHisListModels.get(position).status.equalsIgnoreCase("4")) {
            holder.order_status.setTextColor(Color.parseColor("#FF8000"));
            holder.order_status.setText("Dispatched");
        } else if (orderHisListModels.get(position).status.equalsIgnoreCase("6")) {
            holder.order_status.setTextColor(Color.parseColor("#5CD199"));
            holder.order_status.setText("Shipped");
        } else if (orderHisListModels.get(position).status.equalsIgnoreCase("7")) {
            holder.order_status.setTextColor(Color.parseColor("#05914E"));
            holder.order_status.setText("Delivered");
        } else if (orderHisListModels.get(position).status.equalsIgnoreCase("8")) {
            holder.order_status.setTextColor(Color.RED);
            holder.order_status.setText("Not Delivered");
        } else if (orderHisListModels.get(position).status.equalsIgnoreCase("11")) {
            holder.order_status.setTextColor(Color.BLUE);
            holder.order_status.setText("Received");
        }

        holder.ord_his_price_txt.setText(Html.fromHtml(orderHisListModels.get(position).final_price));

     //   holder.seriel_no.setText(Html.fromHtml("" + (position + 1)) + ".");

        holder.order_date.setTypeface(light);
        holder.order_status.setTypeface(light);
        //holder.ord_his_price_txt.setTypeface(light);
        holder.coupan_apply.setTypeface(light);

        holder.order_date_title.setTypeface(light);
        holder.coupan_apply_title.setTypeface(light);
        holder.order_status_title.setTypeface(light);
        holder.seriel_no.setTypeface(light);

        String ord_his_product_img = orderHisListModels.get(position).images;

        Picasso.with(context)
                .load(ord_his_product_img)
                .placeholder(R.drawable.cart)
                .into(holder.ord_his_product_img);

        holder.setItemClickListener(new MyCartItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
                    Intent intent = new Intent(context, OrderHistoryDetailActivity.class);
                    intent.putExtra("ORDERID", orderHisListModels.get(position).id);
                    context.startActivity(intent);
                } else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.orderHisListModels.size();
    }

    //HOLDER CLASS

    public class OrderHistoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public ImageView ord_his_product_img;
        public TextView order_date_title, coupan_apply_title, order_status_title, order_id, order_date, coupan_apply, order_status, ord_his_price_txt, seriel_no;
        MyCartItemClickListener myCartItemClickListener;
        LinearLayout coup_ll;

        public OrderHistoryHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            ord_his_product_img = itemView.findViewById(R.id.ord_his_product_img);
            order_date_title = itemView.findViewById(R.id.order_date_title);
            coupan_apply_title = itemView.findViewById(R.id.coupan_apply_title);
            order_status_title = itemView.findViewById(R.id.order_status_title);
            order_id = itemView.findViewById(R.id.order_id);
            order_date = itemView.findViewById(R.id.order_date);
            coupan_apply = itemView.findViewById(R.id.coupan_apply);
            order_status = itemView.findViewById(R.id.order_status);
            seriel_no = itemView.findViewById(R.id.seriel_no);
            coup_ll = itemView.findViewById(R.id.coup_ll);

            ord_his_price_txt = itemView.findViewById(R.id.ord_his_price_txt);

        }

        @Override
        public void onClick(View view) {
            this.myCartItemClickListener.onItemClick(view, getLayoutPosition());
        }

        public void setItemClickListener(MyCartItemClickListener ic) {
            this.myCartItemClickListener = ic;
        }
    }


}
