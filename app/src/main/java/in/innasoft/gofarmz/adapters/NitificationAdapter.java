package in.innasoft.gofarmz.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.ImageNotificationActivity;
import in.innasoft.gofarmz.activities.MyCartActivity;
import in.innasoft.gofarmz.activities.NotificationsActivity;
import in.innasoft.gofarmz.activities.OrderHistoryDetailActivity;
import in.innasoft.gofarmz.activities.PaymentDetails;
import in.innasoft.gofarmz.holders.NotificationHolder;
import in.innasoft.gofarmz.holders.OrderHisDetailHolder;
import in.innasoft.gofarmz.itemClickListerners.AreasItemClickListener;
import in.innasoft.gofarmz.itemClickListerners.MyCartItemClickListener;
import in.innasoft.gofarmz.models.NotificationModel;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.DividerItemDecorator;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class NitificationAdapter extends RecyclerView.Adapter<NotificationHolder> {

    NotificationsActivity context;
    private ArrayList<NotificationModel> notificationList;
    private LayoutInflater li;
    private int resource;
    private Typeface light,bold;
    String deviceId, user_id, token;
    SweetAlertDialog sd;
    UserSessionManager userSessionManager;
    private boolean checkInternet;
    String read_status,path;

    public NitificationAdapter( ArrayList<NotificationModel> notificationList,NotificationsActivity context, int resource) {
        this.notificationList = notificationList;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        light = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.light));
       // regular = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));
    }

    @Override
    public NotificationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        NotificationHolder slh = new NotificationHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final NotificationHolder holder, final int position) {


        read_status=   notificationList.get(position).is_read;


        Log.d("asd",read_status);


        if(read_status.equals("0"))
        {

            holder.linear_layout_row.setBackgroundColor(Color.parseColor("#d1f2e2"));
        }
        else
        {
           //nothing
        }


        holder.notify_title_name.setText(notificationList.get(position).title);
        holder.notify_title_name.setTypeface(bold);

        holder.notify_description.setTypeface(light);
        holder.notify_description.setText(notificationList.get(position).message);

        holder.notify_dateandtime.setTypeface(light);
        String timedate=parseDateToddMMyyyy(notificationList.get(position).created_on);
        holder.notify_dateandtime.setText(Html.fromHtml(timedate));


         holder.linear_layout_row.setOnClickListener(new View.OnClickListener()
         {
             @Override
             public void onClick(View v)
             {
                 path=  notificationList.get(position).image_path;
                 Log.d("RRRRRRRR",path);
                 if(path != null && !path.isEmpty())
                   {

                       String send_id=notificationList.get(position).id;
                       String notification_id=notificationList.get(position).notification_id;
                       sendReadStatus(send_id);
                       Intent intent = new Intent(context, ImageNotificationActivity.class);
                       intent.putExtra("GETDATANOTFY",notification_id);
                       context.startActivity(intent);
                   }
                   else
                   {

                       sd = new SweetAlertDialog(context, SweetAlertDialog.CUSTOM_IMAGE_TYPE);
                       sd.setTitleText(""+(notificationList.get(position).title));
                       sd.setContentText(""+notificationList.get(position).message);
                       sd.setConfirmText("OK");
                       sd.showCancelButton(true);
                       sd.setCanceledOnTouchOutside(false);
                       sd.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                           @Override
                           public void onClick(final SweetAlertDialog sweetAlertDialog)
                           {

                               String is_read=notificationList.get(position).is_read;
                               if(is_read.equals("0"))
                               {
                                   String send_id=notificationList.get(position).id;
                                   sendReadStatus(send_id);
                                   Intent intent = new Intent(context, MainActivity.class);
                                   context.startActivity(intent);

                               }
                               else
                               {
                                   sd.cancel();
                                   //nothin
                               }
                           }
                       });
                       sd.show();
                   }
             }
         });


        holder.setItemClickListener(new AreasItemClickListener() {
            @Override
            public void onItemClick(View v, int pos)
            {

            }
        });

    }

    private void sendReadStatus(String send_id)
    {
        checkInternet = NetworkChecking.isConnected(context);
        if (checkInternet) {
                            //  http://gofarmzv1.learningslot.in/api/user/51/notification/1/read
            String pushNotfyurl = AppUrls.BASE_URL + AppUrls.GET_PUSHNOTIFICATION + user_id+"/notification/"+send_id+"/read";
            Log.d("SENDSTATUSURL", pushNotfyurl);

            StringRequest strAllMember = new StringRequest(Request.Method.PUT, pushNotfyurl, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    Log.d("SENDSTATUSRESP", response);

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String successResponceCode = jsonObject.getString("status");
                        if (successResponceCode.equals("10100"))
                        {
                            // sd.cancel();
                             context.getNotification();

                        }
                        if (successResponceCode.equals("10200")) {

                            Toast.makeText(context, "Invalid Input..!", Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    Log.e("Error", error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization-Basic", token);
                    headers.put("x-device-id", deviceId);
                    headers.put("x-device-platform", "ANDROID");
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    Log.d("tttt", "tttt " + headers.toString());
                    return headers;
                }
            };
            strAllMember.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(strAllMember);
        } else {
            Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int getItemCount() {

        return this.notificationList.size();
    }


    public String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd MMM yyyy  HH:mm ";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }
}
