package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.holders.CategoryHolder;
import in.innasoft.gofarmz.itemClickListerners.CategoryItemClickListener;
import in.innasoft.gofarmz.models.CategoryModel;
import in.innasoft.gofarmz.utils.NetworkChecking;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryHolder> {

    private ArrayList<CategoryModel> categoryModels;
    private MainActivity context;
    private LayoutInflater li;
    private int resource;
    private Typeface light,regular,bold;
    private boolean checkInternet;

    String id,type;

    public SparseBooleanArray selectedButton = new SparseBooleanArray();

    public CategoryAdapter(ArrayList<CategoryModel> categoryModels, MainActivity context, int resource) {
        this.categoryModels = categoryModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        light = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));
    }

    @Override
    public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        CategoryHolder slh = new CategoryHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final CategoryHolder holder, final int position) {




        holder.category_name_txt.setTypeface(bold);
        holder.category_name_txt.setText(categoryModels.get(position).getName());

        if (categoryModels.get(position).getId().equalsIgnoreCase("2")){
            holder.category_name_txt.setTextColor(context.getResources().getColor(R.color.white));
        }

        Picasso.with(context)
                .load(categoryModels.get(position).getImage())
                .placeholder(R.drawable.cart)
                .into(holder.category_img);

        if(selectedButton.get(position)) {
            id = categoryModels.get(position).getId();
            type = categoryModels.get(position).getType_avail();
            if(position==0)
                context.getBoxProducts(id,type);
            else
                context.getBoxProducts(id,type);
            holder.category_name_txt.setTextColor(context.getResources().getColor(R.color.white));

            holder.relativ_category.setBackgroundColor(Color.parseColor("#05914E"));
            holder.arrow_img.setBackgroundResource(R.drawable.ic_menu_down_white_24dp);

        } else {
            holder.category_name_txt.setTextColor(context.getResources().getColor(R.color.cate_colr));
            holder.relativ_category.setBackgroundResource(R.drawable.cat_background);
            holder.arrow_img.setBackgroundResource(0);



        }


        holder.setItemClickListener(new CategoryItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {


                    selection(pos);

                    Log.d("IDDDDD", id);

                    id = categoryModels.get(position).getId();
                    type = categoryModels.get(position).getType_avail();
                    if(pos==0)
                        context.getBoxProducts(id,type);
                    else
                    context.getBoxProducts(id,type);
                }else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public int getItemCount() {

        return this.categoryModels.size();
    }

    public void selection(int position) {
        selectedButton.clear();
        selectView(position, true);
    }

    public void selectView(int position, boolean value) {
        if (value)
            selectedButton.put(position, value);
        else
            selectedButton.delete(position);
        notifyDataSetChanged();
    }



}
