package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.ChooseAddressActivity;
import in.innasoft.gofarmz.activities.DeliverySlotsActivity;
import in.innasoft.gofarmz.filters.AddCountriesFilter;
import in.innasoft.gofarmz.holders.CountriesHolder;
import in.innasoft.gofarmz.holders.DeliverySlotHolder;
import in.innasoft.gofarmz.itemClickListerners.CountriesItemClickListener;
import in.innasoft.gofarmz.models.CountriesModel;
import in.innasoft.gofarmz.models.DeliverySlotModel;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class DelivrySlotAdapter extends BaseAdapter {
    Context context;
    ArrayList<Object> daysarr;

    public DelivrySlotAdapter(Context context, ArrayList<Object> info) {
        this.context = context;
        daysarr = info;

    }

    @Override
    public int getCount() {
        return daysarr.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.row_delivery_slot, null);
            holder.day_text = convertView.findViewById(R.id.day_text);
            holder.listSlot = convertView.findViewById(R.id.listSlot);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Map<String, Object> tmpObj = (Map<String, Object>) daysarr.get(position);

        Log.d("DAAYS", tmpObj.toString());

        holder.day_text.setText((String) tmpObj.get("date"));//+" ("+(String)tmpObj.get("day")+")");

        ArrayList<Object> slotarr = (ArrayList<Object>) tmpObj.get("slots");

        Log.d("GEGEGE", slotarr.toString());

            SlotListAdapter  delivrySlotAdapter=new SlotListAdapter( context,slotarr);
            holder.listSlot.setAdapter(delivrySlotAdapter);



       /* priceList.clear();
        Log.d("KLLLK", priceList.toString());
        String price = "";
        for (int i = 0; i < slotarr.size(); i++) {
            Map<String, Object> stringObjectMap = (Map<String, Object>) slotarr.get(i);
            price = price + (String) stringObjectMap.get("start_time") + " -- " + (String) stringObjectMap.get("end_time") + "\n";
            priceList.add(price);
            holder.time_slot_text.setText(price);

        }
*/

        return convertView;
    }

    public static class ViewHolder {
        public TextView day_text;
        public ListView listSlot;
    }
}
