package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.ChooseAddressActivity;
import in.innasoft.gofarmz.activities.EditAddressActivity;
import in.innasoft.gofarmz.activities.PaymentDetails;
import in.innasoft.gofarmz.holders.AddressesHolder;
import in.innasoft.gofarmz.itemClickListerners.AddressesItemClickListener;
import in.innasoft.gofarmz.models.AddressesModel;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class AddressesAdapter extends RecyclerView.Adapter<AddressesHolder> {

    private ArrayList<AddressesModel> addressesModels;
    private ChooseAddressActivity context;
    private LayoutInflater layoutInflater;
    private int resource;
    private Typeface light, regular, bold;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token, user_name, mobile,sendmcontc;
    SweetAlertDialog sd;

    public AddressesAdapter(ArrayList<AddressesModel> addressesModels, ChooseAddressActivity context, int resource) {
        this.addressesModels = addressesModels;
        this.context = context;
        this.resource = resource;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        light = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));

        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_name = userDetails.get(UserSessionManager.USER_NAME);
        mobile = userDetails.get(UserSessionManager.USER_MOBILE);
        Log.d("SESSIONDATA", deviceId + "/n" + user_id + "/n" + token);
    }

    @Override
    public AddressesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = layoutInflater.inflate(resource, parent, false);
        AddressesHolder slh = new AddressesHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final AddressesHolder holder, final int position) {


        holder.name_txt.setText(addressesModels.get(position).getName());
        holder.name_txt.setTypeface(light);

        holder.address_one_txt.setText(addressesModels.get(position).getAddress_line1());
        holder.address_one_txt.setTypeface(light);

        holder.address_two_txt.setText(addressesModels.get(position).getAddress_line2());
        holder.address_two_txt.setTypeface(light);

        holder.area_txt.setText(addressesModels.get(position).getArea());
        holder.area_txt.setTypeface(light);

        holder.city_txt.setText(addressesModels.get(position).getCity());
        holder.city_txt.setTypeface(light);

        holder.state_txt.setText(addressesModels.get(position).getState());
        holder.state_txt.setTypeface(light);

        holder.pincode_txt.setText(addressesModels.get(position).getPincode());
        holder.pincode_txt.setTypeface(light);

        //mobile and alternate no.  in one text
        holder.mobile_txt.setText(addressesModels.get(position).getContact_no()+", "+addressesModels.get(position).getAlternate_contact_no());
        holder.mobile_txt.setTypeface(light);




        holder.edit_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
                    Intent intent = new Intent(context, EditAddressActivity.class);
                    intent.putExtra("id", addressesModels.get(position).getId());
                    context.startActivity(intent);
                }else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.delete_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet)
                {
                    sd = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                    sd.setTitleText("Are you sure?");
                    sd.setContentText("This product will be delete from cart!");
                    sd.setCancelText("No,cancel pls!");
                    sd.setConfirmText("Yes,delete it!");
                    sd.showCancelButton(true);

                    sd.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(final SweetAlertDialog sweetAlertDialog)
                        {
                            StringRequest stringRequest = new StringRequest(Request.Method.DELETE, AppUrls.BASE_URL + AppUrls.ADDADDRESS + user_id + "/addresses/" + addressesModels.get(position).getId(),
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {

                                            try {
                                                JSONObject jsonObject = new JSONObject(response);
                                                String status = jsonObject.getString("status");

                                                if (status.equals("10100")) {
                                                    sweetAlertDialog.cancel();
                                                    String message = jsonObject.getString("message");
                                                    addressesModels.remove(position);
                                                    //context.getAddresses();
                                                    notifyDataSetChanged();

                                                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                                                }

                                                if (status.equals("10200")) {

                                                    Toast.makeText(context, "Invalid Input..", Toast.LENGTH_SHORT).show();
                                                }

                                                if (status.equals("10300")) {

                                                    Toast.makeText(context, "No Data Found..", Toast.LENGTH_SHORT).show();
                                                }

                                                if (status.equals("10400")) {

                                                    Toast.makeText(context, "Unable to Send Otp..", Toast.LENGTH_SHORT).show();
                                                }

                                            } catch (JSONException e) {
                                                e.printStackTrace();

                                            }
                                        }
                                    },
                                    new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            error.getMessage();
                                        }
                                    }) {

                                @Override
                                public Map<String, String> getHeaders() {
                                    Map<String, String> headers = new HashMap<>();
                                    headers.put("X-Platform", "ANDROID");
                                    headers.put("X-Deviceid", deviceId);
                                    headers.put("Authorization-Basic", token);
                                    return headers;
                                }
                            };

                            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            RequestQueue requestQueue = Volley.newRequestQueue(context);
                            requestQueue.add(stringRequest);
                        }
                    });


                    sd.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.cancel();
                        }
                    });
                    sd.show();

                }else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

       /* holder.setItemClickListener(new AddressesItemClickListener() {
            @Override
            public void onItemClick(View v, int pos)
            {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
                    Intent intent = new Intent(context, PaymentDetails.class);
                    intent.putExtra("activity", "ChooseAddressAdapter");
                    intent.putExtra("id", addressesModels.get(position).getId());
                    intent.putExtra("contact_no", addressesModels.get(position).getContact_no());
                    context.startActivity(intent);
                }else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });*/

        holder.selectAddText.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                checkInternet = NetworkChecking.isConnected(context);
                if (checkInternet) {
                    Intent intent = new Intent(context, PaymentDetails.class);
                    intent.putExtra("activity", "ChooseAddressAdapter");
                    intent.putExtra("id", addressesModels.get(position).getId());
                    intent.putExtra("contact_no", addressesModels.get(position).getContact_no());
                    context.startActivity(intent);
                }else {
                    Toast.makeText(context, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return addressesModels.size();
    }
}
