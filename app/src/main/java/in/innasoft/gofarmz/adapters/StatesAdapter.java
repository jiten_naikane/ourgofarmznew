package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.HashMap;

import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.EditAddressActivity;
import in.innasoft.gofarmz.filters.CountriesFilter;
import in.innasoft.gofarmz.filters.StatesFilter;
import in.innasoft.gofarmz.holders.StatesHolder;
import in.innasoft.gofarmz.itemClickListerners.StatesItemClickListener;
import in.innasoft.gofarmz.models.StatesModel;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class StatesAdapter extends RecyclerView.Adapter<StatesHolder> implements Filterable {

    public ArrayList<StatesModel> statesModels,filterList;
    StatesFilter filter;
    private EditAddressActivity context;
    private LayoutInflater layoutInflater;
    private int resource;
    private Typeface light, regular, bold;
    private boolean checkInternet;
    UserSessionManager userSessionManager;
    String deviceId, user_id, token, user_name, mobile;

    public StatesAdapter(ArrayList<StatesModel> statesModels, EditAddressActivity context, int resource) {
        this.statesModels = statesModels;
        this.filterList = statesModels;
        this.context = context;
        this.resource = resource;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        light = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));

        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        user_name = userDetails.get(UserSessionManager.USER_NAME);
        mobile = userDetails.get(UserSessionManager.USER_MOBILE);
        Log.d("SESSIONDATA", deviceId + "/n" + user_id + "/n" + token);
    }

    @Override
    public StatesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = layoutInflater.inflate(resource, parent, false);
        StatesHolder slh = new StatesHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final StatesHolder holder, final int position) {

        holder.state_txt.setText(statesModels.get(position).getName());
        holder.state_txt.setTypeface(light);


        holder.setItemClickListener(new StatesItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
              //  context.setStateName(statesModels.get(pos).getName(),statesModels.get(pos).getId());
            }
        });

    }

    @Override
    public int getItemCount() {
        return statesModels.size();
    }

    @Override
    public Filter getFilter()
    {
        if(filter==null)
        {
            filter=new StatesFilter(filterList,this);
        }

        return filter;
    }

}
