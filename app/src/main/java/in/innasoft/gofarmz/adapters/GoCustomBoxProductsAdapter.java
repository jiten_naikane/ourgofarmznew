package in.innasoft.gofarmz.adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.innasoft.gofarmz.MainActivity;
import in.innasoft.gofarmz.R;
import in.innasoft.gofarmz.activities.FormarActivity;
import in.innasoft.gofarmz.holders.CustomBoxProductsHolder;
import in.innasoft.gofarmz.itemClickListerners.CustomBoxProductsItemClickListener;
import in.innasoft.gofarmz.models.CustomModel;
import in.innasoft.gofarmz.models.OptionsModel;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.UserSessionManager;

public class GoCustomBoxProductsAdapter extends RecyclerView.Adapter<CustomBoxProductsHolder> implements View.OnClickListener{

    private Context context;
    private ArrayList<CustomModel> customBoxProductsModels;
    private ArrayList<OptionsModel> customBoxOptionsModels;
    ArrayList<Object> subProductList = new ArrayList<Object>();
    private LayoutInflater li;
    public String prdo_id;
    private int resource;
    private Typeface light, regular, bold;
    private boolean checkInternet;
    String deviceId, user_id, token, price, qtyprice = "";
    UserSessionManager userSessionManager;
    ArrayList<String> selchkboxlist = new ArrayList<String>();
    public ArrayList<String> cartarr = new ArrayList<String>();
    TextView total_price_txt;
    int total = 0;
    int selectedpos = 0;
    String cartstr = "";
    String productId = "";
    Map<String, Object> stringObjectMap = new HashMap<>();
    boolean isfirsttime = true;

    public GoCustomBoxProductsAdapter(Context context, ArrayList<CustomModel> customBoxProductsModels, int resource, TextView textview, String productId) {
        this.customBoxProductsModels = customBoxProductsModels;

        this.context = context;
        this.resource = resource;
        total_price_txt = textview;
        this.productId = productId;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        userSessionManager = new UserSessionManager(context);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "\n" + user_id + "\n" + token);

        light = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.bold));
    }

    @Override
    public CustomBoxProductsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = li.inflate(resource, parent, false);
        CustomBoxProductsHolder slh = new CustomBoxProductsHolder(layout);
        return slh;
    }

    @Override
    public void onBindViewHolder(final CustomBoxProductsHolder holder, final int position) {

        final ArrayList<String> priceList = new ArrayList<String>(customBoxProductsModels.size());
        final ArrayList<String> qtypriceList = new ArrayList<String>(customBoxProductsModels.size());
        final ArrayList<Integer> qtylist = new ArrayList<Integer>(customBoxProductsModels.size());

        holder.product_name_txt.setTypeface(bold);
        holder.product_name_txt.setText(customBoxProductsModels.get(position).getPdtName());
     //   Log.d("PDTNAME",customBoxProductsModels.get(position).getPdtName()+"----"+customBoxProductsModels.get(position).getId());

        Picasso.with(context)
                .load(AppUrls.IMAGE_URL + "70x70/" + customBoxProductsModels.get(position).getImages())
                .placeholder(R.drawable.cart)
                .into(holder.product_img);
        holder.product_img.setOnClickListener(this);
        holder.product_img.setTag(position);

        checkInternet = NetworkChecking.isConnected(context);

        if (checkInternet) {
            subProductList = (ArrayList<Object>) customBoxProductsModels.get(position).getOptions();
            final ArrayList<String> qtyLists = new ArrayList<String>(customBoxProductsModels.size());
            qtyLists.clear();
            priceList.clear();
            qtypriceList.clear();
            for (int i = 0; i < subProductList.size(); i++) {

                Map<String, Object> stringObjectMap = (Map<String, Object>) subProductList.get(i);
                String optId = (String) stringObjectMap.get("optId");
                String pdtId = (String) stringObjectMap.get("pdtId");
                String optName = (String) stringObjectMap.get("optName");
                String price = (String) stringObjectMap.get("price");
                String qtyprice = (String) stringObjectMap.get("qtyprice");
                int qty = (int) stringObjectMap.get("qty");
                qtylist.add(qty);
                priceList.add(price);
                qtypriceList.add(qtyprice);
                qtyLists.add(optName + " for Rs." + qtyprice);
            }
            String pricestr = priceList.get(0);
            holder.price_txt.setText("Rs." + pricestr);
            holder.qty_txt.setText("" + qtylist.get(0));


            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(context, R.layout.qty_spinner, qtyLists);
            holder.qty_spinner.setAdapter(spinnerArrayAdapter);
            Map<String, Object> stringObjectMap = (Map<String, Object>) subProductList.get(0);
            String optId = (String) stringObjectMap.get("optId");
            String pdtId = (String) stringObjectMap.get("pdtId");
            int qty = (int) stringObjectMap.get("qty");

            holder.qty_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                    selectedpos = i;

                    String pricestr = priceList.get(i);
                    holder.price_txt.setText("Rs." + pricestr);
                    holder.qty_txt.setText("" + qtylist.get(i));
                    ArrayList<Object> tmparr = (ArrayList<Object>) customBoxProductsModels.get(position).getOptions();


                    if (i > 0) {
                        Map<String, Object> spinnerObject = (Map<String, Object>) tmparr.get(holder.qty_spinner.getSelectedItemPosition());
                        Map<String, Object> tmpObj = new HashMap<>();
                        tmpObj.put("optId", spinnerObject.get("optId"));
                        tmpObj.put("pdtId", spinnerObject.get("pdtId"));
                        tmpObj.put("optName", spinnerObject.get("optName"));
                        tmpObj.put("price", pricestr);
                        tmpObj.put("qtyprice", spinnerObject.get("qtyprice"));
                        tmpObj.put("qty", 1);
                        int itemPos = tmparr.indexOf(tmpObj);
                        tmparr.remove(itemPos);
                        tmparr.add(0, tmpObj);
                        customBoxProductsModels.get(position).setOptions(tmparr);
                        notifyDataSetChanged();

                        if (selchkboxlist.size() > 0) {
                            Map<String, Object> spinnerObject1 = (Map<String, Object>) tmparr.get(1);
                            String pdtIdstr = (String) spinnerObject1.get("pdtId");
                            String optIdstr = (String) spinnerObject1.get("optId");
                            int recentqty1 = (Integer) spinnerObject1.get("qty");
                            ;
                            SharedPreferences sharedpreferences1 = context.getSharedPreferences("Products", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedpreferences1.edit();
                            editor.putString("pdtId", pdtIdstr);
                            editor.putString("optId", optIdstr);
                            editor.putInt("qty", recentqty1);
                            editor.apply();
                            if (holder.add_chk.isChecked()) {

                                //selpricechkboxlist.set(selpricechkboxlist.size()-1,result);
                                String pdtId = (String) spinnerObject.get("pdtId");
                                String optId = (String) spinnerObject.get("optId");
                                int qty1 = (Integer) spinnerObject.get("qty");
                                int recentqty = Integer.parseInt(holder.qty_txt.getText().toString());
                                SharedPreferences sharedpreferences = context.getSharedPreferences("Products", Context.MODE_PRIVATE);
                                String oldpdtid = sharedpreferences.getString("pdtId", "");
                                String oldoptid = sharedpreferences.getString("optId", "");
                                Integer oldqty = sharedpreferences.getInt("qty", 0);
                                cartstr = pdtId + "_" + optId + "_" + recentqty;
                                String tmpstr = oldpdtid + "_" + oldoptid + "_" + oldqty;
                                int cartPos = cartarr.indexOf(tmpstr);
                                cartarr.remove(cartPos);
                                cartarr.add(cartPos, cartstr);
                                Log.v("CartArr", "///" + cartarr.toString());
                                String totalprice = total_price_txt.getText().toString();
                                String[] split = totalprice.split(":");
                                int total = Integer.parseInt(split[1].trim()) - (Integer.parseInt(priceList.get(0)));
                                if (totalprice.length() > 0)
                                    total_price_txt.setText("  Total Price : " + (total + Integer.parseInt(pricestr)));
                            }
                        }

                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
        if (productId != null && productId.length() > 0) {
            if (isfirsttime) {
                int itemPos = 0;
                for (int i = 0; i < customBoxProductsModels.size(); i++) {
                    if (productId.equals(customBoxProductsModels.get(i).getId())) {
                        itemPos = i;
                    }
                }
                ArrayList<Object> tmparr = (ArrayList<Object>) customBoxProductsModels.get(itemPos).getOptions();
                Map<String, Object> spinnerObject = (Map<String, Object>) tmparr.get(0);
                String pdtId = (String) spinnerObject.get("pdtId");
                String optId = (String) spinnerObject.get("optId");
                int qty = (Integer) spinnerObject.get("qty");
                cartstr = pdtId + "_" + optId + "_" + qty;
                cartarr.add(cartstr);
                Log.v("CartArr", "///" + cartarr.toString());
                selchkboxlist.add(customBoxProductsModels.get(itemPos).getId().toString());
                String selectedPrice = (String) spinnerObject.get("price");
                String totalprice = total_price_txt.getText().toString();
                String[] split = totalprice.split(":");
                total_price_txt.setText("  Total Price : " + (Integer.parseInt(split[1].trim()) + Integer.parseInt(selectedPrice.trim())));
                // Toast.makeText(context, selchkboxlist.toString().replace("[","").replace("]",""), Toast.LENGTH_SHORT).show();
                Log.d("TOTALPRICES", selectedPrice);
                customBoxProductsModels.get(itemPos).setSelected(true);
                isfirsttime = false;
            }
        }

        holder.add_chk.setOnCheckedChangeListener(null);
        holder.add_chk.setChecked(customBoxProductsModels.get(position).isSelected());
        holder.add_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                int qty = Integer.parseInt(holder.qty_txt.getText().toString());

                if(qty==8)
                {
                    Toast.makeText(context,"Sorry,You Can't add more qunatity!!!",Toast.LENGTH_LONG).show();
                }
                else
                {
                    if (qty != 0) {
                        ArrayList<Object> tmparr = (ArrayList<Object>) customBoxProductsModels.get(position).getOptions();
                        Map<String, Object> spinnerObject = (Map<String, Object>) tmparr.get(0);
                        if (qty == 1)
                            price = qtypriceList.get(0);
                        qty++;

                        String selectedprice = (String) spinnerObject.get("qtyprice");
                        holder.qty_txt.setText(String.valueOf(qty));
                        double finalPrice = qty * Double.parseDouble(selectedprice);
                        String result = new DecimalFormat("##.##").format(finalPrice);
                        holder.price_txt.setText("Rs." + result);
                        Map<String, Object> tmpObj = new HashMap<>();
                        tmpObj.put("optId", spinnerObject.get("optId"));
                        tmpObj.put("pdtId", spinnerObject.get("pdtId"));
                        tmpObj.put("optName", spinnerObject.get("optName"));
                        tmpObj.put("price", result);
                        tmpObj.put("qtyprice", spinnerObject.get("qtyprice"));
                        tmpObj.put("qty", qty);
                        tmparr.set(0, tmpObj);
                        customBoxProductsModels.get(position).setOptions(tmparr);
                        notifyDataSetChanged();
                        if (selchkboxlist.size() > 0) {
                            if (holder.add_chk.isChecked()) {
                                String pdtId = (String) spinnerObject.get("pdtId");
                                String optId = (String) spinnerObject.get("optId");
                                int qty1 = (Integer) spinnerObject.get("qty");
                                int recentqty = Integer.parseInt(holder.qty_txt.getText().toString());
                                SharedPreferences sharedpreferences = context.getSharedPreferences("Products", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putString("pdtId", pdtId);
                                editor.putString("optId", optId);
                                editor.putInt("qty", recentqty);
                                editor.apply();
                                cartstr = pdtId + "_" + optId + "_" + recentqty;
                                String tmpstr = pdtId + "_" + optId + "_" + qty1;
                                int itemPos = cartarr.indexOf(tmpstr);
                                cartarr.remove(itemPos);
                                cartarr.add(itemPos, cartstr);
                                //cartarr.set(position,cartstr);
                                Log.v("CartArr", "///" + cartarr.toString());
                                String totalprice = total_price_txt.getText().toString();
                                String[] split = totalprice.split(":");
                                if (totalprice.length() > 0)
                                    total_price_txt.setText("  Total Price : " + (Integer.parseInt(split[1].trim()) + Integer.parseInt(price)));
                            }
                        }

                    } else {
                        Toast.makeText(context, "Minimum 1 Quantity Required...!", Toast.LENGTH_SHORT).show();
                    }
                }


            }
        });

        holder.minus_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int qty = Integer.parseInt(holder.qty_txt.getText().toString());
                if (qty > 1) {

                    qty--;
                    if (qty == 1)
                        price = qtypriceList.get(0);
                    ArrayList<Object> tmparr = (ArrayList<Object>) customBoxProductsModels.get(position).getOptions();
                    Map<String, Object> spinnerObject = (Map<String, Object>) tmparr.get(0);
                    String selectedprice = (String) spinnerObject.get("qtyprice");
                    holder.qty_txt.setText(String.valueOf(qty));
                    double finalPrice = qty * Double.parseDouble(selectedprice);
                    String result = new DecimalFormat("##.##").format(finalPrice);
                    holder.price_txt.setText("Rs." + result);
                    Map<String, Object> tmpObj = new HashMap<>();
                    tmpObj.put("optId", spinnerObject.get("optId"));
                    tmpObj.put("pdtId", spinnerObject.get("pdtId"));
                    tmpObj.put("optName", spinnerObject.get("optName"));
                    tmpObj.put("price", result);
                    tmpObj.put("qtyprice", spinnerObject.get("qtyprice"));
                    tmpObj.put("qty", qty);
                    tmparr.set(0, tmpObj);
                    customBoxProductsModels.get(position).setOptions(tmparr);
                    notifyDataSetChanged();
                    if (selchkboxlist.size() > 0) {
                        try {
                            if (holder.add_chk.isChecked()) {
                                String pdtId = (String) spinnerObject.get("pdtId");
                                String optId = (String) spinnerObject.get("optId");
                                int qty1 = (Integer) spinnerObject.get("qty");
                                int recentqty = Integer.parseInt(holder.qty_txt.getText().toString());
                                SharedPreferences sharedpreferences = context.getSharedPreferences("Products", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putString("pdtId", pdtId);
                                editor.putString("optId", optId);
                                editor.putInt("qty", recentqty);
                                editor.apply();
                                cartstr = pdtId + "_" + optId + "_" + recentqty;
                                String tmpstr = pdtId + "_" + optId + "_" + qty1;
                                int itemPos = cartarr.indexOf(tmpstr);
                                cartarr.remove(itemPos);
                                cartarr.add(itemPos, cartstr);
                                Log.v("CartArr", "///" + cartarr.toString());
                                String totalprice = total_price_txt.getText().toString();
                                String[] split = totalprice.split(":");
                                if (totalprice.length() > 0) {
                                    if (Integer.parseInt(split[1].trim()) != 0)
                                        total_price_txt.setText("  Total Price : " + (Integer.parseInt(split[1].trim()) - Integer.parseInt(price)));
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } else {
                    Toast.makeText(context, "Minimum 1 Quantity Required...!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        holder.add_chk.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                int pos = (int) compoundButton.getTag();
                customBoxProductsModels.get(holder.getAdapterPosition()).setSelected(isChecked);
                if (isChecked) {
                    ArrayList<Object> tmparr = (ArrayList<Object>) customBoxProductsModels.get(position).getOptions();
                    Map<String, Object> spinnerObject = (Map<String, Object>) tmparr.get(0);
                    String pdtId = (String) spinnerObject.get("pdtId");
                    String optId = (String) spinnerObject.get("optId");
                    int qty = (Integer) spinnerObject.get("qty");
                    cartstr = pdtId + "_" + optId + "_" + qty;
                    cartarr.add(cartstr);
                    Log.v("CartArr", "///" + cartarr.toString());
                    selchkboxlist.add(customBoxProductsModels.get(pos).getId().toString());
                    String selectedPrice = holder.price_txt.getText().toString();
                    String totalprice = total_price_txt.getText().toString();
                    String[] split = totalprice.split(":");
                    total_price_txt.setText("  Total Price : " + (Integer.parseInt(split[1].trim()) + Integer.parseInt(selectedPrice.substring(3).trim())));
                    // Toast.makeText(context, selchkboxlist.toString().replace("[","").replace("]",""), Toast.LENGTH_SHORT).show();
                    Log.d("TOTALPRICES", selectedPrice);

                } else {
                    selchkboxlist.remove(customBoxProductsModels.get(pos).getId().toString());
                    ArrayList<Object> tmparr = (ArrayList<Object>) customBoxProductsModels.get(position).getOptions();
                    Map<String, Object> spinnerObject = (Map<String, Object>) tmparr.get(0);
                    String pdtId = (String) spinnerObject.get("pdtId");
                    String optId = (String) spinnerObject.get("optId");
                    int qty = (Integer) spinnerObject.get("qty");
                    cartstr = pdtId + "_" + optId + "_" + qty;
                    cartarr.remove(cartstr);
                    Log.v("CartArr", "///" + cartarr.toString());
                    String selectedPrice = holder.price_txt.getText().toString();
                    String totalprice = total_price_txt.getText().toString();
                    String[] split = totalprice.split(":");
                    total_price_txt.setText("  Total Price : " + (Integer.parseInt(split[1].trim()) - Integer.parseInt(selectedPrice.substring(3).trim())));

                }
            }
        });

        holder.setItemClickListener(new CustomBoxProductsItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {

            }
        });
        holder.add_chk.setTag(position);

        holder.product_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {


                Intent it = new Intent(context, FormarActivity.class);
                it.putExtra("IDD", customBoxProductsModels.get(position).getId());
                it.putExtra("Name", customBoxProductsModels.get(position).getPdtName());
                it.putExtra("Image", customBoxProductsModels.get(position).getImages());
                it.putExtra("FROM","FROMBROWSE");
                context.startActivity(it);


            }
        });
    }

    @Override
    public int getItemCount() {

        return this.customBoxProductsModels.size();
    }

    private void chkSelected() {

    }

    @Override
    public void onClick(View view) {
        int pos= (int) view.getTag();
      /*  Map<String,Object> tmpObj=new HashMap<>();
        tmpObj.put("pdtId",customBoxProductsModels.get(pos).getId());
        tmpObj.put("childPdtName",customBoxProductsModels.get(pos).getPdtName());
        tmpObj.put("images",customBoxProductsModels.get(pos).getImages());
        Intent it=new Intent(context,FormarActivity.class);
        it.putExtra("productobj",new HashMap<>(tmpObj));
        context.startActivity(it);*/
    }
}
