package in.innasoft.gofarmz;

import android.animation.Animator;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.smarteist.autoimageslider.SliderLayout;
import com.smarteist.autoimageslider.SliderView;
import com.squareup.picasso.Picasso;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.innasoft.gofarmz.activities.CustomBoxProductsActivity;
import in.innasoft.gofarmz.activities.DeliverySlotsActivity;
import in.innasoft.gofarmz.activities.FarmerNetworkActivity;
import in.innasoft.gofarmz.activities.FormarActivity;
import in.innasoft.gofarmz.activities.KYFActivity;
import in.innasoft.gofarmz.activities.MyCartActivity;
import in.innasoft.gofarmz.activities.NotificationsActivity;
import in.innasoft.gofarmz.activities.OrdersHistoryActivity;
import in.innasoft.gofarmz.activities.OurStoryActivity;
import in.innasoft.gofarmz.activities.PaymentDetails;
import in.innasoft.gofarmz.activities.ProfileActivity;
import in.innasoft.gofarmz.activities.ReferFarmerActivity;
import in.innasoft.gofarmz.activities.SupportActivity;
import in.innasoft.gofarmz.activities.TermsConditionsAndPrivacy;
import in.innasoft.gofarmz.adapters.BoxNameAdapter;
import in.innasoft.gofarmz.adapters.BoxProductsAdapter;
import in.innasoft.gofarmz.adapters.CategoryAdapter;
import in.innasoft.gofarmz.adapters.CustomPageAdapter;
import in.innasoft.gofarmz.adapters.GoCustomBoxProductsAdapter;
import in.innasoft.gofarmz.animation.GuillotineAnimation;
import in.innasoft.gofarmz.database.CategoryDbHelper;
import in.innasoft.gofarmz.models.AdBottomSliderModel;
import in.innasoft.gofarmz.models.CategoryModel;
import in.innasoft.gofarmz.models.CustomModel;
import in.innasoft.gofarmz.models.SliderModel;
import in.innasoft.gofarmz.utils.AppUrls;
import in.innasoft.gofarmz.utils.CircleAnimationUtil;
import in.innasoft.gofarmz.utils.Config;
import in.innasoft.gofarmz.utils.NetworkChecking;
import in.innasoft.gofarmz.utils.ScrollableGridView;
import in.innasoft.gofarmz.utils.UserSessionManager;
import in.innasoft.gofarmz.utils.viewpager.EnchantedViewPager;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private static final long RIPPLE_DURATION = 250;
    FrameLayout root;
    RelativeLayout toolbar;
    ImageView contentHamburger, cart_img,no_product_img,viewpager_image;
    String productid="";
    String selectedType="";
    RelativeLayout price_rl,go_box_bottom,layoutBottom;
    ImageView rupee_img, proceed_img, veg_img, kyf_img, story_img, fn_img, support_img, buy_img,terms_faq_policies_img;
    TextView cart_count_txt, price_txt,box_name_txt, kyf_txt, story_txt, fn_txt, support_txt,terms_faq_policies_txt, profile_txt, orders_txt, share_txt, notifications_txt,ref_farmer_menu;
    LinearLayout box_ll, kyf_group, story_group, fn_group, support_group,terms_faq_policies,refer_farmer;
    Button custom_btn;
    Typeface light, regular, bold;
     TextView total_price_txt;
    ScrollableGridView box_products_grid;
    ArrayList<CustomModel> productsList = new ArrayList<>();
    RecyclerView cat_recyclerview, box_recyclerview,go_box_recyclerview;
     LinearLayout comobo_liner_layout;
     RelativeLayout rllll;
    CategoryAdapter categoryAdapter;
    ArrayList<CategoryModel> categoryModels = new ArrayList<CategoryModel>();
    private Boolean exit = false;
    BoxNameAdapter boxNameAdapter;
    GoCustomBoxProductsAdapter customBoxProductsAdapter;
    ArrayList<Object> boxNameList = new ArrayList<>();
    private boolean checkInternet;
    BoxProductsAdapter boxProductsAdapter;
    ArrayList<Object> boxProductList = new ArrayList<>();
    ArrayList<SliderModel> slidermodel = new ArrayList<>();
    UserSessionManager userSessionManager;
    String deviceId, user_id, token;
    SliderLayout sliderLayout;
    /*Database*/
    CategoryDbHelper categoryDbHelper;


    TextView desp_txt;
    String pr_id,android_version,ios_version,checnkVeriosn;

    SweetAlertDialog  sd_update;


    public String user_image;
    View guillotineMenu;

    CustomPageAdapter mCustomPagerAdapter;
    private EnchantedViewPager viewPager;
    ArrayList<AdBottomSliderModel> adSliderList = new ArrayList<>();
    ProgressDialog pprogressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        PackageInfo eInfo = null;
        try {
            eInfo = getPackageManager().getPackageInfo("in.innasoft.gofarmz", 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        checnkVeriosn=eInfo.versionName;

        light = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.light));
        regular = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.regular));
        bold = Typeface.createFromAsset(this.getAssets(), getResources().getString(R.string.bold));
        comobo_liner_layout=findViewById(R.id.comobo_liner_layout);
        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        deviceId = userDetails.get(UserSessionManager.DEVICE_ID);
        user_id = userDetails.get(UserSessionManager.USER_ID);
        token = userDetails.get(UserSessionManager.KEY_ACCSES);
        Log.d("SESSIONDATA", deviceId + "/n" + user_id + "/n" + token+"///////"+checnkVeriosn);

        pprogressDialog = new ProgressDialog(MainActivity.this);
        pprogressDialog.setMessage("Please wait......");
        pprogressDialog.setProgressStyle(R.style.DialogTheme);

        no_product_img = findViewById(R.id.no_product_img);
        viewpager_image = findViewById(R.id.viewpager_image);

        root = findViewById(R.id.root);
        toolbar = findViewById(R.id.toolbar);
        contentHamburger = findViewById(R.id.content_hamburger);
        checkInternet = NetworkChecking.isConnected(this);


         guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine, null);
        root.addView(guillotineMenu);

        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setStartDelay(RIPPLE_DURATION)
                .setActionBarViewForAnimation(toolbar)
                .setClosedOnStart(true)
                .build();



        cart_img = findViewById(R.id.cart_img);

        cart_img.setOnClickListener(this);


        categoryDbHelper = new CategoryDbHelper(MainActivity.this);


        kyf_group = guillotineMenu.findViewById(R.id.kyf_group);
        kyf_img = guillotineMenu.findViewById(R.id.kyf_img);
        kyf_txt = guillotineMenu.findViewById(R.id.kyf_txt);
        kyf_txt.setTypeface(bold);
        story_group = guillotineMenu.findViewById(R.id.story_group);
        story_img = guillotineMenu.findViewById(R.id.story_img);
        story_txt = guillotineMenu.findViewById(R.id.story_txt);
        story_txt.setTypeface(bold);
        fn_group = guillotineMenu.findViewById(R.id.fn_group);
        fn_img = guillotineMenu.findViewById(R.id.fn_img);
        fn_txt = guillotineMenu.findViewById(R.id.fn_txt);
        fn_txt.setTypeface(bold);
        support_group = guillotineMenu.findViewById(R.id.support_group);
        support_img = guillotineMenu.findViewById(R.id.support_img);
        support_txt = guillotineMenu.findViewById(R.id.support_txt);
        support_txt.setTypeface(bold);

        ref_farmer_menu = guillotineMenu.findViewById(R.id.ref_farmer_menu);
        ref_farmer_menu.setTypeface(bold);

        sliderLayout = findViewById(R.id.slider_image);
        sliderLayout.setIndicatorAnimation(SliderLayout.Animations.FILL);
       //set indicator animation by using SliderLayout.Animations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderLayout.setScrollTimeInSec(1);


        refer_farmer = guillotineMenu.findViewById(R.id.refer_farmer);
        terms_faq_policies = guillotineMenu.findViewById(R.id.terms_faq_policies);
        terms_faq_policies_img = guillotineMenu.findViewById(R.id.terms_faq_policies_img);
        terms_faq_policies_txt = guillotineMenu.findViewById(R.id.terms_faq_policies_txt);
        terms_faq_policies_txt.setTypeface(bold);



        kyf_group.setOnClickListener(this);
        story_group.setOnClickListener(this);
        fn_group.setOnClickListener(this);
        support_group.setOnClickListener(this);
        terms_faq_policies.setOnClickListener(this);
        refer_farmer.setOnClickListener(this);

        box_ll = findViewById(R.id.box_ll);



        rllll = findViewById(R.id.rllll);
        desp_txt = findViewById(R.id.desp_txt);
        desp_txt.setTypeface(bold);
        cart_count_txt = findViewById(R.id.cart_count_txt);
        total_price_txt=findViewById(R.id.total_price_txt);
        price_rl = findViewById(R.id.price_rl);
        rupee_img = findViewById(R.id.rupee_img);
        price_txt = findViewById(R.id.price_txt);
        proceed_img = findViewById(R.id.proceed_img);
        veg_img = findViewById(R.id.veg_img);
        layoutBottom=findViewById(R.id.layoutBottom);
        go_box_bottom=findViewById(R.id.go_box_bottom);
        box_name_txt=findViewById(R.id.box_name_txt);

        custom_btn = findViewById(R.id.custom_btn);
        custom_btn.setTypeface(light);
        custom_btn.setOnClickListener(this);

        buy_img = findViewById(R.id.buy_img);
        profile_txt = findViewById(R.id.profile_txt);
        profile_txt.setTypeface(bold);
        orders_txt = findViewById(R.id.orders_txt);
        orders_txt.setTypeface(bold);
        share_txt = findViewById(R.id.share_txt);
        share_txt.setTypeface(bold);
        notifications_txt = findViewById(R.id.notifications_txt);
        notifications_txt.setTypeface(bold);

        profile_txt.setOnClickListener(this);
        orders_txt.setOnClickListener(this);
        share_txt.setOnClickListener(this);
        notifications_txt.setOnClickListener(this);
        buy_img.setOnClickListener(this);

        box_products_grid = findViewById(R.id.box_products_grid);
        box_products_grid.setOnItemClickListener(this);

        box_products_grid.setFocusable(false);

        cat_recyclerview = findViewById(R.id.cat_recyclerview);
        cat_recyclerview.setHasFixedSize(true);
        categoryAdapter = new CategoryAdapter(categoryModels, this, R.layout.row_category);
        cat_recyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        cat_recyclerview.setItemAnimator(new DefaultItemAnimator());
        cat_recyclerview.setAdapter(categoryAdapter);

        box_recyclerview = findViewById(R.id.box_recyclerview);
        box_recyclerview.setHasFixedSize(true);
        box_recyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        box_recyclerview.setItemAnimator(new DefaultItemAnimator());

        go_box_recyclerview = findViewById(R.id.go_box_recyclerview);
        go_box_recyclerview.setHasFixedSize(true);
        go_box_recyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        go_box_recyclerview.setItemAnimator(new DefaultItemAnimator());

        updateUIAd();
        getToken();
        getCartCount();
        getCategories();
        addBanners(); //for slider banner

    }

    private void bottomOffersAd()
    {
       // adSliderList.clear();
        checkInternet = NetworkChecking.isConnected(MainActivity.this);
        if (checkInternet) {
            pprogressDialog.show();

            Log.d("SENDADURL", AppUrls.BASE_URL + AppUrls.BOTTOM_ADS_SCROLL);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.BOTTOM_ADS_SCROLL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            try {
                                pprogressDialog.dismiss();
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("RESPFSAF", response);
                                String responceCode = jsonObject.getString("status");
                                if(responceCode.equals("10100"))
                                {

                                    JSONArray jsonArray = jsonObject.getJSONArray("data");

                                    for (int i = 0; i < jsonArray.length(); i++) {

                                        AdBottomSliderModel qm = new AdBottomSliderModel();

                                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                        String id = jsonObject1.getString("id");
                                        String description = jsonObject1.getString("description");
                                        String url_images = AppUrls.BASE_URL+jsonObject1.getString("image");
                                        String title = jsonObject1.getString("title");


                                        qm.setId(id);
                                        qm.setDesc(description);
                                        qm.setImage(url_images);
                                        qm.setTitle(title);

                                     //   qm.setUrl_name(url_name);
                                    //    qm.setStatus(status);
                                   //     qm.setCreate_date_time(create_date_time);
                                        adSliderList.add(qm);
                                    }
                                    mCustomPagerAdapter = new CustomPageAdapter(MainActivity.this, adSliderList);
                                    viewPager.setAdapter(mCustomPagerAdapter);
                               }

                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                  error.getMessage();
                }
            }){

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/json");
                    headers.put("Authorization-Basic", token);
                    Log.d("SENDADSSAFSAF", "HEADDER "+headers.toString());
                    return headers;
                }
            };
            stringRequest.setRetryPolicy( new DefaultRetryPolicy( 500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT ));
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);

        }else {
           Toast.makeText(MainActivity.this,"No Internet Connection",Toast.LENGTH_LONG).show();
        }
    }

    private void updateUIAd()
    {
        bottomOffersAd();
        viewPager = findViewById(R.id.viewpager);
        viewPager.setClipToPadding(true);
        viewPager.useScale();
        viewPager.setPageMargin(0);


        //viewPager.setCurrentItem(true);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {
                String path=adSliderList.get(position).getImage();
                Picasso.with(MainActivity.this)
                        .load(path)
                        .into(viewpager_image);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state)
            {

            }

        });
    }

    public void addBanners()
    {
        if (checkInternet)
        {
            pprogressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.APP_BANNERS_SLIDERS,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {
                            pprogressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                Log.d("BANNERDATAS", response);
                                String status = jsonObject.getString("status");
                                String p_banner = jsonObject.getString("p_banner");

                                if (status.equals("10100"))
                                {
                                    JSONArray jsonArray1 = jsonObject.getJSONArray("data");

                                    for (int i = 0; i < jsonArray1.length(); i++)
                                    {
                                        JSONObject jsonObject2 = jsonArray1.getJSONObject(i);
                                        SliderModel slider_model=new SliderModel();
                                        slider_model.setId(jsonObject2.getString("id"));
                                        slider_model.setHighlighted_text(jsonObject2.getString("highlighted_text"));
                                        slider_model.setImage(AppUrls.BASE_URL+""+jsonObject2.getString("image"));
                                        SliderView sliderView = new SliderView(MainActivity.this);

                                        user_image=AppUrls.BASE_URL+""+jsonObject2.getString("image");
                                       String  highlighted_text=jsonObject2.getString("highlighted_text");

                                        Log.d("IMAGEARRAYS",user_image);
                                        sliderView.setImageUrl(user_image);
                                        sliderView.setImageScaleType(ImageView.ScaleType.FIT_CENTER);

                                        sliderView.setDescription(highlighted_text);
                                        final int finalI = i;
                                          //at last add this view in your layout :
                                        sliderLayout.addSliderView(sliderView);
                                    }

                                        if(p_banner.equals("null") && p_banner.equals(""))
                                        {
                                            ///nothing
                                        }
                                        else
                                        {

                                        }



                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                                  error.getMessage();
                        }
                    });

            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);
        }else
            {

              //  pprogressDialog.dismiss();
                Toast.makeText(MainActivity.this,"Check Internet Connection",Toast.LENGTH_LONG);

        }

    }

    public void getToken() {
        BroadcastReceiver mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegId();
                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String message = intent.getStringExtra("message");
                    // Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                }
            }
        };
        displayFirebaseRegId();
    }

    @Override
    public void onBackPressed()
    {
     //  guillotineMenu.clearAnimation();

        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            moveTaskToBack(true);
        } else {
            Toast.makeText(getApplicationContext(), "Press Back again to Exit.", Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
            if (guillotineMenu != null){
                new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                        .setStartDelay(RIPPLE_DURATION)
                        .setActionBarViewForAnimation(toolbar)
                        .setClosedOnStart(true)
                        .build();
            }
        }

    }

    private void displayFirebaseRegId()
    {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        if (!TextUtils.isEmpty(regId)) {
            //Toast.makeText(getApplicationContext(), "Firebase Reg Id: " + regId, Toast.LENGTH_LONG).show();
            Log.d("FCM",regId);
            sendFCMTokes(user_id, regId, "ANDROID");
        } else {
            Toast.makeText(getApplicationContext(), "Firebase Reg Id is not received yet!", Toast.LENGTH_LONG).show();
        }
    }

    public void sendFCMTokes(final String user_id, final String token_fcm, final String platform) {
        boolean checkInternet = NetworkChecking.isConnected(MainActivity.this);
        if (checkInternet) {
            Log.d("SENDTOKEN", AppUrls.BASE_URL + AppUrls.FCM);
            StringRequest stringRequest = new StringRequest(Request.Method.PUT, AppUrls.BASE_URL + AppUrls.FCM,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("REPORTRESP", response);
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                String status = jsonObject.getString("status");
                                if (status.equals("10100")) {
                                    //Toast.makeText(MainActivity.this, "Token Sent", Toast.LENGTH_SHORT).show();
                                }
                                if (status.equals("10200")) {
                                    //Toast.makeText(getApplicationContext(), "Invalid Input", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                          error.getMessage();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("user_id", user_id);
                    params.put("fcm_token", token_fcm);
                    Log.d("REPORT_PARAM:", "PARMS" + params.toString());
                    return params;
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("X-Platform", "ANDROID");
                    headers.put("X-Deviceid", deviceId);
                    headers.put("Authorization-Basic", token);
                    return headers;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Toast.makeText(MainActivity.this, "No Internet Connection...!", Toast.LENGTH_SHORT).show();
        }
    }

    private void getCartCount() {

        StringRequest string_request = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.CARTCOUNT + user_id + "&browser_id=" + deviceId, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {


                try {
                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    int data = jsonObject.getInt("data");
                    cart_count_txt.setText("" + data);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.getMessage();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(string_request);
    }

    private void getCategories()
    {
        pprogressDialog.show();
        StringRequest string_request = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.CATEGORIES, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                //progressDialog.dismiss();

                categoryModels.clear();
                categoryDbHelper.deleteCategory();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    pprogressDialog.dismiss();
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    if (status.equals("10100")) {


                        //progress_indicator.setVisibility(View.GONE);


                        JSONArray jsonArray = jsonObject.getJSONArray("data");

                        ContentValues values = new ContentValues();

                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            values.put(CategoryDbHelper.CATEGORY_ID, jsonObject1.getString("id"));
                            values.put(CategoryDbHelper.CATEGORY_NAME, jsonObject1.getString("name"));
                            values.put(CategoryDbHelper.CATEGORY_IMAGE, AppUrls.BASE_URL + jsonObject1.getString("image"));
                            values.put(CategoryDbHelper.TYPE_AVAIL, jsonObject1.getString("type_avail"));
                            categoryDbHelper.addCategoryList(values);


                        }

                        //getBoxProducts(id);


                        getCategoryList();


                        //cat_recyclerview.setAdapter(categoryAdapter);

                        //progressDialog.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    //progressDialog.cancel();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

               error.getMessage();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(string_request);
    }

    private void getCategoryList() {

        categoryModels.clear();

        CategoryDbHelper db = new CategoryDbHelper(MainActivity.this);
        List<String> categoryIdList = db.getCategoryId();
        List<String> categoryNameList = db.getCategoryName();
        List<String> categoryImageList = db.getCategoryImage();
        List<String> type_avail = db.getTypeAvail();

        for (int i = 0; i < categoryIdList.size(); i++) {

            CategoryModel cm = new CategoryModel();
            cm.setId(categoryIdList.get(i));
            cm.setName(categoryNameList.get(i));
            cm.setImage(categoryImageList.get(i));
            cm.setType_avail(type_avail.get(i));

            categoryModels.add(cm);
        }

        categoryAdapter.selection(1);
        cat_recyclerview.setAdapter(categoryAdapter);
        db.close();
    }

    public void getBoxProducts(String id, final String type)
    {
        pprogressDialog.show();
        boxNameList.clear();
        selectedType=type;
        productid=id;
        String url = AppUrls.BASE_URL + AppUrls.BOXPRODUCTS + id + "&type="+type+"&page=1&user_id=" + user_id + "&browser_id=" + deviceId;
        Log.d("LISTOFINURL", url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response)
                    {
                        pprogressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            Log.d("RESPONSE:", response);

                            String status = jsonObject.getString("status");
                            String message = jsonObject.getString("message");

                             JSONObject jobVer=jsonObject.getJSONObject("versions");
                               android_version=jobVer.getString("android_version");
                               ios_version=jobVer.getString("ios_version");


                              if(!android_version.equalsIgnoreCase(checnkVeriosn))
                              {
                                  sd_update = new SweetAlertDialog(MainActivity.this, SweetAlertDialog.NORMAL_TYPE);
                                  sd_update.setTitleText("App Update");
                                  sd_update.setContentText("Please update your app now to experience our latest enhancements..!");
                                  sd_update.setConfirmText("Update Now");
                                  sd_update.showCancelButton(true);
                                  sd_update.setCanceledOnTouchOutside(false);
                                  sd_update.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                      @Override
                                      public void onClick(final SweetAlertDialog sweetAlertDialog)
                                      {
                                          Intent intent = new Intent();
                                          intent.setAction(Intent.ACTION_VIEW);
                                          intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=in.innasoft.gofarmz&hl=en"));
                                          startActivity(intent);
                                          sd_update.cancel();

                                      }
                                  });
                                  sd_update.show();
                              }

                            if (status.equalsIgnoreCase("10100"))
                            {
                                no_product_img.setVisibility(View.GONE);
                                String content = jsonObject.getString("content");
                                desp_txt.setText(content);
                                pprogressDialog.dismiss();
                                box_ll.setVisibility(View.VISIBLE);
                                price_rl.setVisibility(View.VISIBLE);
                                boxNameList.clear();
                                productsList.clear();
                                Type stringStringMap = new TypeToken<ArrayList<Object>>() {}.getType();
                               if(type.equalsIgnoreCase("COMBO_CUSTOM"))
                               {
                                   go_box_recyclerview.setVisibility(View.VISIBLE);
                                   go_box_bottom.setVisibility(View.VISIBLE);
                                   comobo_liner_layout.setVisibility(View.GONE);
                                   layoutBottom.setVisibility(View.GONE);
                                   ArrayList<Object> data = new Gson().fromJson(jsonObject.optString("data"), stringStringMap);
                                   boxNameList.addAll(data);

                                   Map<String, Object> boxPos = (Map<String, Object>) boxNameList.get(0);
                                // productid = (String) boxPos.get("id");
                                   String type = (String) boxPos.get("type");
                                   String pdtName = (String) boxPos.get("pdtName");
                                   String unitValue = (String) boxPos.get("unitValue");
                                   String price = (String) boxPos.get("price");
                                   String about = (String) boxPos.get("about");
                                   String moreinfo = (String) boxPos.get("moreinfo");
                                   String availability = (String) boxPos.get("availability");
                                   String unitName = (String) boxPos.get("unitName");
                                   String images = (String) boxPos.get("images");
                                   ContentValues values = new ContentValues();
                                   ArrayList<Object> tmpList = (ArrayList<Object>) boxPos.get("OPTIONAL");
                                   for (int i = 0; i < tmpList.size(); i++) {
                                       ArrayList<Object> custompricesarr = new ArrayList<>(tmpList.size());
                                       CustomModel model = new CustomModel();
                                       Map<String, Object> tmpObj = (Map<String, Object>) tmpList.get(i);
                                       model.setId((String) tmpObj.get("id"));
                                       model.setPdtName((String) tmpObj.get("pdtName"));
                                       model.setImages((String) tmpObj.get("images"));
                                       model.setAvailability((String) tmpObj.get("availability"));
                                       ArrayList<Object> optionstmparr = (ArrayList<Object>) tmpObj.get("options");

                                       for (int k = 0; k < optionstmparr.size(); k++) {
                                           Map<String, Object> objectMap = new HashMap<>(tmpList.size());
                                           Map<String, Object> priceObj1 = (Map<String, Object>) optionstmparr.get(k);
                                           objectMap.put("optId", priceObj1.get("optId"));
                                           objectMap.put("pdtId", priceObj1.get("pdtId"));
                                           objectMap.put("optName", priceObj1.get("optName"));
                                           objectMap.put("price", priceObj1.get("price"));
                                           objectMap.put("qtyprice", priceObj1.get("price"));
                                           objectMap.put("qty", 1);

                                           custompricesarr.add(objectMap);
                                           if (optionstmparr.size() == custompricesarr.size())
                                               model.setOptions(custompricesarr);
                                           //Log.v("LLLLLLLLLL","///"+custompricesarr);
                                       }
                                       productsList.add(model);


                                   }

                                   //  productsList();
                                   customBoxProductsAdapter = new GoCustomBoxProductsAdapter(MainActivity.this, productsList, R.layout.row_custom_box_products, total_price_txt, "");
                                   go_box_recyclerview.setNestedScrollingEnabled(false);
                                   go_box_recyclerview.setAdapter(customBoxProductsAdapter);
                               }
                               else
                                   {
                                   comobo_liner_layout.setVisibility(View.VISIBLE);
                                   layoutBottom.setVisibility(View.VISIBLE);
                                   go_box_recyclerview.setVisibility(View.GONE);
                                   go_box_bottom.setVisibility(View.GONE);
                                   ArrayList<Object> data = new Gson().fromJson(jsonObject.optString("data"), stringStringMap);
                                   boxNameList.addAll(data);
                                   boxNameAdapter = new BoxNameAdapter(boxNameList, MainActivity.this, R.layout.row_box, box_products_grid);
                                   box_recyclerview.setAdapter(boxNameAdapter);

                                   Map<String, Object> boxPos = (Map<String, Object>) boxNameList.get(0);
                                   boxNameAdapter.selection(0);
                                   SharedPreferences sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                                   SharedPreferences.Editor editor = sharedpreferences.edit();
                                   editor.putInt("position", 0);
                                   editor.apply();

                                   price(boxPos.get("price").toString());
                                   boxpdtName(boxPos.get("pdtName").toString());

                                   boxProductList = (ArrayList<Object>) boxPos.get("ESSENTIAL");
                                   boxProductsAdapter = new BoxProductsAdapter(MainActivity.this, boxProductList);
                                   box_products_grid.setAdapter(boxProductsAdapter);
                                   box_products_grid.setExpanded(true);
                               }


                            }

                            if (status.equalsIgnoreCase("10300")) {
                                pprogressDialog.dismiss();
                               // progress_indicator.setVisibility(View.GONE);
                                box_ll.setVisibility(View.GONE);
                                price_rl.setVisibility(View.GONE);
                                no_product_img.setVisibility(View.VISIBLE);

                             //   Toast.makeText(MainActivity.this, "No Data Found.", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.getMessage();
                    }
                }) {

        };
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(stringRequest);
    }

    public void price(String price)
    {
        price_txt.setText(price);
    }

    public void productId(String id) {

    }
    public void boxpdtName(String name)
    {
        box_name_txt.setText(name);
    }

    @Override
    public void onClick(View v)
    {

        if (v == cart_img) {
            String count = cart_count_txt.getText().toString();
            if (!count.equalsIgnoreCase("0"))
            {
                Intent intent = new Intent(MainActivity.this, MyCartActivity.class);
                intent.putExtra("from", "Home");
                startActivity(intent);
            } else {
                Toast.makeText(this, "Please add atleast one product", Toast.LENGTH_SHORT).show();
            }

        }
        if (v == kyf_group) {
            Intent intent = new Intent(MainActivity.this, KYFActivity.class);
            startActivity(intent);
        }
        if (v == story_group) {
            Intent intent = new Intent(MainActivity.this, OurStoryActivity.class);
            startActivity(intent);
        }
        if (v == fn_group) {
            Intent intent = new Intent(MainActivity.this, FarmerNetworkActivity.class);
            startActivity(intent);
        }
        if (v == support_group) {
            Intent intent = new Intent(MainActivity.this, SupportActivity.class);
            startActivity(intent);
        }
        if (v == refer_farmer)
        {
           Intent referFarmer = new Intent(MainActivity.this, ReferFarmerActivity.class);
           startActivity(referFarmer);
        }
        if (v == terms_faq_policies) {

           // buy_img.setClickable(false);
            Intent intent = new Intent(MainActivity.this, TermsConditionsAndPrivacy.class);
            intent.putExtra("from", "NORMAL");
            startActivity(intent);
        }

        if (v == custom_btn) {

            Intent intent = new Intent(MainActivity.this, CustomBoxProductsActivity.class);
            intent.putExtra("pid",productid);
            intent.putExtra("from","main");
            startActivity(intent);

        }
        if (v == buy_img)
        {
            boolean checkInternet = NetworkChecking.isConnected(MainActivity.this);
            if(checkInternet)
            {
                SharedPreferences sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                int shPre_val = sharedpreferences.getInt("position", 0);
                Map<String, Object> boxPos = (Map<String, Object>) boxNameList.get(shPre_val);
                pr_id = (String) boxPos.get("id");
                Log.d("ISSS", pr_id);
                if (selectedType.equalsIgnoreCase("COMBO_CUSTOM"))
                {
                    addToCart(pr_id);
                   /* String totalprice = total_price_txt.getText().toString();
                    String[] split = totalprice.split(":");
                    int total = Integer.parseInt(split[1].trim());
                    if (total >= 300)
                        addToCart(pr_id);
                    else
                        Toast.makeText(this, "Minimun order is 300/-", Toast.LENGTH_SHORT).show();*/
                }
                else
                    {
                    addToCart(pr_id);
                }
            }
            else
            {
               Toast.makeText(MainActivity.this,"Check Internet Connection",Toast.LENGTH_LONG);
            }
           /* if(selectedType.equalsIgnoreCase("COMBO_CUSTOM")){

            }else {

                addToCart(selectedType);   //ADD TO CART METHOD
            }*/

        }
        if (v == profile_txt) {
            Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
            intent.putExtra("activity","MAINACTIVITY");
            startActivity(intent);
        }
        if (v == orders_txt) {
            Intent intent = new Intent(MainActivity.this, OrdersHistoryActivity.class);
            startActivity(intent);
        }
        if (v == share_txt) {
            boolean checkInternet = NetworkChecking.isConnected(MainActivity.this);
            if (checkInternet) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "GoFarmz -");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Hi, Here is an excellent app which supplies genuine naturally grown food products, sourced from local farmers, straight to your doorstep. Download to Check it out.  "+"https://play.google.com/store/apps/details?id=in.innasoft.gofarmz&hl=en");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            } else {
                Toast.makeText(this, "No Internet connection...!", Toast.LENGTH_SHORT).show();
            }
        }
        if (v == notifications_txt) {
            Intent intent = new Intent(MainActivity.this, NotificationsActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        }


    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
    {
        Map<String, Object> mainproductObj = (Map<String, Object>) boxNameList.get(boxNameAdapter.selectedPos);
        ArrayList<Object> tmpboxproductlist = (ArrayList<Object>) mainproductObj.get("ESSENTIAL");
        Map<String, Object> itemPosition = (Map<String, Object>) tmpboxproductlist.get(position);
        itemPosition.put("pid",productid);
        Intent it = new Intent(MainActivity.this, FormarActivity.class);
        it.putExtra("productobj", new HashMap<>(itemPosition));
        it.putExtra("FROM","NONBROUSE");
        startActivity(it);
        Log.d("Chlid Id", "///" + itemPosition.get("pdtId"));
       /* new AlertDialog.Builder(MainActivity.this)
                 .setMessage("")
                .setCancelable(false)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.cancel();
                    }
                })
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent it=new Intent(MainActivity.this, FormarActivity.class);
                        it.putExtra("productobj",new HashMap<>(itemPosition));
                        startActivity(it);
                        Log.d("Chlid Id","///" + itemPosition.get("pdtId"));

                    }
                }).show();
*/
    }

    private void addToCart(final String pr_id) {
        String url = AppUrls.BASE_URL + AppUrls.ADDTOCART;
        Log.d("ADDCARTURL", url);
        pprogressDialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        pprogressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            Log.d("ADDCARTPONSE:", response);

                            String status = jsonObject.getString("status");
                            // String message = jsonObject.getString("message");

                            if (status.equalsIgnoreCase("10100")) {

                                if(selectedType.equalsIgnoreCase("COMBO_CUSTOM"))
                                makeFlyAnimation(total_price_txt);
                                else
                                    makeFlyAnimation(price_txt);
                                getCartCount();
                                Toast.makeText(MainActivity.this, "Add to Cart Successfully ", Toast.LENGTH_LONG).show();
                            }
                            if (status.equalsIgnoreCase("10200")) {
                                pprogressDialog.dismiss();
                                Toast.makeText(MainActivity.this, "Invalid Input.", Toast.LENGTH_SHORT).show();
                            }

                            if (status.equalsIgnoreCase("10300")) {
                                pprogressDialog.dismiss();
                                Toast.makeText(MainActivity.this, "Sorry Try Again.", Toast.LENGTH_SHORT).show();
                            }
                            if (status.equalsIgnoreCase("10400")) {
                                pprogressDialog.dismiss();
                                Toast.makeText(MainActivity.this, "Already added to cart", Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.getMessage();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.d("rtttrtttrt",selectedType);
                if(selectedType.equalsIgnoreCase("COMBO_CUSTOM"))
                {

                    params.put("user_id", user_id);
                    params.put("product_id", pr_id);
                    params.put("browser_id", deviceId);
                    params.put("purchase_quantity", "1");  //by default one quantity
                    for (int i = 0; i < customBoxProductsAdapter.cartarr.size(); i++)
                        params.put("addOnProducts[" + i + "]", customBoxProductsAdapter.cartarr.get(i));  //Arrya values
                }else {
                    params.put("user_id", user_id);
                    params.put("product_id", pr_id);
                    params.put("browser_id", deviceId);
                    params.put("purchase_quantity", "1");  //by default one quantity
                }

                Log.d("CARTPARAM", params.toString());
                return params;
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization-Basic", token);
                headers.put("x-device-id", deviceId);
                headers.put("x-device-platform", "ANDROID");
                Log.d("GETOTPEADER", "HEADDER " + headers.toString());
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(stringRequest);
    }



    private void makeFlyAnimation(final TextView targetView) {

        ImageView destView = findViewById(R.id.cart_img);
        new CircleAnimationUtil().attachActivity(this).setTargetView(targetView).setMoveDuration(1000).setDestView(destView).setAnimationListener(new Animator.AnimatorListener() {

            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                targetView.setVisibility(View.VISIBLE);
                getCartCount();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).startAnimation();
    }

    public void setImagePath(String imgPath)
    {

        Log.d("asdkfjsdf",imgPath);
        Picasso.with(MainActivity.this)
                .load(imgPath)
                .placeholder(R.drawable.vegetable_img)
                .into(veg_img);
   }
}
